package com.dealmaar.utilities;

/**
 * Created by cdara on 10-02-2016.
 */

import android.content.Context;
import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;


/**
 * Created by cdara on 28-01-2015.
 */
public class RetroHelper {

    public static String mEnvironment = StringConstants.DEV;

    private static Context mContext;

    public static RestAdapter getAdapter(Context ctx, String serverUrl,String header) {
        mContext=ctx;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(StringConstants.BASE_CLASSES_URL+serverUrl)
                .setRequestInterceptor(getRequestInterceptor(header))
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Retro Helper", msg);
                    }
                })
                .build();

        return restAdapter;
    }

    private static RequestInterceptor getRequestInterceptor(final String header) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                if (header != null) {
                    Log.e("KAR","header :: "+header);
                    request.addHeader("authorization", header);
                }
            }
        };

        return requestInterceptor;
    }



}
