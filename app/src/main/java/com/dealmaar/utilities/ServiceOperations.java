package com.dealmaar.utilities;

import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by sciens1 on 8/4/2016.
 */
public interface ServiceOperations {
    @GET("/products")
    public void getOnSale(@Query("on_sale") String countryId, Callback<JsonObject> callback);
}
