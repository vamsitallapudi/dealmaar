package com.dealmaar.push;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.batch.android.Batch;

import java.util.Arrays;

public class PushReceiver extends WakefulBroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        ComponentName comp = new ComponentName(context.getPackageName(), PushService.class.getName());
        startWakefulService(context, intent.setComponent(comp));
        Log.e("title key Receive", " " + intent.getStringExtra(Batch.Push.TITLE_KEY));
        Log.e("payload key Receive  ", " " + Arrays.toString(intent.getStringArrayExtra(Batch.Push.PAYLOAD_KEY)));

        setResultCode(Activity.RESULT_OK);
        Log.e("test", "on recieve");

    }

}