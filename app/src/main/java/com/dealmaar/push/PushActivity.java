package com.dealmaar.push;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebView;

import com.dealmaar.customer.R;

public class PushActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String id = getIntent().getExtras().getString("pushdata");
        Log.e("id", "" + id);
        setContentView(R.layout.activity_push);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        toolbar.setNavigationIcon(R.drawable.menu);

        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        final Drawable navigationDrawer = getResources().getDrawable(R.drawable.menu);
        navigationDrawer.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(navigationDrawer);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


      /* TextView webView=(TextView)findViewById(R.id.push_id);
        webView.setText(id);*/


        webView = (WebView) findViewById(R.id.push_id);
        webView.getSettings().setJavaScriptEnabled(true);
        //webView.loadUrl("http://www.google.com");

        //  String customHtml = "<html><body><h1>Hello, WebView</h1></body></html>";
        webView.loadData(id, "text/html", "UTF-8");
    }
}
