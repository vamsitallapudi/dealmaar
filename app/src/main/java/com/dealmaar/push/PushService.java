package com.dealmaar.push;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.batch.android.Batch;
import com.batch.android.BatchPushData;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;

import java.util.Iterator;
import java.util.Set;

public class PushService extends IntentService {
    public PushService() {
        super("MyPushService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e("test", "push recieved");

        try {
            if (Batch.Push.shouldDisplayPush(this, intent)) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                BatchPushData pushData = new BatchPushData(this, intent);
                dumpIntent(intent);
                builder.setSmallIcon(R.drawable.push_icon)
                        .setContentTitle(intent.getStringExtra(Batch.Push.TITLE_KEY))
                        .setContentText(intent.getStringExtra(Batch.Push.ALERT_KEY));
                Log.e("title key", " " + intent.getStringExtra(Batch.Push.TITLE_KEY));
                Log.e("payload key  ", " " + intent.getStringExtra("push_id"));

                Intent launchIntent;
                try {
                    if (intent.getStringExtra("push_id") != null) {
                        final String pushdata = intent.getStringExtra("push_id");


                        launchIntent = new Intent(getApplicationContext(), PushActivity.class);
                        launchIntent.putExtra("pushdata", pushdata);
                        Log.e("group id", "push " + pushdata);
                        Batch.Push.appendBatchData(intent, launchIntent);
                        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentIntent(contentIntent);


                    } else {
                        launchIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        Batch.Push.appendBatchData(intent, launchIntent);
                        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentIntent(contentIntent);
                    }
                } catch (Exception e) {
                    Log.e("Exception", "" + e);


                }


                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                // "id" is supposed to be a unique id, in order to be able to update the notification if you want.
                // If you don't care about updating it, you can simply make a random it, like below
                int id = (int) (Math.random() * Integer.MAX_VALUE);
                notificationManager.notify(id, builder.build());

                // Deeplink
               /* if (pushData.hasDeeplink()) {
                    String deeplink = pushData.getDeeplink();
                }*/
                Log.e("test", " push recieved if " + pushData);
                // Display the notification
//                Batch.Push.displayNotification(this, intent);


            }


        } finally {
            PushReceiver.completeWakefulIntent(intent);
        }
    }

    public static void dumpIntent(Intent i) {

        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("text", "Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                Log.e("test 1", "[" + key + "=" + bundle.get(key) + "]");
            }
            Log.e("test 2", "Dumping Intent end");
        }
    }
}