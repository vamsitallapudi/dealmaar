package com.dealmaar.orderslist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.BaseActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

public class OrdersListActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {
    int order_id = 28;
    int page = 2;
    int pagenumber = 10;
    ArrayList<Orders> orderslist;
    ListView list;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    ProgressDialog progressDialog;
    String uid;
    TextView noitems;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    HashSet hash = new HashSet();

    ArrayList<Product> orderProducts = new ArrayList<Product>();
    private String count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orders_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo = toolbar.getChildAt(1);
       /* final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());


        if (uid == null) {
            Toast.makeText(OrdersListActivity.this, "User not found. Please login to continue!", Toast.LENGTH_SHORT).show();
            mIntent = new Intent(this, SigninActivity.class);
            startActivity(mIntent);
            finish();
        } else {

            list = (ListView) findViewById(R.id.orderslist);
            orderslist = new ArrayList<Orders>();
            noitems = (TextView) findViewById(R.id.orders_noitems);
            new GetOrders().executeOnExecutor(MyApplication.threadPoolExecutor);

            drawerFragment = (FragmentDrawer)
                    getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
            drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
            drawerFragment.setDrawerListener(this);

        }
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        OrdersListActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    Log.e("View", String.valueOf(syncItemView));
                    //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("OrdersListActivity", e.toString());
                    }
                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(OrdersListActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

           /* case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }

    private class GetOrders extends AsyncTask<Void, Void, Void> {


        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OrdersListActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            BufferedReader bufferedReader = null;
            try {


                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();

                URI uri = new URI(getString(R.string.url) + "orders?user_id=" + uid + "&page=1&items_per_page=5");//refer to all products. Only GET and POST are supported.

                httpGet.setURI(uri);

                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Orders-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Orders-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //   Log.e("Test Activity", String.valueOf(stringBuffer));

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                jArray = jobj.optJSONArray("orders");

                if (jArray != null) {

                    for (int i = 0; i < jArray.length(); ++i) {
                        Orders or = new Orders();

                        JSONObject jobj2 = jArray.getJSONObject(i);
                        if (hash != null) {
                            if (!hash.contains(jobj2.getString("order_id"))) {
                                getProductsFromOder(jobj2.getString("order_id"));
                            }
                        }

                        hash.add(jobj2.getString("order_id"));


                    }
                }

                //Log.e("Orders", String.valueOf(orderslist));


            } catch (JSONException e) {
                //  Log.e("log_tag", "Error parsing data " );
            }


            return null;
        }


        protected void onPostExecute(Void result) {

            if (orderProducts.size() <= 0) {

                String count = "null";
                Preferences.add(Preferences.PrefType.CART_count, count, getApplicationContext());

                noitems.setVisibility(View.VISIBLE);
            }

            OrdersAdapter adapter = new OrdersAdapter(OrdersListActivity.this, R.layout.orders_item, orderProducts);

            // Binds the Adapter to the ListView
            try {
                progressDialog.dismiss();
            } catch (Exception e) {

            }
            adapter.notifyDataSetChanged();
            list.setAdapter(adapter);

            list.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view,
                                                 int scrollState) { // TODO Auto-generated method stub
                    int threshold = 1;
                    int count = list.getCount();
                    //   Log.e("State", String.valueOf(scrollState));
                    if (scrollState == SCROLL_STATE_IDLE) {
                        if (list.getLastVisiblePosition() >= count
                                - threshold) {
                            //  Log.e("count", String.valueOf(count));
                            new LoadMoreDataTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    // TODO Auto-generated method stub
                }
            });


        }

        private void getProductsFromOder(String orderId) {
            //ArrayList<Product> products = new ArrayList<Product>();
            StringBuffer stringBuffer = new StringBuffer("");
            BufferedReader bufferedReader = null;
            JSONArray jsonArray = new JSONArray();

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "orders/" + orderId);//refer to all orders. Only GET and POST are supported.
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("OrderProd-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("OrderProd-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    // Log.i("Test Adapter", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                //Log.e("String Buffer", String.valueOf(stringBuffer));
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                // jArray = jobj.optJSONArray("products");
                JSONObject productsOBJ = jobj.getJSONObject("products");
                Iterator<String> keys = productsOBJ.keys();
                jsonArray = jobj.optJSONArray("product_groups");
                for (int i = 0; i < jsonArray.length(); ++i) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONObject jobj1 = jsonObject.getJSONObject("products");
                    while (keys.hasNext()) {
                        Product pro = new Product("product", "my_product");

                        if (jobj.has("status")) {

                            // Orders or = new Orders();
                            pro.setStatus(jobj.getString("status"));

                        }
                        String key = keys.next();
                        JSONObject jobj2 = jobj1.getJSONObject(key);
                        Log.e("Name", "" + jobj2.getString("product"));
                        pro.setProduct_name(jobj2.getString("product"));
                        pro.setProduct_image(jobj2.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));


                        orderProducts.add(pro);
                    }


                }


                //Log.e("Order Product List", String.valueOf(orderProductsList));
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

            //return products;
        }


        //Load more data
        private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {

            private StringBuffer stringBuffer = new StringBuffer();
            private JSONArray jArray = new JSONArray();
            //ArrayList<Product> orderProducts = new ArrayList<Product>();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Void doInBackground(Void... params) {


                BufferedReader bufferedReader = null;
                try {

                    //   Log.e("Vendors","About to execute vendors");
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet();

                    URI uri = new URI(getString(R.string.url) + "orders?user_id=" + uid + "&page=" + page + "&items_per_page=5");//refer to all products. Only GET and POST are supported.

                    Log.e("uri", "" + uri);
                    httpGet.setURI(uri);

                    httpGet.addHeader(BasicScheme.authenticate(
                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                            HTTP.UTF_8, false));
                    HttpResponse httpResponse;
                    Log.e("LoadOrders-Before", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    httpResponse = httpClient.execute(httpGet);
                    Log.e("LoadOrders-After", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String readLine = bufferedReader.readLine();
                    while (readLine != null) {
                        stringBuffer.append(readLine);
                        stringBuffer.append("\n");
                        readLine = bufferedReader.readLine();
                        //Log.e("Test Activity", String.valueOf(stringBuffer));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {

                        }
                    }
                }


                try {


                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                    jArray = jobj.optJSONArray("orders");
                    Log.e("Ord", String.valueOf(jArray));
                    if (jArray.length() > 0) {
                        page++;
                    }


                    if (jArray != null) {

                        for (int i = 0; i < jArray.length(); ++i) {
                            Orders or = new Orders();

                            JSONObject jobj2 = jArray.getJSONObject(i);
                            if (hash != null) {
                                if (!hash.contains(jobj2.getString("order_id"))) {
                                    getProductsFromOder(jobj2.getString("order_id"));
                                }
                            }

                            hash.add(jobj2.getString("order_id"));


                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }


            protected void onPostExecute(Void result) {
                int position = list.getLastVisiblePosition();

                if (orderProducts.size() > 0) {
                    OrdersAdapter adapter = new OrdersAdapter(OrdersListActivity.this, R.layout.orders_item, orderProducts);

                    adapter.notifyDataSetChanged();
                    list.setAdapter(adapter);
                    list.setSelectionFromTop(position, 0);
                    adapter.notifyDataSetChanged();
                }
            }

            private void getProductsFromOder(String orderId) {

                StringBuffer stringBuffer = new StringBuffer("");
                BufferedReader bufferedReader = null;
                JSONArray jsonArray = new JSONArray();
                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet();
                    URI uri = new URI(getString(R.string.url) + "orders/" + orderId);//refer to all orders. Only GET and POST are supported.
                    httpGet.setURI(uri);
                    httpGet.addHeader(BasicScheme.authenticate(
                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                            HTTP.UTF_8, false));
                    Log.e("LOrderProd-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    HttpResponse httpResponse = httpClient.execute(httpGet);
                    Log.e("LOrderProd-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String readLine = bufferedReader.readLine();
                    while (readLine != null) {
                        stringBuffer.append(readLine);
                        stringBuffer.append("\n");
                        readLine = bufferedReader.readLine();
                        // Log.i("Test Adapter", String.valueOf(stringBuffer));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {

                        }
                    }
                }
                try {
                    //Log.e("String Buffer", String.valueOf(stringBuffer));
                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                    // jArray = jobj.optJSONArray("products");
                    JSONObject productsOBJ = jobj.getJSONObject("products");
                    Iterator<String> keys = productsOBJ.keys();


                    jsonArray = jobj.optJSONArray("product_groups");
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        JSONObject jobj1 = jsonObject.getJSONObject("products");
                        Product pro = new Product("product", "my_product");

                        while (keys.hasNext()) {
                            if (jobj.has("status")) {

                                //Orders or = new Orders();
                                pro.setStatus(jobj.getString("status"));

                            }
                            String key = keys.next();
                            JSONObject jobj2 = jobj1.getJSONObject(key);
                            Log.e("Name", "" + jobj2.getString("product"));
                            pro.setProduct_name(jobj2.getString("product"));
                            pro.setProduct_image(jobj2.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));


                        }
                        orderProducts.add(pro);

                    }
                    //Log.e("Order Product List", String.valueOf(orderProductsList));
                } catch (JSONException e) {
                    Log.e("log_tag", "Error parsing data " + e.toString());
                }

                //return products;
            }


        }
    }

}
