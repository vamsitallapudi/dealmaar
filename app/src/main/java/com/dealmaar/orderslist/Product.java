package com.dealmaar.orderslist;

/**
 * Created by omm on 11/6/2015.
 */
public class Product {

    private String product_companyname;

    private String product_name;
    private String product_image;
    private String base_price;
    private String status;


    public Product(String product, String my_product) {
    }


    public String getBase_price() {
        return base_price;
    }

    public void setBase_price(String base_price) {
        this.base_price = base_price;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status=status;
    }


    public String getProduct_companyname() {
        return product_companyname;
    }

    public void setProduct_companyname(String product_id) {
        this.product_companyname = product_companyname;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }


}
