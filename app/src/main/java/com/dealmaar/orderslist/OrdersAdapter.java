package com.dealmaar.orderslist;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.customer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrdersAdapter extends ArrayAdapter<Product> {

   // ArrayList<Orders> arrayListOrders;
    ArrayList<Product> orderProductsList;
    int resource;
    Context context;
    LayoutInflater vi;
    StringBuffer stringBuffer;

    public OrdersAdapter(Context context, int resource, ArrayList<Product> objects) {
        super(context, resource, objects);
        //arrayListOrders = (ArrayList<Orders>) objects;
        orderProductsList = (ArrayList<Product>)objects ;
        this.resource = resource;
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            convertView = vi.inflate(R.layout.orders_item, parent, false);
            holder = new ViewHolder();
            holder.order_name = (TextView) convertView.findViewById(R.id.order_name);
            holder.order_status=(TextView)convertView.findViewById(R.id.order_status);
            holder.product_image = (ImageView) convertView.findViewById(R.id.order_img);
            holder.product_image.setImageResource(R.drawable.ic_launcher);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.order_name.setText("order name");

        holder.order_name.setText(orderProductsList.get(position).getProduct_name());
        if((orderProductsList.get(position).getStatus()).equals("O"))
        {
            holder.order_status.setText("Order is Open");
        }
        if((orderProductsList.get(position).getStatus()).equals("Y"))
        {
            holder.order_status.setText("Order is Awaiting Call");
        }
        if((orderProductsList.get(position).getStatus()).equals("I"))
        {
            holder.order_status.setText("Order is Canceled");
        }
        if((orderProductsList.get(position).getStatus()).equals("C"))
        {
            holder.order_status.setText("Order is Complete");
        }
        if((orderProductsList.get(position).getStatus()).equals("P"))
        {
            holder.order_status.setText("Order is being processed");
        }
        if((orderProductsList.get(position).getStatus()).equals("F"))
        {
            holder.order_status.setText("Order is Failed");
        }
        if((orderProductsList.get(position).getStatus()).equals("Y"))
        {
            holder.order_status.setText("Order is Awaiting Call");
        }
        if((orderProductsList.get(position).getStatus()).equals("D"))
        {
            holder.order_status.setText("Order is Declined");
        }

        Uri uri = Uri.parse(orderProductsList.get(position).getProduct_image());
        Context mcontext = holder.product_image.getContext();
        Picasso.with(mcontext)
                .load(uri).error(R.drawable.ic_launcher).into(holder.product_image);

        return convertView;
    }

    static class ViewHolder {
        public TextView orders_id;
        public TextView company_id;
        public ImageView product_image;
        public TextView order_name,order_status;

    }


}
