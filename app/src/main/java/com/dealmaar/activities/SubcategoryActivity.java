package com.dealmaar.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.adapters.CategoriesAdapter;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.productpage.ProductPageActivity;
import com.dealmaar.productslist.Categories;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.search.ProductSearchActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.readystatesoftware.viewbadger.BadgeView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SubcategoryActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private ListView lvSubCategory;
    private TextView tvNoItemsFound;
    private ArrayList<Categories> categoriesList;
    String catListString;
    private Intent mIntent = null;
    private ArrayList<Categories> subCatList;

    private ProgressDialog progressDialog;
    private DatabaseHandler db;
    String uid;
    DrawerLayout drawerLayout;
    private FragmentDrawer drawerFragment;
    private String count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);



        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setLogo(R.drawable.logo2);
        Batch.onStart(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.home_fragment_navigation_drawer);
        drawerFragment.setUp(R.id.home_fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.home_drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        drawerFragment.onDestroy();

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        Batch.User.getEditor().setIdentifier(uid).setAttribute("user_id", uid).save();
        View logo = toolbar.getChildAt(1);


        lvSubCategory = (ListView) findViewById(R.id.lv_products_list);
        subCatList = new ArrayList<Categories>();

        categoriesList = CategoriesAdapter.categoriesList;

        String categoryId = getIntent().getStringExtra("category_id");

        for (int i = 0; i < categoriesList.size(); i++) {
            Categories categories = categoriesList.get(i);
            if (categoryId.equals(categories.getParent_id())) {
                subCatList.add(categories);
            }
        }
        if (subCatList.size() != 0) {
//            if (tvNoItemsFound.getVisibility() == View.VISIBLE) {
//                tvNoItemsFound.setVisibility(View.GONE);
//            }
            CategoriesAdapter categoriesAdapter = new CategoriesAdapter(this, subCatList);
            lvSubCategory.setAdapter(categoriesAdapter);
            Log.d("MyCategoriesList", categoriesList.toString());
        } else {
//            tvNoItemsFound.setVisibility(View.VISIBLE);
            Intent intent = new Intent(this, ProductSearchActivity.class);
            intent.putExtra("fromSubCategory", "1");
            intent.putExtra("catid", categoryId);
            startActivity(intent);
            finish();
        }


    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }
    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:

                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    progressDialog = new ProgressDialog(SubcategoryActivity.this);
                    progressDialog.setMessage("Processing logout...");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(false);
                    progressDialog.show();

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();
                    progressDialog.dismiss();
                    break;

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }


        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View cart = findViewById(R.id.action_cart);
                    // View notifications=findViewById(R.id.action_posts);
                    // Log.e("View", String.valueOf(cart));
                    // String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    //Integer c=Integer.parseInt(count);
                    // Log.e("Co", "Count" + (count.equals("null")));
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("SubcategoryActivity", e.toString());
                    }
                    if (count == null || (count.equals("null"))) {
                        //Log.e("If", "if");
                        count = "0";

                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        // Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(SubcategoryActivity.this, cart);
                        badge.setText(count);
                        badge.show();
                    }
                    //HomeActivity.this.invalidateOptionsMenu();
                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_cart:
                Intent cartintent = new Intent(SubcategoryActivity.this, CartActivity.class);
                startActivity(cartintent);
                // finish();
                return true;


            case R.id.action_posts:
                Intent postsintent = new Intent(SubcategoryActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

}
