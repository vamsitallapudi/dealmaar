package com.dealmaar.activities;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dealmaar.customer.R;
import com.dealmaar.home.CustomAdapter;
import com.dealmaar.home.Home;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class PaymentOptionsActivity extends AppCompatActivity {

    LinearLayout llPaymentRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);

        llPaymentRadioGroup = (LinearLayout) findViewById(R.id.ll_payment_radio_group);
        RadioGroup rg = new RadioGroup(this);
        llPaymentRadioGroup.addView(rg);


        RadioButton radioButton = new RadioButton(this);
        radioButton.setText("CC Avenue");
        rg.addView(radioButton);
        RadioButton radioButton1 = new RadioButton(this);
        radioButton1.setText("Cash On Delivery");
        rg.addView(radioButton1);
    }




    private class GetPaymentOptions extends AsyncTask<Void, Void, Void> {

        StringBuffer stringBuffer = new StringBuffer("");
        JSONObject jArray = new JSONObject();
        BufferedReader bufferedReader = null;

        @Override
        protected Void doInBackground(Void... params) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "payments/");
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Payments-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Payments-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }

            }
            try {
                JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                // Log.e("Json obj", String.valueOf(jsonObject));
                jArray = jsonObject.put("Image", "true");
                // Log.e("Image array", String.valueOf(productsArray));
             /* for(int i=0;i<productsArray.length();++i)
              {
                  jsonObject=productsArray.getJSONObject(String.valueOf(i));
                  Log.e("image"," "+jsonObject);
              }*/

                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    //Log.e("JArray in for loop ", String.valueOf(productsArray));
                    String key = keys.next();
                    //Log.e("prod key",key);
                    Home home = new Home();
                    JSONObject jobj2 = jsonObject.getJSONObject(key);
                    home.setProduct_image(jobj2.getString("image_path"));
                    //  pro.setProduct_image(jobj2.getString("img_path"));
//                        images.add(home);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
//                CustomAdapter adapter = new CustomAdapter(PaymentOptionsActivity.this, images);
//                viewPager.setAdapter(adapter);
            } catch (Exception e) {
                Toast.makeText(PaymentOptionsActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }

    }



}
