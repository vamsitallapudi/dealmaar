package com.dealmaar.activities;

//Created By Vamsi Tallapudi on 1- July 16

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.batch.android.Batch;
import com.dealmaar.adapters.CategoriesAdapter;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.models.Product;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.productslist.Categories;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {


    private ArrayList<Categories> categoriesList;
    private List<Categories> primeCatList;
    private ListView lvCategoriesList;
    DrawerLayout drawerLayout;
    private FragmentDrawer drawerFragment;

    String uid;
    private Intent mIntent = null;
    private ProgressDialog progressDialog;
    private DatabaseHandler db;
    private String count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setLogo(R.drawable.logo2);
        Batch.onStart(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.home_fragment_navigation_drawer);
        drawerFragment.setUp(R.id.home_fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.home_drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        drawerFragment.onDestroy();

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());

            Batch.User.getEditor().setIdentifier(uid).setAttribute("user_id", uid).save();
            View logo = toolbar.getChildAt(1);

            categoriesList = new ArrayList<>();
            primeCatList = new ArrayList<>();
            lvCategoriesList = (ListView) findViewById(R.id.lv_category_list);

            new PerformNetworkRequest(getString(R.string.url) + "categories/").executeOnExecutor(MyApplication.threadPoolExecutor);

            logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(i);
                    finish();
                }
            });
    }

//    private void fetchCategories() {
//        StringRequest sr = new StringRequest(Request.Method.GET,
//                getString(R.string.url) + "categories/",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject respObj = new JSONObject(response);
//                            JSONArray catArray = respObj.getJSONArray("categories");
//                            for (int i = 0; i < catArray.length(); i++) {
//
//                                JSONObject catObj = catArray.getJSONObject(i);
//                                Categories categories = new Categories();
//                                categories.setCategory_id(catObj.getString("category_id"));
//                                categories.setParent_id(catObj.getString("parent_id"));
//                                categories.setPath_id(catObj.getString("id_path"));
//                                categories.setCategory(catObj.getString("category"));
//                                categories.setPosition(catObj.getString("position"));
//                                categories.setStatus(catObj.getString("status"));
//                                categories.setProduct_count(catObj.getString("product_count"));
//                                categories.setSeo_name(catObj.getString("seo_name"));
//                                categories.setSeo_path(catObj.getString("seo_path"));
//                                categoriesList.add(categories);
//                            }
//
//                            lvCategoriesList.setAdapter(new CategoriesAdapter(CategoriesActivity.this, categoriesList));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        Log.d("volleyerror", "error");
//                    }
//                }
//        );
//        MyApplication.getInstance().addToRequestQueue(sr);
//    }

    private class PerformNetworkRequest extends AsyncTask<Void, Void, Void> {

        StringBuffer stringBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;
        String searchUrl;

        public PerformNetworkRequest(String searchUrl) {
            this.searchUrl = searchUrl;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(CategoriesActivity.this, "Getting Data",
                    "Please wait while we fetch data", true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                // Log.e("url", String.valueOf(uri));

                URI uri = new URI(searchUrl.replace(" ", "%20"));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //Log.e("Test1", String.valueOf(stringBuffer));
                }


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            Log.d("response", stringBuffer.toString());

            try {
                JSONObject respObj = new JSONObject(stringBuffer.toString());

                JSONArray catArray = respObj.getJSONArray("categories");
                for (int i = 0; i < catArray.length(); i++) {

                    JSONObject catObj = catArray.getJSONObject(i);
                    Categories categories = new Categories();
                    categories.setCategory_id(catObj.getString("category_id"));
                    categories.setParent_id(catObj.getString("parent_id"));
                    categories.setPath_id(catObj.getString("id_path"));
                    categories.setCategory(catObj.getString("category"));
                    categories.setPosition(catObj.getString("position"));
                    categories.setStatus(catObj.getString("status"));
                    categories.setProduct_count(catObj.getString("product_count"));
                    categories.setSeo_name(catObj.getString("seo_name"));
                    categories.setSeo_path(catObj.getString("seo_path"));
                    if (categories.getStatus().equals("A")) {
                        categoriesList.add(categories);
                    }
                    if (catObj.getString("parent_id").equals("0") && catObj.getString("status").equals("A")) {
                        primeCatList.add(categories);
                    }
                }


            } catch (JSONException exception) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Collections.sort(primeCatList, new SortBasedOnName());
            CategoriesAdapter adapter = new CategoriesAdapter(CategoriesActivity.this,
                    primeCatList,
                    categoriesList);
            lvCategoriesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }

    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:

                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    progressDialog = new ProgressDialog(CategoriesActivity.this);
                    progressDialog.setMessage("Processing logout...");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(false);
                    progressDialog.show();
                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();
                    progressDialog.dismiss();
                    break;

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }


        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View cart = findViewById(R.id.action_cart);
                    // View notifications=findViewById(R.id.action_posts);
                    // Log.e("View", String.valueOf(cart));
                    // String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    //Integer c=Integer.parseInt(count);
                    // Log.e("Co", "Count" + (count.equals("null")));
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("CategoriesActivity", e.toString());
                    }
                    if (count == null || (count.equals("null"))) {
                        //Log.e("If", "if");
                        count = "0";

                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        // Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(CategoriesActivity.this, cart);
                        badge.setText(count);
                        badge.show();
                    }
                    //HomeActivity.this.invalidateOptionsMenu();
                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_cart:
                Intent cartintent = new Intent(CategoriesActivity.this, CartActivity.class);
                startActivity(cartintent);
                // finish();
                return true;


            case R.id.action_posts:
                Intent postsintent = new Intent(CategoriesActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }


    public class SortBasedOnName implements Comparator {
        public int compare(Object o1, Object o2) {

            Categories dd1 = (Categories) o1;// where Categories is your object class
            Categories dd2 = (Categories) o2;
            return dd1.getCategory().compareToIgnoreCase(dd2.getCategory());//where uname is field name
        }
    }

}
