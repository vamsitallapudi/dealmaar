package com.dealmaar.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dealmaar.customer.R;
import com.dealmaar.search.ProductSearchActivity;

/**
 * Created by vamsi on 29-Jun-16.
 */
public class FilterFragment extends Fragment implements View.OnClickListener{

    LinearLayout llBrandsList,llPriceList;
    CheckBox cbBrand,cbPrice;
    CheckBox cb0To500,cb500To1000,cb1000To5000,cb5000To10000,cb10000To20000,
            cb20000To50000,cb50000above;
    Button bApply;
    String urlAdd ="";

    RadioGroup mRadioGroup;
    RadioButton rb0To500,rb500To1000,rb1000To5000,rb5000To10000,rb10000To20000,
            rb20000To50000,rb50000above;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.productslist_filter,container,false);

        llBrandsList = (LinearLayout) v.findViewById(R.id.ll_brands_list);
        llPriceList = (LinearLayout) v.findViewById(R.id.ll_price_list);
        cbBrand = (CheckBox) v.findViewById(R.id.cb_brand);
        cbPrice = (CheckBox) v.findViewById(R.id.cb_price);
        bApply = (Button) v.findViewById(R.id.b_apply);

        cb0To500 = (CheckBox) v.findViewById(R.id.cb_price_0_500);
        cb500To1000 = (CheckBox) v.findViewById(R.id.cb_price_500_1000);
        cb1000To5000 = (CheckBox) v.findViewById(R.id.cb_price_1000_5000);
        cb5000To10000 = (CheckBox) v.findViewById(R.id.cb_price_5000_10000);
        cb10000To20000 = (CheckBox) v.findViewById(R.id.cb_price_10000_20000);
        cb20000To50000 = (CheckBox) v.findViewById(R.id.cb_price_20000_50000);
        cb50000above = (CheckBox) v.findViewById(R.id.cb_price_50000_and_above);

        rb0To500 = (RadioButton)v.findViewById(R.id.rb_price_0_500) ;
        rb500To1000 = (RadioButton)v.findViewById(R.id.rb_price_500_1000) ;
        rb1000To5000 = (RadioButton)v.findViewById(R.id.rb_price_1000_5000) ;
        rb5000To10000 = (RadioButton)v.findViewById(R.id.rb_price_5000_10000) ;
        rb10000To20000 = (RadioButton)v.findViewById(R.id.rb_price_10000_20000) ;
        rb20000To50000 = (RadioButton)v.findViewById(R.id.rb_price_20000_50000) ;
        rb50000above = (RadioButton)v.findViewById(R.id.rb_price_50000_and_above) ;


        cbBrand.setOnClickListener(this);
        cbPrice.setOnClickListener(this);
        llBrandsList.setOnClickListener(this);
        bApply.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.ll_brands_list:
                break;
            case R.id.cb_brand:
                Toast.makeText(getActivity(), "cbBrand Clicked", Toast.LENGTH_SHORT).show();
                if(llBrandsList.getVisibility()== View.VISIBLE){
                    llBrandsList.setVisibility(View.GONE);
                }else{
                    llBrandsList.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.cb_price:
                Toast.makeText(getActivity(), "cbPrice Clicked", Toast.LENGTH_SHORT).show();
                if(llPriceList.getVisibility()== View.VISIBLE){
                    llPriceList.setVisibility(View.GONE);
                }else{
                    llPriceList.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.b_apply:
                urlAdd = "";

                if(rb0To500.isChecked()){
                    urlAdd = "&price_from=0&price_to=500";
                }
                if(rb500To1000.isChecked()){
                    urlAdd = "&price_from=500&price_to=1000";
                }
                if(rb1000To5000.isChecked()){
                    urlAdd = "&price_from=1000&price_to=5000";
                }
                if(rb5000To10000.isChecked()){
                    urlAdd = "&price_from=5000&price_to=10000";
                }
                if(rb10000To20000.isChecked()){
                    urlAdd =  "&price_from=10000&price_to=20000";
                }
                if(rb20000To50000.isChecked()){
                    urlAdd =  "&price_from=20000&price_to=50000";
                }
                if(rb50000above.isChecked()){
                    urlAdd = "&price_from=50000";
                }

                ((ProductSearchActivity)getActivity()).setPriceFilter(urlAdd);
                Log.d("urlAdd",urlAdd);
                getActivity().onBackPressed();
                break;


            default:

                break;
        }
    }
}