package com.dealmaar.home;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.customer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;


/**
 * Created by OMM on 11/2/2015.
 */
public class HomeMusic extends Fragment {
    ImageView mimage1, mimage2, mimage3, mimage4, mimage5;
    TextView mprice1, mprice2, mprice3, mprice4, mprice5;
    TextView mname1, mname2, mname3, mname4, mname5;
    ArrayList<Home> Music = new ArrayList<>();

    public HomeMusic() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_music, container, false);
        mname1 = (TextView) view.findViewById(R.id.home_music_name1);
        mname2 = (TextView) view.findViewById(R.id.home_music_name2);
        mname3 = (TextView) view.findViewById(R.id.home_music_name3);
        mname4 = (TextView) view.findViewById(R.id.home_music_name4);
        mname5 = (TextView) view.findViewById(R.id.home_music_name5);

        mprice1 = (TextView) view.findViewById(R.id.home_music_price1);
        mprice2 = (TextView) view.findViewById(R.id.home_music_price2);
        mprice3 = (TextView) view.findViewById(R.id.home_music_price3);
        mprice4 = (TextView) view.findViewById(R.id.home_music_price4);
        mprice5 = (TextView) view.findViewById(R.id.home_music_price5);

        mimage1 = (ImageView) view.findViewById(R.id.home_music_image1);
        mimage2 = (ImageView) view.findViewById(R.id.home_music_image2);
        mimage3 = (ImageView) view.findViewById(R.id.home_music_image3);
        mimage4 = (ImageView) view.findViewById(R.id.home_music_image4);
        mimage5 = (ImageView) view.findViewById(R.id.home_music_image5);
        new GetMusic().execute();
        return view;
    }

    private class GetMusic extends AsyncTask<Void, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();
        BufferedReader bufferedReader = null;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(getContext().getAssets().open("MainScreen/Music")));
                Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                Log.e("NewProducts", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                JSONArray jsonArray = new JSONArray(String.valueOf(stringBuffer));
                Log.e("j", String.valueOf(jsonArray));
                for (int i = 0; i < jsonArray.length(); i++) {
                    Home home = new Home();
                    JSONObject jobj = jsonArray.getJSONObject(i);
                    home.setProduct_id(jobj.getString("product_id"));
                    home.setProduct_name(jobj.getString("product"));
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(jobj.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(jobj.getString("list_price"))));
                    home.setProduct_des(jobj.getString("full_description"));

                    home.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                    Music.add(home);
                }
                Log.e("BLength", String.valueOf(Music.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mname1.setText(Music.get(0).getProduct_name());
            mname2.setText(Music.get(1).getProduct_name());
            mname3.setText(Music.get(2).getProduct_name());
            mname4.setText(Music.get(3).getProduct_name());
            mname5.setText(Music.get(4).getProduct_name());
            mprice1.setText(Music.get(0).getProduct_price());
            mprice2.setText(Music.get(1).getProduct_price());
            mprice3.setText(Music.get(2).getProduct_price());
            mprice4.setText(Music.get(3).getProduct_price());
            mprice5.setText(Music.get(4).getProduct_price());
            String mimg1 = Music.get(0).getProduct_image();
            String mimg2 = Music.get(1).getProduct_image();
            String mimg3 = Music.get(2).getProduct_image();
            String mimg4 = Music.get(3).getProduct_image();
            String mimg5 = Music.get(4).getProduct_image();
            InputStream mis1 = null, mis2 = null, mis3 = null, mis4 = null, mis5 = null;
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/music/url_uploaded_file_13287719184f33734f6b3e9.jpg")
                    .into(mimage1);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/music/url_uploaded_file_13287720374f3373c6eef1f.jpg")
                    .into(mimage2);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/music/cover1.jpg")
                    .into(mimage3);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/music/02xrms-cd.jpg")
                    .into(mimage4);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/music/url_uploaded_file_13287662164f335d096a3ce.jpg")
                    .into(mimage5);

           /* try {
                mis1=getContext().getAssets().open(mimg1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap mbitmap1= BitmapFactory.decodeStream(mis1);
            mimage1.setImageBitmap(mbitmap1);
            try {
                mis2=getContext().getAssets().open(mimg2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap mbitmap2=BitmapFactory.decodeStream(mis2);
            mimage2.setImageBitmap(mbitmap2);

            try {
                mis3=getContext().getAssets().open(mimg3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap mbitmap3=BitmapFactory.decodeStream(mis3);
            mimage3.setImageBitmap(mbitmap3);
            try {
                mis4=getContext().getAssets().open(mimg4);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap mbitmap4=BitmapFactory.decodeStream(mis4);
            mimage4.setImageBitmap(mbitmap4);
            try {
                mis5=getContext().getAssets().open(mimg5);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap mbitmap5=BitmapFactory.decodeStream(mis5);
            mimage5.setImageBitmap(mbitmap5);
*/
        }
    }
}
