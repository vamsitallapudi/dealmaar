package com.dealmaar.home;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.activities.CategoriesActivity;
import com.dealmaar.adapters.ItemAdapter;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.BaseActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.models.Item;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.other.RecyclerItemClickListener;
import com.dealmaar.productpage.ProductPageActivity;
import com.dealmaar.productslist.Productslist;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.search.ProductSearchActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class HomeActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {


    public ArrayList<Productslist> recentlyviewed = new ArrayList<>();
    ImageView eimage1, eimage2, eimage3, eimage4, eimage5;
    TextView[] osname;
    TextView[] osprice;
    TextView[] bsname;
    TextView[] bsprice;
    TextView[] tname;
    TextView[] tprice;
    ImageView[] osimage, bsimage, timage;
    LinearLayout[] linearLayouts, onsalelinear, bestsellerslinear, trendinglinear;
    String uid;
    ViewPager viewPager, viewPager1;
    private ProgressDialog progressDialog;
    private DatabaseHandler db;

    private TextView ename1, ename2, ename3, ename4, ename5, eprice1, eprice2, eprice3, eprice4, eprice5;
    private ArrayList<Home> images = new ArrayList<>();
    private ArrayList<Home> images1 = new ArrayList<>();
    private ImageView[] recently_image;
    private TextView[] recently_name, recently_price;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    private ArrayList<Home> NewProducts;
    private ArrayList<Home> BestSellers = new ArrayList<>();
    private ArrayList<Home> Electronics = new ArrayList<>();
    private ArrayList<Home> Sports = new ArrayList<>();
    private DrawerLayout drawerLayout;
    private String count;
    private RecyclerView mOnSaleRecyclerView;
    private RecyclerView mTrendingRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mTrendingLayoutManager;
    private ArrayList<Item> onSaleItemList;
    private ArrayList<Item> trendingItemList;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int pastVisibleItemsTrending,visibleItemCountTrending, totalItemCountTrending;
    private ItemAdapter onSaleItemAdapter;
    private ItemAdapter trendingItemAdapter;
    private int onSalePage;
    private int trendingPage;
    private boolean shouldFetchOnSale = true;
    private boolean shouldFetchTrending = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        Batch.onStart(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mOnSaleRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        if (mOnSaleRecyclerView != null) {
            mOnSaleRecyclerView.setHasFixedSize(true);
        }


        mTrendingRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_trending);

        if(mTrendingRecyclerView != null){
            mTrendingRecyclerView.setHasFixedSize(true);
        }

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mTrendingLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        mOnSaleRecyclerView.setLayoutManager(mLayoutManager);
        mTrendingRecyclerView.setLayoutManager(mTrendingLayoutManager);

        onSaleItemList = new ArrayList<>();
        trendingItemList = new ArrayList<>();


        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        Batch.User.getEditor().setIdentifier(uid).setAttribute("user_id", uid).save();
        View logo = toolbar.getChildAt(1);
        NewProducts = new ArrayList<>();
        viewPager = (ViewPager) findViewById(R.id.home_viewPager);
        viewPager1 = (ViewPager) findViewById(R.id.home_viewPager1);

        new GetImages().executeOnExecutor(MyApplication.threadPoolExecutor);
        new GetImages1().executeOnExecutor(MyApplication.threadPoolExecutor);

        onsalelinear = new LinearLayout[30];
        bestsellerslinear = new LinearLayout[30];
        trendinglinear = new LinearLayout[30];
        linearLayouts = new LinearLayout[20];
        osname = new TextView[30];
        osprice = new TextView[30];
        osimage = new ImageView[30];
        bsname = new TextView[30];
        bsprice = new TextView[30];
        tprice = new TextView[25];
        tname = new TextView[30];
        timage = new ImageView[30];
        bsimage = new ImageView[30];
        recently_name = new TextView[20];
        recently_price = new TextView[20];
        recently_image = new ImageView[20];
        ArrayList arrayList = new ArrayList<>();
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
        String s = ((MyApplication) getApplicationContext()).getCartCount();

        arrayList = db.getAllRecentlyViewedList();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });

        trendinglinear[0] = (LinearLayout) findViewById(R.id.trending_linear1);
        trendinglinear[1] = (LinearLayout) findViewById(R.id.trending_linear2);
        trendinglinear[2] = (LinearLayout) findViewById(R.id.trending_linear3);
        trendinglinear[3] = (LinearLayout) findViewById(R.id.trending_linear4);
        trendinglinear[4] = (LinearLayout) findViewById(R.id.trending_linear5);
        trendinglinear[5] = (LinearLayout) findViewById(R.id.trending_linear6);
        trendinglinear[6] = (LinearLayout) findViewById(R.id.trending_linear7);
        trendinglinear[7] = (LinearLayout) findViewById(R.id.trending_linear8);
        trendinglinear[8] = (LinearLayout) findViewById(R.id.trending_linear9);
        trendinglinear[9] = (LinearLayout) findViewById(R.id.trending_linear10);
        trendinglinear[10] = (LinearLayout) findViewById(R.id.trending_linear11);
        trendinglinear[11] = (LinearLayout) findViewById(R.id.trending_linear12);
        trendinglinear[12] = (LinearLayout) findViewById(R.id.trending_linear13);
        trendinglinear[13] = (LinearLayout) findViewById(R.id.trending_linear14);
        trendinglinear[14] = (LinearLayout) findViewById(R.id.trending_linear15);
        trendinglinear[15] = (LinearLayout) findViewById(R.id.trending_linear16);
        trendinglinear[16] = (LinearLayout) findViewById(R.id.trending_linear17);
        trendinglinear[17] = (LinearLayout) findViewById(R.id.trending_linear18);
        trendinglinear[18] = (LinearLayout) findViewById(R.id.trending_linear19);
        trendinglinear[19] = (LinearLayout) findViewById(R.id.trending_linear20);
        trendinglinear[20] = (LinearLayout) findViewById(R.id.trending_linear21);
        trendinglinear[21] = (LinearLayout) findViewById(R.id.trending_linear22);
        trendinglinear[22] = (LinearLayout) findViewById(R.id.trending_linear23);
        trendinglinear[23] = (LinearLayout) findViewById(R.id.trending_linear24);


        onsalelinear[0] = (LinearLayout) findViewById(R.id.onsale_linear1);
        onsalelinear[1] = (LinearLayout) findViewById(R.id.onsale_linear2);
        onsalelinear[2] = (LinearLayout) findViewById(R.id.onsale_linear3);
        onsalelinear[3] = (LinearLayout) findViewById(R.id.onsale_linear4);
        onsalelinear[4] = (LinearLayout) findViewById(R.id.onsale_linear5);
        onsalelinear[5] = (LinearLayout) findViewById(R.id.onsale_linear6);
        onsalelinear[6] = (LinearLayout) findViewById(R.id.onsale_linear7);
        onsalelinear[7] = (LinearLayout) findViewById(R.id.onsale_linear8);
        onsalelinear[8] = (LinearLayout) findViewById(R.id.onsale_linear9);
        onsalelinear[9] = (LinearLayout) findViewById(R.id.onsale_linear10);
        onsalelinear[10] = (LinearLayout) findViewById(R.id.onsale_linear11);
        onsalelinear[11] = (LinearLayout) findViewById(R.id.onsale_linear12);
        onsalelinear[12] = (LinearLayout) findViewById(R.id.onsale_linear13);
        onsalelinear[13] = (LinearLayout) findViewById(R.id.onsale_linear14);
        onsalelinear[14] = (LinearLayout) findViewById(R.id.onsale_linear15);
        onsalelinear[15] = (LinearLayout) findViewById(R.id.onsale_linear16);
        onsalelinear[16] = (LinearLayout) findViewById(R.id.onsale_linear17);
        onsalelinear[17] = (LinearLayout) findViewById(R.id.onsale_linear18);
        onsalelinear[18] = (LinearLayout) findViewById(R.id.onsale_linear19);
        onsalelinear[19] = (LinearLayout) findViewById(R.id.onsale_linear20);
        onsalelinear[20] = (LinearLayout) findViewById(R.id.onsale_linear21);
        onsalelinear[21] = (LinearLayout) findViewById(R.id.onsale_linear22);
        onsalelinear[22] = (LinearLayout) findViewById(R.id.onsale_linear23);
        onsalelinear[23] = (LinearLayout) findViewById(R.id.onsale_linear24);

        bestsellerslinear[0] = (LinearLayout) findViewById(R.id.bestsellers_linear1);
        bestsellerslinear[1] = (LinearLayout) findViewById(R.id.bestsellers_linear2);
        bestsellerslinear[2] = (LinearLayout) findViewById(R.id.bestsellers_linear3);
        bestsellerslinear[3] = (LinearLayout) findViewById(R.id.bestsellers_linear4);
        bestsellerslinear[4] = (LinearLayout) findViewById(R.id.bestsellers_linear5);
        bestsellerslinear[5] = (LinearLayout) findViewById(R.id.bestsellers_linear6);
        bestsellerslinear[6] = (LinearLayout) findViewById(R.id.bestsellers_linear7);
        bestsellerslinear[7] = (LinearLayout) findViewById(R.id.bestsellers_linear8);
        bestsellerslinear[8] = (LinearLayout) findViewById(R.id.bestsellers_linear9);
        bestsellerslinear[9] = (LinearLayout) findViewById(R.id.bestsellers_linear10);
        bestsellerslinear[10] = (LinearLayout) findViewById(R.id.bestsellers_linear11);
        bestsellerslinear[11] = (LinearLayout) findViewById(R.id.bestsellers_linear12);
        bestsellerslinear[12] = (LinearLayout) findViewById(R.id.bestsellers_linear13);
        bestsellerslinear[13] = (LinearLayout) findViewById(R.id.bestsellers_linear14);
        bestsellerslinear[14] = (LinearLayout) findViewById(R.id.bestsellers_linear15);
        bestsellerslinear[15] = (LinearLayout) findViewById(R.id.bestsellers_linear16);
        bestsellerslinear[16] = (LinearLayout) findViewById(R.id.bestsellers_linear17);
        bestsellerslinear[17] = (LinearLayout) findViewById(R.id.bestsellers_linear18);
        bestsellerslinear[18] = (LinearLayout) findViewById(R.id.bestsellers_linear19);
        bestsellerslinear[19] = (LinearLayout) findViewById(R.id.bestsellers_linear20);
        bestsellerslinear[20] = (LinearLayout) findViewById(R.id.bestsellers_linear21);
        bestsellerslinear[21] = (LinearLayout) findViewById(R.id.bestsellers_linear22);
        bestsellerslinear[22] = (LinearLayout) findViewById(R.id.bestsellers_linear23);
        bestsellerslinear[23] = (LinearLayout) findViewById(R.id.bestsellers_linear24);

        linearLayouts[0] = (LinearLayout) findViewById(R.id.recently_linear1);
        linearLayouts[1] = (LinearLayout) findViewById(R.id.recently_linear2);
        linearLayouts[2] = (LinearLayout) findViewById(R.id.recently_linear3);
        linearLayouts[3] = (LinearLayout) findViewById(R.id.recently_linear4);
        linearLayouts[4] = (LinearLayout) findViewById(R.id.recently_linear5);
        linearLayouts[5] = (LinearLayout) findViewById(R.id.recently_linear6);
        linearLayouts[6] = (LinearLayout) findViewById(R.id.recently_linear7);
        linearLayouts[7] = (LinearLayout) findViewById(R.id.recently_linear8);
        linearLayouts[8] = (LinearLayout) findViewById(R.id.recently_linear9);
        linearLayouts[9] = (LinearLayout) findViewById(R.id.recently_linear10);


        osname[0] = (TextView) findViewById(R.id.home_newproducts_name1);
        osname[1] = (TextView) findViewById(R.id.home_newproducts_name2);
        osname[2] = (TextView) findViewById(R.id.home_newproducts_name3);
        osname[3] = (TextView) findViewById(R.id.home_newproducts_name4);
        osname[4] = (TextView) findViewById(R.id.home_newproducts_name5);
        osname[5] = (TextView) findViewById(R.id.home_newproducts_name6);
        osname[6] = (TextView) findViewById(R.id.home_newproducts_name7);
        osname[7] = (TextView) findViewById(R.id.home_newproducts_name8);
        osname[8] = (TextView) findViewById(R.id.home_newproducts_name9);
        osname[9] = (TextView) findViewById(R.id.home_newproducts_name10);
        osname[10] = (TextView) findViewById(R.id.home_newproducts_name11);
        osname[11] = (TextView) findViewById(R.id.home_newproducts_name12);
        osname[12] = (TextView) findViewById(R.id.home_newproducts_name13);
        osname[13] = (TextView) findViewById(R.id.home_newproducts_name14);
        osname[14] = (TextView) findViewById(R.id.home_newproducts_name15);
        osname[15] = (TextView) findViewById(R.id.home_newproducts_name16);
        osname[16] = (TextView) findViewById(R.id.home_newproducts_name17);
        osname[17] = (TextView) findViewById(R.id.home_newproducts_name18);
        osname[18] = (TextView) findViewById(R.id.home_newproducts_name19);
        osname[19] = (TextView) findViewById(R.id.home_newproducts_name20);
        osname[20] = (TextView) findViewById(R.id.home_newproducts_name21);
        osname[21] = (TextView) findViewById(R.id.home_newproducts_name22);
        osname[22] = (TextView) findViewById(R.id.home_newproducts_name23);
        osname[23] = (TextView) findViewById(R.id.home_newproducts_name24);
        osname[24] = (TextView) findViewById(R.id.home_newproducts_name25);

        bsname[0] = (TextView) findViewById(R.id.home_bestsellers_name1);
        bsname[1] = (TextView) findViewById(R.id.home_bestsellers_name2);
        bsname[2] = (TextView) findViewById(R.id.home_bestsellers_name3);
        bsname[3] = (TextView) findViewById(R.id.home_bestsellers_name4);
        bsname[4] = (TextView) findViewById(R.id.home_bestsellers_name5);
        bsname[5] = (TextView) findViewById(R.id.home_bestsellers_name6);
        bsname[6] = (TextView) findViewById(R.id.home_bestsellers_name7);
        bsname[7] = (TextView) findViewById(R.id.home_bestsellers_name8);
        bsname[8] = (TextView) findViewById(R.id.home_bestsellers_name9);
        bsname[9] = (TextView) findViewById(R.id.home_bestsellers_name10);
        bsname[10] = (TextView) findViewById(R.id.home_bestsellers_name11);
        bsname[11] = (TextView) findViewById(R.id.home_bestsellers_name12);
        bsname[12] = (TextView) findViewById(R.id.home_bestsellers_name13);
        bsname[13] = (TextView) findViewById(R.id.home_bestsellers_name14);
        bsname[14] = (TextView) findViewById(R.id.home_bestsellers_name15);
        bsname[15] = (TextView) findViewById(R.id.home_bestsellers_name16);
        bsname[16] = (TextView) findViewById(R.id.home_bestsellers_name17);
        bsname[17] = (TextView) findViewById(R.id.home_bestsellers_name18);
        bsname[18] = (TextView) findViewById(R.id.home_bestsellers_name19);
        bsname[19] = (TextView) findViewById(R.id.home_bestsellers_name20);
        bsname[20] = (TextView) findViewById(R.id.home_bestsellers_name21);
        bsname[21] = (TextView) findViewById(R.id.home_bestsellers_name22);
        bsname[22] = (TextView) findViewById(R.id.home_bestsellers_name23);
        bsname[23] = (TextView) findViewById(R.id.home_bestsellers_name24);


        recently_image[0] = (ImageView) findViewById(R.id.recently_image1);
        recently_image[1] = (ImageView) findViewById(R.id.recently_image2);
        recently_image[2] = (ImageView) findViewById(R.id.recently_image3);
        recently_image[3] = (ImageView) findViewById(R.id.recently_image4);
        recently_image[4] = (ImageView) findViewById(R.id.recently_image5);
        recently_image[5] = (ImageView) findViewById(R.id.recently_image6);
        recently_image[6] = (ImageView) findViewById(R.id.recently_image7);
        recently_image[7] = (ImageView) findViewById(R.id.recently_image8);
        recently_image[8] = (ImageView) findViewById(R.id.recently_image9);
        recently_image[9] = (ImageView) findViewById(R.id.recently_image10);

        recently_name[0] = (TextView) findViewById(R.id.recently_name1);
        recently_name[1] = (TextView) findViewById(R.id.recently_name2);
        recently_name[2] = (TextView) findViewById(R.id.recently_name3);
        recently_name[3] = (TextView) findViewById(R.id.recently_name4);
        recently_name[4] = (TextView) findViewById(R.id.recently_name5);
        recently_name[5] = (TextView) findViewById(R.id.recently_name6);
        recently_name[6] = (TextView) findViewById(R.id.recently_name7);
        recently_name[7] = (TextView) findViewById(R.id.recently_name8);
        recently_name[8] = (TextView) findViewById(R.id.recently_name9);
        recently_name[9] = (TextView) findViewById(R.id.recently_name10);


        recently_price[0] = (TextView) findViewById(R.id.recently_price1);
        recently_price[1] = (TextView) findViewById(R.id.recently_price2);
        recently_price[2] = (TextView) findViewById(R.id.recently_price3);
        recently_price[3] = (TextView) findViewById(R.id.recently_price4);
        recently_price[4] = (TextView) findViewById(R.id.recently_price5);
        recently_price[5] = (TextView) findViewById(R.id.recently_price6);
        recently_price[6] = (TextView) findViewById(R.id.recently_price7);
        recently_price[7] = (TextView) findViewById(R.id.recently_price8);
        recently_price[8] = (TextView) findViewById(R.id.recently_price9);
        recently_price[9] = (TextView) findViewById(R.id.recently_price10);


        osprice[0] = (TextView) findViewById(R.id.home_newproducts_price1);
        osprice[1] = (TextView) findViewById(R.id.home_newproducts_price2);
        osprice[2] = (TextView) findViewById(R.id.home_newproducts_price3);
        osprice[3] = (TextView) findViewById(R.id.home_newproducts_price4);
        osprice[4] = (TextView) findViewById(R.id.home_newproducts_price5);
        osprice[5] = (TextView) findViewById(R.id.home_newproducts_price6);
        osprice[6] = (TextView) findViewById(R.id.home_newproducts_price7);
        osprice[7] = (TextView) findViewById(R.id.home_newproducts_price8);
        osprice[8] = (TextView) findViewById(R.id.home_newproducts_price9);
        osprice[9] = (TextView) findViewById(R.id.home_newproducts_price10);
        osprice[10] = (TextView) findViewById(R.id.home_newproducts_price11);
        osprice[11] = (TextView) findViewById(R.id.home_newproducts_price12);
        osprice[12] = (TextView) findViewById(R.id.home_newproducts_price13);
        osprice[13] = (TextView) findViewById(R.id.home_newproducts_price14);
        osprice[14] = (TextView) findViewById(R.id.home_newproducts_price15);
        osprice[15] = (TextView) findViewById(R.id.home_newproducts_price16);
        osprice[16] = (TextView) findViewById(R.id.home_newproducts_price17);
        osprice[17] = (TextView) findViewById(R.id.home_newproducts_price18);
        osprice[18] = (TextView) findViewById(R.id.home_newproducts_price19);
        osprice[19] = (TextView) findViewById(R.id.home_newproducts_price20);
        osprice[20] = (TextView) findViewById(R.id.home_newproducts_price21);
        osprice[21] = (TextView) findViewById(R.id.home_newproducts_price22);
        osprice[22] = (TextView) findViewById(R.id.home_newproducts_price23);
        osprice[23] = (TextView) findViewById(R.id.home_newproducts_price24);
        osprice[24] = (TextView) findViewById(R.id.home_newproducts_price25);


        osimage[0] = (ImageView) findViewById(R.id.home_newproducts_img1);
        osimage[1] = (ImageView) findViewById(R.id.home_newproducts_img2);
        osimage[2] = (ImageView) findViewById(R.id.home_newproducts_img3);
        osimage[3] = (ImageView) findViewById(R.id.home_newproducts_img4);
        osimage[4] = (ImageView) findViewById(R.id.home_newproducts_img5);
        osimage[5] = (ImageView) findViewById(R.id.home_newproducts_img6);
        osimage[6] = (ImageView) findViewById(R.id.home_newproducts_img7);
        osimage[7] = (ImageView) findViewById(R.id.home_newproducts_img8);
        osimage[8] = (ImageView) findViewById(R.id.home_newproducts_img9);
        osimage[9] = (ImageView) findViewById(R.id.home_newproducts_img10);
        osimage[10] = (ImageView) findViewById(R.id.home_newproducts_img11);
        osimage[11] = (ImageView) findViewById(R.id.home_newproducts_img12);
        osimage[12] = (ImageView) findViewById(R.id.home_newproducts_img13);
        osimage[13] = (ImageView) findViewById(R.id.home_newproducts_img14);
        osimage[14] = (ImageView) findViewById(R.id.home_newproducts_img15);
        osimage[15] = (ImageView) findViewById(R.id.home_newproducts_img16);
        osimage[16] = (ImageView) findViewById(R.id.home_newproducts_img17);
        osimage[17] = (ImageView) findViewById(R.id.home_newproducts_img18);
        osimage[18] = (ImageView) findViewById(R.id.home_newproducts_img19);
        osimage[19] = (ImageView) findViewById(R.id.home_newproducts_img20);
        osimage[20] = (ImageView) findViewById(R.id.home_newproducts_img21);
        osimage[21] = (ImageView) findViewById(R.id.home_newproducts_img22);
        osimage[22] = (ImageView) findViewById(R.id.home_newproducts_img23);
        osimage[23] = (ImageView) findViewById(R.id.home_newproducts_img24);
        osimage[24] = (ImageView) findViewById(R.id.home_newproducts_img25);


        bsprice[0] = (TextView) findViewById(R.id.home_bestsellers_price1);
        bsprice[1] = (TextView) findViewById(R.id.home_bestsellers_price2);
        bsprice[2] = (TextView) findViewById(R.id.home_bestsellers_price3);
        bsprice[3] = (TextView) findViewById(R.id.home_bestsellers_price4);
        bsprice[4] = (TextView) findViewById(R.id.home_bestsellers_price5);
        bsprice[5] = (TextView) findViewById(R.id.home_bestsellers_price6);
        bsprice[6] = (TextView) findViewById(R.id.home_bestsellers_price7);
        bsprice[7] = (TextView) findViewById(R.id.home_bestsellers_price8);
        bsprice[8] = (TextView) findViewById(R.id.home_bestsellers_price9);
        bsprice[9] = (TextView) findViewById(R.id.home_bestsellers_price10);
        bsprice[10] = (TextView) findViewById(R.id.home_bestsellers_price11);
        bsprice[11] = (TextView) findViewById(R.id.home_bestsellers_price12);
        bsprice[12] = (TextView) findViewById(R.id.home_bestsellers_price13);
        bsprice[13] = (TextView) findViewById(R.id.home_bestsellers_price14);
        bsprice[14] = (TextView) findViewById(R.id.home_bestsellers_price15);
        bsprice[15] = (TextView) findViewById(R.id.home_bestsellers_price16);
        bsprice[16] = (TextView) findViewById(R.id.home_bestsellers_price17);
        bsprice[17] = (TextView) findViewById(R.id.home_bestsellers_price18);
        bsprice[18] = (TextView) findViewById(R.id.home_bestsellers_price19);
        bsprice[19] = (TextView) findViewById(R.id.home_bestsellers_price20);
        bsprice[20] = (TextView) findViewById(R.id.home_bestsellers_price21);
        bsprice[21] = (TextView) findViewById(R.id.home_bestsellers_price22);
        bsprice[22] = (TextView) findViewById(R.id.home_bestsellers_price23);
        bsprice[23] = (TextView) findViewById(R.id.home_bestsellers_price24);


        bsimage[0] = (ImageView) findViewById(R.id.home_bestsellers_image1);
        bsimage[1] = (ImageView) findViewById(R.id.home_bestsellers_image2);
        bsimage[2] = (ImageView) findViewById(R.id.home_bestsellers_image3);
        bsimage[3] = (ImageView) findViewById(R.id.home_bestsellers_image4);
        bsimage[4] = (ImageView) findViewById(R.id.home_bestsellers_image5);
        bsimage[5] = (ImageView) findViewById(R.id.home_bestsellers_image6);
        bsimage[6] = (ImageView) findViewById(R.id.home_bestsellers_image7);
        bsimage[7] = (ImageView) findViewById(R.id.home_bestsellers_image8);
        bsimage[8] = (ImageView) findViewById(R.id.home_bestsellers_image9);
        bsimage[9] = (ImageView) findViewById(R.id.home_bestsellers_image10);
        bsimage[10] = (ImageView) findViewById(R.id.home_bestsellers_image11);
        bsimage[11] = (ImageView) findViewById(R.id.home_bestsellers_image12);
        bsimage[12] = (ImageView) findViewById(R.id.home_bestsellers_image13);
        bsimage[13] = (ImageView) findViewById(R.id.home_bestsellers_image14);
        bsimage[14] = (ImageView) findViewById(R.id.home_bestsellers_image15);
        bsimage[15] = (ImageView) findViewById(R.id.home_bestsellers_image16);
        bsimage[16] = (ImageView) findViewById(R.id.home_bestsellers_image17);
        bsimage[17] = (ImageView) findViewById(R.id.home_bestsellers_image18);
        bsimage[18] = (ImageView) findViewById(R.id.home_bestsellers_image19);
        bsimage[19] = (ImageView) findViewById(R.id.home_bestsellers_image20);
        bsimage[20] = (ImageView) findViewById(R.id.home_bestsellers_image21);
        bsimage[21] = (ImageView) findViewById(R.id.home_bestsellers_image22);
        bsimage[22] = (ImageView) findViewById(R.id.home_bestsellers_image23);
        bsimage[23] = (ImageView) findViewById(R.id.home_bestsellers_image24);


        ename1 = (TextView) findViewById(R.id.home_electronics_name1);
        ename2 = (TextView) findViewById(R.id.home_electronics_name2);
        ename3 = (TextView) findViewById(R.id.home_electronics_name3);
        ename4 = (TextView) findViewById(R.id.home_electronics_name4);
        ename5 = (TextView) findViewById(R.id.home_electronics_name5);

        eprice1 = (TextView) findViewById(R.id.home_electronics_price1);
        eprice2 = (TextView) findViewById(R.id.home_electronics_price2);
        eprice3 = (TextView) findViewById(R.id.home_electronics_price3);
        eprice4 = (TextView) findViewById(R.id.home_electronics_price4);
        eprice5 = (TextView) findViewById(R.id.home_electronics_price5);

        eimage1 = (ImageView) findViewById(R.id.home_electronics_image1);
        eimage2 = (ImageView) findViewById(R.id.home_electronics_image2);
        eimage3 = (ImageView) findViewById(R.id.home_electronics_image3);
        eimage4 = (ImageView) findViewById(R.id.home_electronics_image4);
        eimage5 = (ImageView) findViewById(R.id.home_electronics_image5);

        tname[0] = (TextView) findViewById(R.id.home_trending_name1);
        tname[1] = (TextView) findViewById(R.id.home_trending_name2);
        tname[2] = (TextView) findViewById(R.id.home_trending_name3);
        tname[3] = (TextView) findViewById(R.id.home_trending_name4);
        tname[4] = (TextView) findViewById(R.id.home_trending_name5);
        tname[5] = (TextView) findViewById(R.id.home_trending_name6);
        tname[6] = (TextView) findViewById(R.id.home_trending_name7);
        tname[7] = (TextView) findViewById(R.id.home_trending_name8);
        tname[8] = (TextView) findViewById(R.id.home_trending_name9);
        tname[9] = (TextView) findViewById(R.id.home_trending_name10);
        tname[10] = (TextView) findViewById(R.id.home_trending_name11);
        tname[11] = (TextView) findViewById(R.id.home_trending_name12);
        tname[12] = (TextView) findViewById(R.id.home_trending_name13);
        tname[13] = (TextView) findViewById(R.id.home_trending_name14);
        tname[14] = (TextView) findViewById(R.id.home_trending_name15);
        tname[15] = (TextView) findViewById(R.id.home_trending_name16);
        tname[16] = (TextView) findViewById(R.id.home_trending_name17);
        tname[17] = (TextView) findViewById(R.id.home_trending_name18);
        tname[18] = (TextView) findViewById(R.id.home_trending_name19);
        tname[19] = (TextView) findViewById(R.id.home_trending_name20);
        tname[20] = (TextView) findViewById(R.id.home_trending_name21);
        tname[21] = (TextView) findViewById(R.id.home_trending_name22);
        tname[22] = (TextView) findViewById(R.id.home_trending_name23);
        tname[23] = (TextView) findViewById(R.id.home_trending_name24);

        tprice[0] = (TextView) findViewById(R.id.home_trending_price1);
        tprice[1] = (TextView) findViewById(R.id.home_trending_price2);
        tprice[2] = (TextView) findViewById(R.id.home_trending_price3);
        tprice[3] = (TextView) findViewById(R.id.home_trending_price4);
        tprice[4] = (TextView) findViewById(R.id.home_trending_price5);
        tprice[5] = (TextView) findViewById(R.id.home_trending_price6);
        tprice[6] = (TextView) findViewById(R.id.home_trending_price7);
        tprice[7] = (TextView) findViewById(R.id.home_trending_price8);
        tprice[8] = (TextView) findViewById(R.id.home_trending_price9);
        tprice[9] = (TextView) findViewById(R.id.home_trending_price10);
        tprice[10] = (TextView) findViewById(R.id.home_trending_price11);
        tprice[11] = (TextView) findViewById(R.id.home_trending_price12);
        tprice[12] = (TextView) findViewById(R.id.home_trending_price13);
        tprice[13] = (TextView) findViewById(R.id.home_trending_price14);
        tprice[14] = (TextView) findViewById(R.id.home_trending_price15);
        tprice[15] = (TextView) findViewById(R.id.home_trending_price16);
        tprice[16] = (TextView) findViewById(R.id.home_trending_price17);
        tprice[17] = (TextView) findViewById(R.id.home_trending_price18);
        tprice[18] = (TextView) findViewById(R.id.home_trending_price19);
        tprice[19] = (TextView) findViewById(R.id.home_trending_price20);
        tprice[20] = (TextView) findViewById(R.id.home_trending_price21);
        tprice[21] = (TextView) findViewById(R.id.home_trending_price22);
        tprice[22] = (TextView) findViewById(R.id.home_trending_price23);
        tprice[23] = (TextView) findViewById(R.id.home_trending_price24);

        timage[0] = (ImageView) findViewById(R.id.home_trending_image1);
        timage[1] = (ImageView) findViewById(R.id.home_trending_image2);
        timage[2] = (ImageView) findViewById(R.id.home_trending_image3);
        timage[3] = (ImageView) findViewById(R.id.home_trending_image4);
        timage[4] = (ImageView) findViewById(R.id.home_trending_image5);
        timage[5] = (ImageView) findViewById(R.id.home_trending_image6);
        timage[6] = (ImageView) findViewById(R.id.home_trending_image7);
        timage[7] = (ImageView) findViewById(R.id.home_trending_image8);
        timage[8] = (ImageView) findViewById(R.id.home_trending_image9);
        timage[9] = (ImageView) findViewById(R.id.home_trending_image10);
        timage[10] = (ImageView) findViewById(R.id.home_trending_image11);
        timage[11] = (ImageView) findViewById(R.id.home_trending_image12);
        timage[12] = (ImageView) findViewById(R.id.home_trending_image13);
        timage[13] = (ImageView) findViewById(R.id.home_trending_image14);
        timage[14] = (ImageView) findViewById(R.id.home_trending_image15);
        timage[15] = (ImageView) findViewById(R.id.home_trending_image16);
        timage[16] = (ImageView) findViewById(R.id.home_trending_image17);
        timage[17] = (ImageView) findViewById(R.id.home_trending_image18);
        timage[18] = (ImageView) findViewById(R.id.home_trending_image19);
        timage[19] = (ImageView) findViewById(R.id.home_trending_image20);
        timage[20] = (ImageView) findViewById(R.id.home_trending_image21);
        timage[21] = (ImageView) findViewById(R.id.home_trending_image22);
        timage[22] = (ImageView) findViewById(R.id.home_trending_image23);
        timage[23] = (ImageView) findViewById(R.id.home_trending_image24);

        // newproducts=(LinearLayout)findViewById(R.id.home_onsale);


        onSalePage = 1;
        trendingPage = 1;
        new GetOnSale(onSalePage, true).executeOnExecutor(MyApplication.threadPoolExecutor);
        new GetTrending(trendingPage,true).executeOnExecutor(MyApplication.threadPoolExecutor);

        new GetBestSellers().executeOnExecutor(MyApplication.threadPoolExecutor);


        new GetProducts(arrayList).executeOnExecutor(MyApplication.threadPoolExecutor);
        //new PostData().executeOnExecutor(MyApplication.threadPoolExecutor);


        drawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.home_fragment_navigation_drawer);
        drawerFragment.setUp(R.id.home_fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.home_drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        drawerFragment.onDestroy();


        ImageButton shopbycategories = (ImageButton) findViewById(R.id.home_shopbycategories_button);
        shopbycategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(HomeActivity.this, CategoriesActivity.class);
                startActivity(myintent);
                //finish();
            }
        });


        ImageButton search = (ImageButton) findViewById(R.id.home_search_button);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(HomeActivity.this, ProductSearchActivity.class);
                myintent.putExtra("fromSubCategory", "0");
                startActivity(myintent);
            }
        });


        mOnSaleRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dx > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        Log.v("...", "Last Item Wow !");
                        //Do pagination.. i.e. fetch new data

                        if (shouldFetchOnSale) {
                            onSalePage++;
                            new GetOnSale(onSalePage, false).executeOnExecutor(MyApplication.threadPoolExecutor);
                        }

                    }
                }
            }
        });

        mTrendingRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dx > 0) //check for scroll down
                {
                    visibleItemCountTrending = mTrendingLayoutManager.getChildCount();
                    totalItemCountTrending = mTrendingLayoutManager.getItemCount();
                    pastVisibleItemsTrending = mTrendingLayoutManager.findFirstVisibleItemPosition();

                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        Log.v("...", "Last Item Wow !");
                        //Do pagination.. i.e. fetch new data

                        if (shouldFetchTrending) {
                            trendingPage++;
                            new GetTrending(trendingPage, false).executeOnExecutor(MyApplication.threadPoolExecutor);
                        }

                    }
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            moveTaskToBack(true);
            //HomeActivity.this.invalidateOptionsMenu();
        }
    }

    //unused
    public boolean isOnline() {
        boolean var = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            var = true;
        }
        return var;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        HomeActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    //old code
    public void getPage(View v) {
        v.getTag();
        // Log.e("V", String.valueOf(v.getTag()));
        Intent myintent = new Intent(HomeActivity.this, ProductPageActivity.class);
        //myintent.putExtra("NewProducts", NewProducts);
        myintent.putExtra("Tag", (String) v.getTag());
        //myintent.putExtra("BestSellers", BestSellers);
        myintent.putExtra("Electronics", Electronics);
        myintent.putExtra("Sports", Sports);
       /* myintent.putExtra("Books", Books);
        myintent.putExtra("Music", Music);
        myintent.putExtra("Apparel", Apparel);*/
        startActivity(myintent);
    }

    public void getRecently(View v) {
        v.getTag();
        if (v.getTag() != null) {
            Intent myintent = new Intent(HomeActivity.this, ProductPageActivity.class);
            myintent.putExtra("Prodid", (String) v.getTag());
            myintent.putExtra("FromHome", "Home");
            startActivity(myintent);
        }
    }

    public void getProductpage(View v) {

       /* for (int i = 0; i < NewProducts.size(); ++i) {
            v.setTag(i, NewProducts.get(i).getProduct_id());
        }*/
        v.getTag();
        // Log.e("View", String.valueOf(v.getTag()));
        if (v.getTag() != null) {
            Intent myintent = new Intent(HomeActivity.this, ProductPageActivity.class);
            //myintent.putExtra("NewProducts", NewProducts);
            myintent.putExtra("Prodid", (String) v.getTag());
            myintent.putExtra("FromHome", "Home");
            startActivity(myintent);
            // finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View cart = findViewById(R.id.action_cart);

                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("HomeActivity", e.toString());
                    }

                    if (count == null || (count.equals("null"))) {
                        //Log.e("If", "if");
                        count = "0";

                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        // Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(HomeActivity.this, cart);
                        badge.setText(count);
                        badge.show();
                    }
                    //HomeActivity.this.invalidateOptionsMenu();
                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:

                Uri uri = Uri.parse("market://details?id=com.dealmaar.customer");
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    this.startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, " Sorry, Not able to open!", Toast.LENGTH_SHORT).show();
                }
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:

                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, SigninActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    progressDialog = new ProgressDialog(HomeActivity.this);
                    progressDialog.setMessage("Processing logout...");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(false);
                    progressDialog.show();

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    //Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();
                    progressDialog.dismiss();
                    break;

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }


        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_cart:
                Intent cartintent = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(cartintent);
                // finish();
                return true;


            case R.id.action_posts:
                Intent postsintent = new Intent(HomeActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }

    private class GetImages extends AsyncTask<Void, Void, Void> {

        StringBuffer stringBuffer = new StringBuffer("");
        JSONObject jArray = new JSONObject();
        //StringBuffer stringBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;

        @Override
        protected Void doInBackground(Void... params) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Banners");
                // Log.e("url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Banner1-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Banner1-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //Log.e("Test1", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }

            }
            try {
                JSONObject dataObj = new JSONObject(String.valueOf(stringBuffer));
                // Log.e("Json obj", String.valueOf(jsonObject));
                jArray = dataObj.put("Image", "true");
                // Log.e("Image array", String.valueOf(productsArray));
             /* for(int i=0;i<productsArray.length();++i)
              {
                  jsonObject=productsArray.getJSONObject(String.valueOf(i));
                  Log.e("image"," "+jsonObject);
              }*/

                Iterator<String> keys = dataObj.keys();
                while (keys.hasNext()) {
                    //Log.e("JArray in for loop ", String.valueOf(productsArray));
                    String key = keys.next();
                    //Log.e("prod key",key);
                    Home home = new Home();
                    JSONObject jobj2 = dataObj.getJSONObject(key);
                    home.setProduct_image(jobj2.getString("image_path"));

                    if (jobj2.getString("status").equalsIgnoreCase("A")) {
                        images.add(home);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                CustomAdapter adapter = new CustomAdapter(HomeActivity.this, images);
                viewPager.setAdapter(adapter);
            } catch (Exception e) {
                Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private class GetImages1 extends AsyncTask<Void, Void, Void> {


        StringBuffer stringBuffer = new StringBuffer("");
        JSONObject jArray = new JSONObject();
        //StringBuffer stringBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;

        @Override
        protected Void doInBackground(Void... params) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Banners?mid_banner=on");
                // Log.e("url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Banner1-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Banner1-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //Log.e("Test1", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }

            }
            try {
                JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                // Log.e("Json obj", String.valueOf(jsonObject));
                jArray = jsonObject.put("Image", "true");
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    //Log.e("JArray in for loop ", String.valueOf(productsArray));
                    String key = keys.next();
                    //Log.e("prod key",key);
                    Home home = new Home();
                    JSONObject jobj2 = jsonObject.getJSONObject(key);
                    home.setProduct_image(jobj2.getString("image_path"));
                    //  pro.setProduct_image(jobj2.getString("img_path"));

                    if (jobj2.getString("status").equalsIgnoreCase("A")) {
                        images1.add(home);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Log.e("images", images1.get(0).getProduct_image());
            try {
                MidBannerAdapter adapter = new MidBannerAdapter(HomeActivity.this, images1);
                viewPager1.setAdapter(adapter);
            } catch (Exception e) {
                //Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private class GetProducts extends AsyncTask<ArrayList, Void, Void> {
        ArrayList p_id;

        public GetProducts(ArrayList prod_id) {
            this.p_id = prod_id;
        }


        @Override

        protected Void doInBackground(ArrayList... params) {
            // StringBuffer stringBuffer = new StringBuffer("");
            JSONObject jArray = new JSONObject();
            //StringBuffer stringBuffer = new StringBuffer("");

            for (int i = 0; i < p_id.size(); ++i) {
                //Log.e("i", p_id.get(i).toString());
                String pid = p_id.get(i).toString();
                StringBuffer stringBuffer = new StringBuffer("");
                BufferedReader bufferedReader = null;
                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet();
                    URI uri = new URI(getString(R.string.url) + "products/" + pid);
                    // Log.e("url", String.valueOf(uri));
                    httpGet.setURI(uri);
                    httpGet.addHeader(BasicScheme.authenticate(
                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                            HTTP.UTF_8, false));
                    HttpResponse httpResponse;
                    Log.e("Products-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    httpResponse = httpClient.execute(httpGet);
                    Log.e("Products-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String readLine = bufferedReader.readLine();
                    while (readLine != null) {
                        stringBuffer.append(readLine);
                        stringBuffer.append("\n");
                        readLine = bufferedReader.readLine();
                        //Log.e("ProductsTest1", String.valueOf(stringBuffer));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {

                        }
                    }

                }

                try {
                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                    // JSONObject jobj1=new JSONObject(String.valueOf(prod));
                    // Log.e("ProductObjects", String.valueOf(jobj));
                    // Log.e("Obj", String.valueOf(jobj1));
                    jArray = jobj.put("Product", "true");
                    // Log.e("ProductsArray", String.valueOf(productsArray));


                    Productslist productslist = new Productslist("product", "my_product");
                    //jobj=productsArray.getJSONObject(String.valueOf(i));
                    productslist.setProduct_id(jobj.getString("product_id"));
                    productslist.setProduct_name(jobj.getString("product"));
                    productslist.setBase_price(jobj.getString("price"));
                    try {


                        if (jobj.getJSONObject("main_pair").getJSONObject("detailed").has("image_path")) {
                            productslist.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                        } else {
                            //Log.e("Entered ", "else");
                            productslist.setProduct_image(String.valueOf(R.drawable.ic_launcher));
                        }
                    } catch (Exception e) {

                    }
                    recentlyviewed.add(productslist);
                    // Log.e("r", String.valueOf(recentlyviewed.size()));
                    // Log.e("recently", String.valueOf(recentlyviewed.get(i).getProduct_id()));


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
            return null;
        }

        protected void onPostExecute(Void result) {
            //progressDialog.dismiss();
            super.onPostExecute(result);
            //Log.e("recentlyviewed", "RV" + String.valueOf(recentlyviewed.size()));

            try {
                TextView noitem = (TextView) findViewById(R.id.no_items);
                if (recentlyviewed.size() > 0 && recentlyviewed.size() <= 10) {

                    for (int i = 0; i < recentlyviewed.size(); ++i) {
                        NumberFormat nf = NumberFormat.getInstance();
                        nf.setMinimumFractionDigits(2);
                        nf.setMaximumFractionDigits(2);
                        linearLayouts[i].setVisibility(View.VISIBLE);
                        recently_name[i].setText(recentlyviewed.get(i).getProduct_name());
                        recently_price[i].setText(nf.format(Double.parseDouble(recentlyviewed.get(i).getBase_price())));
                        String[] rimage = new String[20];
                        ((View) recently_name[i].getParent()).setTag(recentlyviewed.get(i).getProduct_id());

                        try {
                            rimage[i] = recentlyviewed.get(i).getProduct_image();
                            // Log.e("image", rimage[i]);

                        } catch (Exception e) {

                        }
                        Picasso.with(getApplicationContext()).load(rimage[i]).error(R.drawable.ic_launcher).resize(400, 400).into(recently_image[i]);
                    }

                } else {
                    noitem.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                // Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }

    }

    private class GetOnSale extends AsyncTask<Void, Void, Void> {

        JSONArray jArray = new JSONArray();
        BufferedReader bufferedReader = null;
        AssetManager assetManager;
        private StringBuffer stringBuffer = new StringBuffer();
        private int pageNum;
        boolean value;

        public GetOnSale(int pageNum, boolean value) {
            this.pageNum = pageNum;
            this.value = value;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products?on_sale=on&items_per_page=24&page=" + pageNum);
                //Log.e("url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Onsale-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Onsale-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                //Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                // Log.e("OnSale", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }


            try {
                JSONObject prodObj = new JSONObject(String.valueOf(stringBuffer));
                // Log.e("j", String.valueOf(prodObj));
                jArray = prodObj.optJSONArray("products");
                // Log.e("On Sale Array", "" + productsArray);

//                if(!value){
//                    onSaleItemList.clear();
//                }

                if (jArray.length() < 24) {
                    shouldFetchOnSale = false;
                }

                for (int i = 0; i < jArray.length(); i++) {
                    Home home = new Home();
                    prodObj = jArray.getJSONObject(i);

                    home.setProduct_id(prodObj.getString("product_id"));
                    //Log.e("onsale1d", prodObj.getString("product_id"));
                    home.setProduct_name(prodObj.getString("product"));
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(prodObj.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(prodObj.getString("list_price"))));

                    //home.setProduct_des(prodObj.getString("full_description"));
                    // home.setProduct_shipping(prodObj.getString("shipping_params"));
                    // home.setAmount(prodObj.getString("amount"));
                    Item item = new Item();

                    item.setName(prodObj.getString("product"));
                    item.setProductId(prodObj.getString("product_id"));
                    item.setPrice(nf.format(Double.parseDouble(prodObj.getString("price"))));
                    item.setListPrice(nf.format(Double.parseDouble(prodObj.getString("list_price"))));

                    try {
                        home.setProduct_image(prodObj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                        item.setImage(prodObj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                    } catch (Exception e) {

                    }

                    if (prodObj.getString("status").equalsIgnoreCase("A") && !prodObj.getString("amount").equalsIgnoreCase("0")) {
                        NewProducts.add(home);
                        onSaleItemList.add(item);
                    }


                    //Log.e("onsale", String.valueOf(NewProducts.size()));
                }
                //Log.e("NewProducts Length", String.valueOf(NewProducts.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (value) {
                //initializing adapter
                onSaleItemAdapter = new ItemAdapter(HomeActivity.this, onSaleItemList);

                //specifying an adapter to access data, create views and replace the content
                mOnSaleRecyclerView.setAdapter(onSaleItemAdapter);
            }
            onSaleItemAdapter.notifyDataSetChanged();


            mOnSaleRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(HomeActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    ((CardView) view).setCardElevation(100);
                    Intent myintent = new Intent(HomeActivity.this, ProductPageActivity.class);
                    myintent.putExtra("Prodid", onSaleItemList.get(position).getProductId());
                    myintent.putExtra("FromHome", "Home");
                    startActivity(myintent);
                }
            }));

            try {

                for (int i = 0; i < NewProducts.size(); ++i) {
                    onsalelinear[i].setVisibility(View.VISIBLE);
                    ((View) osname[i].getParent()).setTag(NewProducts.get(i).getProduct_id());
                    String[] image = new String[30];
                    osname[i].setText(NewProducts.get(i).getProduct_name());
                    osprice[i].setText(NewProducts.get(i).getProduct_price());
                    image[i] = NewProducts.get(i).getProduct_image();
                    Picasso.with(getApplicationContext()).load(image[i]).error(R.drawable.ic_launcher).resize(400, 400).into(osimage[i]);
                }

            } catch (Exception e) {
                // Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }

    }

    private class GetBestSellers extends AsyncTask<Void, Void, Void> {
        BufferedReader bufferedReader = null;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray productsArray = new JSONArray();

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products?bestsellers=on");
                // Log.e("url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Onsale-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Onsale-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                //Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                //Log.e("BestSellers", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }


            try {
                JSONObject prodObj = new JSONObject(String.valueOf(stringBuffer));
                //Log.e("j", String.valueOf(jobj));
                productsArray = prodObj.optJSONArray("products");
                //Log.e("On Sale Array", "" + productsArray);
                for (int i = 0; i < productsArray.length(); i++) {
                    Home home = new Home();
                    prodObj = productsArray.getJSONObject(i);
                    //Log.e("Onsale objects", "" + jobj);
                    home.setProduct_id(prodObj.getString("product_id"));
                    //Log.e("onsale1d", jobj.getString("product_id"));
                    home.setProduct_name(prodObj.getString("product"));
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(prodObj.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(prodObj.getString("list_price"))));
                    //home.setProduct_des(jobj.getString("full_description"));
                    // home.setProduct_shipping(jobj.getString("shipping_params"));
                    // home.setAmount(jobj.getString("amount"));

                    try {
                        home.setProduct_image(prodObj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                    } catch (Exception e) {

                    }
                    if (prodObj.getString("status").equalsIgnoreCase("A") && !prodObj.getString("amount").equalsIgnoreCase("0")) {
                        BestSellers.add(home);
                    }
                }
                // Log.e("Length", String.valueOf(BestSellers.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                for (int i = 0; i < BestSellers.size(); ++i) {
                    bestsellerslinear[i].setVisibility(View.VISIBLE);
                    ((View) bsname[i].getParent()).setTag(BestSellers.get(i).getProduct_id());
                    bsname[i].setText(BestSellers.get(i).getProduct_name());
                    bsprice[i].setText(BestSellers.get(i).getProduct_price());
                    String[] image = new String[30];
                    image[i] = BestSellers.get(i).getProduct_image();
                    Picasso.with(getApplicationContext()).load(image[i]).error(R.drawable.ic_launcher).resize(400, 400).into(bsimage[i]);
                }
            } catch (Exception e) {
                // Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }



    private class GetTrending extends AsyncTask<Void, Void, Void> {
        BufferedReader bufferedReader = null;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();
        private int pageNum;
        private boolean value;

        public GetTrending(int pageNum, boolean value){
            this.pageNum = pageNum;
            this.value = value;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products?bestsellers=on&items_per_page=24&page=" + pageNum);

                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Trending-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Trending-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                //Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                //Log.e("BestSellers", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                JSONObject prodObjects = new JSONObject(String.valueOf(stringBuffer));
                jArray = prodObjects.optJSONArray("products");
                //Log.e("Trending", "" + productsArray);

                if (jArray.length() < 24) {
                    shouldFetchTrending = false;
                }


                for (int i = 0; i < jArray.length(); i++) {
                    Home home = new Home();
                    prodObjects = jArray.getJSONObject(i);
                    home.setProduct_id(prodObjects.getString("product_id"));
                    // Log.e("Trending id", jobj.getString("product_id"));
                    home.setProduct_name(prodObjects.getString("product"));
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(prodObjects.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(prodObjects.getString("list_price"))));
                    //home.setProduct_des(jobj.getString("full_description"));
                    //home.setProduct_shipping(jobj.getString("shipping_params"));
                    home.setAmount(prodObjects.getString("amount"));


                    Item item = new Item();

                    item.setName(prodObjects.getString("product"));
                    item.setProductId(prodObjects.getString("product_id"));
                    item.setPrice(nf.format(Double.parseDouble(prodObjects.getString("price"))));
                    item.setListPrice(nf.format(Double.parseDouble(prodObjects.getString("list_price"))));

                    item.setImage(prodObjects.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));



                    home.setProduct_image(prodObjects.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));

                    if (prodObjects.getString("status").equalsIgnoreCase("A") && !prodObjects.getString("amount").equalsIgnoreCase("0")) {
                        Sports.add(home);
                        trendingItemList.add(item);
                    }


                }
                // Log.e("Length", String.valueOf(Sports.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(value){
                trendingItemAdapter = new ItemAdapter(HomeActivity.this, trendingItemList);
                mTrendingRecyclerView.setAdapter(trendingItemAdapter);
            }
            trendingItemAdapter.notifyDataSetChanged();


            mTrendingRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(HomeActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    ((CardView) view).setCardElevation(100);
                    Intent myintent = new Intent(HomeActivity.this, ProductPageActivity.class);
                    myintent.putExtra("Prodid", trendingItemList.get(position).getProductId());
                    myintent.putExtra("FromHome", "Home");
                    startActivity(myintent);
                }
            }));

            try {
                for (int i = 0; i < Sports.size(); ++i) {
                    trendinglinear[i].setVisibility(View.VISIBLE);
                    tname[i].setText(Sports.get(i).getProduct_name());
                    tprice[i].setText(Sports.get(i).getProduct_price());
                    ((View) tname[i].getParent()).setTag(Sports.get(i).getProduct_id());
                    String[] simg = new String[25];
                    simg[i] = Sports.get(i).getProduct_image();
                    //Log.e("image", simg[i]);
                    Picasso.with(getApplicationContext()).load(simg[i]).error(R.drawable.ic_launcher).resize(400, 400).into(timage[i]);

                }
            } catch (Exception e) {
                //Toast.makeText(HomeActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }
    }
}
