package com.dealmaar.home;

import java.io.Serializable;

/**
 * Created by omm on 12/25/2015.
 */
public class Home implements Serializable {

    private String product_id;
    private String product_name;
    private String product_price;
    private String product_image;
    private String product_listprice;
    private String product_des, product_shipping, amount;


    public Home() {

    }

    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProduct_shipping() {
        return product_shipping;
    }

    public void setProduct_shipping(String product_shipping) {
        this.product_shipping = getProduct_shipping();
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_listprice() {
        return product_listprice;
    }

    public void setProduct_listprice(String product_listprice) {
        this.product_listprice = product_listprice;
    }

    public String getProduct_des() {
        return product_des;

    }

    public void setProduct_des(String product_des) {
        this.product_des = product_des;
    }
}

