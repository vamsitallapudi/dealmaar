package com.dealmaar.home;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.customer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;


/**
 * Created by OMM on 11/2/2015.
 */
public class HomeApparel extends Fragment {
    ImageView aimage1, aimage2, aimage3, aimage4, aimage5;
    TextView aprice1, aprice2, aprice3, aprice4, aprice5;
    TextView aname1, aname2, aname3, aname4, aname5;
    ArrayList<Home> Apparel = new ArrayList<>();

    public HomeApparel() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_apparel, container, false);
        aname1 = (TextView) view.findViewById(R.id.home_apparel_name1);
        aname2 = (TextView) view.findViewById(R.id.home_apparel_name2);
        aname3 = (TextView) view.findViewById(R.id.home_apparel_name3);
        aname4 = (TextView) view.findViewById(R.id.home_apparel_name4);
        aname5 = (TextView) view.findViewById(R.id.home_apparel_name5);

        aprice1 = (TextView) view.findViewById(R.id.home_apparel_price1);
        aprice2 = (TextView) view.findViewById(R.id.home_apparel_price2);
        aprice3 = (TextView) view.findViewById(R.id.home_apparel_price3);
        aprice4 = (TextView) view.findViewById(R.id.home_apparel_price4);
        aprice5 = (TextView) view.findViewById(R.id.home_apparel_price5);

        aimage1 = (ImageView) view.findViewById(R.id.home_apparel_image1);
        aimage2 = (ImageView) view.findViewById(R.id.home_apparel_image2);
        aimage3 = (ImageView) view.findViewById(R.id.home_apparel_image3);
        aimage4 = (ImageView) view.findViewById(R.id.home_apparel_image4);
        aimage5 = (ImageView) view.findViewById(R.id.home_apparel_image5);
        new GetApparel().execute();
        return view;
    }

    private class GetApparel extends AsyncTask<Void, Void, Void> {

        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();
        BufferedReader bufferedReader = null;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(getContext().getAssets().open("MainScreen/Apparel")));
                Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                Log.e("NewProducts", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                JSONArray jsonArray = new JSONArray(String.valueOf(stringBuffer));
                Log.e("j", String.valueOf(jsonArray));
                for (int i = 0; i < jsonArray.length(); i++) {
                    Home home = new Home();
                    JSONObject jobj = jsonArray.getJSONObject(i);
                    home.setProduct_id(jobj.getString("product_id"));
                    home.setProduct_name(jobj.getString("product"));
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(jobj.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(jobj.getString("list_price"))));
                    home.setProduct_des(jobj.getString("full_description"));

                    home.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                    Apparel.add(home);
                }
                Log.e("BLength", String.valueOf(Apparel.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            aname1.setText(Apparel.get(0).getProduct_name());
            aname2.setText(Apparel.get(1).getProduct_name());
            aname3.setText(Apparel.get(2).getProduct_name());
            aname4.setText(Apparel.get(3).getProduct_name());
            aname5.setText(Apparel.get(4).getProduct_name());
            aprice1.setText(Apparel.get(0).getProduct_price());
            aprice2.setText(Apparel.get(1).getProduct_price());
            aprice3.setText(Apparel.get(2).getProduct_price());
            aprice4.setText(Apparel.get(3).getProduct_price());
            aprice5.setText(Apparel.get(4).getProduct_price());
            String aimg1 = Apparel.get(0).getProduct_image();
            String aimg2 = Apparel.get(1).getProduct_image();
            String aimg3 = Apparel.get(2).getProduct_image();
            String aimg4 = Apparel.get(3).getProduct_image();
            String aimg5 = Apparel.get(4).getProduct_image();
            InputStream ais1 = null, ais2 = null, ais3 = null, ais4 = null, ais5 = null;
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/apparel/437394_010_A.jpg")
                    .into(aimage1);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/apparel/452499_010_A.jpg")
                    .into(aimage2);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/apparel/X41185_01.jpg")
                    .into(aimage3);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/apparel/476750_003_A.jpg")
                    .into(aimage4);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/apparel/102337_l.ashx.jpg")
                    .into(aimage5);
          /*  try {
                ais1=getContext().getAssets().open(aimg1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap abitmap1= BitmapFactory.decodeStream(ais1);
            aimage1.setImageBitmap(abitmap1);
            try {
                ais2=getContext().getAssets().open(aimg2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap abitmap2=BitmapFactory.decodeStream(ais2);
            aimage2.setImageBitmap(abitmap2);
            try {
                ais3=getContext().getAssets().open(aimg3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap abitmap3=BitmapFactory.decodeStream(ais3);
            aimage3.setImageBitmap(abitmap3);
            try {
                ais4=getContext().getAssets().open(aimg4);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap abitmap4=BitmapFactory.decodeStream(ais4);
            aimage4.setImageBitmap(abitmap4);
            try {
                ais5=getContext().getAssets().open(aimg5);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap abitmap5=BitmapFactory.decodeStream(ais5);
            aimage5.setImageBitmap(abitmap5);*/
        }
    }
}
