package com.dealmaar.home;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dealmaar.customer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by omm on 2/24/2016.
 */
public class CustomAdapter  extends PagerAdapter {

    Context context;
    //int[] image = {R.drawable.home_layout_laptops, R.drawable.home_layout_laptops, R.drawable.home_layout_laptops};
    ArrayList<Home> imageId;

    public CustomAdapter(Context context,ArrayList<Home> image){
        this.context = context;
        this.imageId=image;

    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.home_categorylayout, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.home_imageView);
        ViewPager viewPager=(ViewPager)viewItem.findViewById(R.id.home_viewPager);
        //imageView.setImageResource(image[position]);
        String image=imageId.get(position).getProduct_image();
        //Log.e("image",image);
        Picasso.with((Activity)context).load(image).error(R.drawable.home_layout_laptops).into(imageView);

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        //Log.e("S", String.valueOf(imageId.size()));
        return imageId.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }

}
