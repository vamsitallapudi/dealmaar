package com.dealmaar.home;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.customer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;


/**
 * Created by OMM on 11/2/2015.
 */
public class HomeElectronics extends Fragment {

    ImageView bimage1,bimage2,bimage3,bimage4,bimage5;
    TextView bprice1,bprice2,bprice3,bprice4,bprice5;
    TextView bname1,bname2,bname3,bname4,bname5;
    ArrayList<Home> Books=new ArrayList<>();
    public HomeElectronics() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.home_electronics, container, false);
        bname1=(TextView)view.findViewById(R.id.home_books_name1);
        bname2=(TextView)view.findViewById(R.id.home_books_name2);
        bname3=(TextView)view.findViewById(R.id.home_books_name3);
        bname4=(TextView)view.findViewById(R.id.home_books_name4);
        bname5=(TextView)view.findViewById(R.id.home_books_name5);

        bprice1=(TextView)view.findViewById(R.id.home_books_price1);
        bprice2=(TextView)view.findViewById(R.id.home_books_price2);
        bprice3=(TextView)view.findViewById(R.id.home_books_price3);
        bprice4=(TextView)view.findViewById(R.id.home_books_price4);
        bprice5=(TextView)view.findViewById(R.id.home_books_price5);

        bimage1=(ImageView)view.findViewById(R.id.home_books_image1);
        bimage2=(ImageView)view.findViewById(R.id.home_books_image2);
        bimage3=(ImageView)view.findViewById(R.id.home_books_image3);
        bimage4=(ImageView)view.findViewById(R.id.home_books_image4);
        bimage5=(ImageView)view.findViewById(R.id.home_books_image5);
        new GetBooks().execute();
        return view;
    }
    private class GetBooks extends AsyncTask<Void,Void,Void>
    {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();
        BufferedReader bufferedReader = null;
        @Override
        protected Void doInBackground(Void... params) {

            try {
                bufferedReader = new BufferedReader(new InputStreamReader(getContext().getAssets().open("MainScreen/Books")));
                Log.e("Hi", String.valueOf(bufferedReader));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    //stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
                Log.e("NewProducts", String.valueOf(stringBuffer));
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                JSONArray jsonArray = new JSONArray(String.valueOf(stringBuffer));
                Log.e("j", String.valueOf(jsonArray));
                for (int i = 0; i < jsonArray.length(); i++) {
                    Home home = new Home();
                    JSONObject jobj = jsonArray.getJSONObject(i);
                    home.setProduct_id(jobj.getString("product_id"));
                    home.setProduct_name(jobj.getString("product"));
                    NumberFormat nf=NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    home.setProduct_price(nf.format(Double.parseDouble(jobj.getString("price"))));
                    home.setProduct_listprice(nf.format(Double.parseDouble(jobj.getString("list_price"))));
                    home.setProduct_des(jobj.getString("full_description"));

                    home.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                    Books.add(home);
                }
                Log.e("BLength", String.valueOf(Books.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.e("bname",Books.get(0).getProduct_name());
            bname1.setText(Books.get(0).getProduct_name());
            bname2.setText(Books.get(1).getProduct_name());
            bname3.setText(Books.get(2).getProduct_name());
            bname4.setText(Books.get(3).getProduct_name());
            bname5.setText(Books.get(4).getProduct_name());

            bprice1.setText(Books.get(0).getProduct_price());
            bprice2.setText(Books.get(1).getProduct_price());
            bprice3.setText(Books.get(2).getProduct_price());
            bprice4.setText(Books.get(3).getProduct_price());
            bprice5.setText(Books.get(4).getProduct_price());

            String bimg1=Books.get(0).getProduct_image();
            String bimg2=Books.get(1).getProduct_image();
            String bimg3=Books.get(2).getProduct_image();
            String bimg4=Books.get(3).getProduct_image();
            String bimg5=Books.get(4).getProduct_image();

            InputStream bis1=null,bis2=null,bis3=null,bis4=null,bis5=null;
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/books/Main_116.jpg")
                    .into(bimage1);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/books/29336_McBride_The_Process_of_Research_in_Psychology_72ppiRGB_150pixW.jpg")
                    .into(bimage2);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/books/cat.gif")
                    .into(bimage3);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/books/url_uploaded_file_13286235024f312f8f85905.jpg")
                    .into(bimage4);
            Picasso.with(getContext())
                    .load("file:///android_asset/MainScreen/images/books/1886039720.jpg")
                    .into(bimage5);

            /*try {
                bis1=getContext().getAssets().open(bimg1);
                Log.e("Input", String.valueOf(bis1));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bbitmap1= BitmapFactory.decodeStream(bis1);

            bimage1.setImageBitmap(bbitmap1);
            try {
                bis2=getContext().getAssets().open(bimg2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bbitmap2=BitmapFactory.decodeStream(bis2);
            bimage2.setImageBitmap(bbitmap2);
            try {
                bis3=getContext().getAssets().open(bimg3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bbitmap3=BitmapFactory.decodeStream(bis3);
            bimage3.setImageBitmap(bbitmap3);

            try {
                bis4=getContext().getAssets().open(bimg4);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bbitmap4=BitmapFactory.decodeStream(bis4);
            bimage4.setImageBitmap(bbitmap4);

            try {
                bis5=getContext().getAssets().open(bimg5);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bbitmap5=BitmapFactory.decodeStream(bis5);
            bimage5.setImageBitmap(bbitmap5);
*/
        }
    }

}
