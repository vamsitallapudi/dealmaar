package com.dealmaar.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.batch.android.Batch;
import com.batch.android.Config;
import com.batch.android.PushNotificationType;
import com.dealmaar.cart.Cart;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.utilities.BitmapCache;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by omm on 11/13/2015.
 */
public class MyApplication extends MultiDexApplication implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static Executor threadPoolExecutor;
    private DatabaseHandler db;
    private MainCardModel mainCardModel;
    private ArrayList<Cart> arrayCartList = null;
    private ListView list;
    private String shippingAmount;
    private TextView cart, shipping;
    Button checkout, checkout1;
    double total;
    String count;
    ArrayList<String> productid = new ArrayList<>();


    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static MyApplication mInstance;

    private static final String TAG = "BatchSampleApplication";

    private static final String PREF_NOTIF_ALERT = "notifications_alert";
    private static final String PREF_NOTIF_SOUND = "notifications_sound";
    private static final String PREF_NOTIF_VIBRATE = "notifications_vibrate";
    private static final String PREF_NOTIF_LIGHTS = "notifications_lights";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        Log.e("My", "Application");
        Parse.enableLocalDatastore(this);
        PreferenceManager.setDefaultValues(this, R.layout.settings_notification, false);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        Batch.setConfig(new Config("56E29E1330805296F422F570809477"));//api key
        Batch.Push.setGCMSenderId("736021568503");//YOUR-GCM-SENDER-ID
        Batch.Push.setSmallIconResourceId(R.drawable.push_icon);
        Batch.Push.setLargeIcon(BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher));
        Batch.Push.setNotificationsColor(0x16225310);
        Batch.Push.setManualDisplay(true);
/*
        cart = (TextView) findViewById(R.id.cart_total);
        shipping=(TextView)findViewById(R.id.shipping_total);
        list = (ListView) findViewById(R.id.customer_cart_list);*/
        arrayCartList = new ArrayList<Cart>();
        db = new DatabaseHandler(getApplicationContext());
        mainCardModel = new MainCardModel();


        try {
            Parse.initialize(getApplicationContext(), getString(R.string.parse_app_id), getString(R.string.parse_client_key));
            //  ParseFacebookUtils.initialize(this);

        } catch (Exception e) {

        }
        int corePoolSize = 60;
        int maximumPoolSize = 80;
        int keepAliveTime = 10;

        try {
            BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(maximumPoolSize);
            threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, workQueue);
        } catch (Exception e) {

        }
        new RemoteDataTask().executeOnExecutor(MyApplication.threadPoolExecutor);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
        //new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);
        //printHashKey();
        updateNotificationSettings();
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        Intent intent = new Intent();
        intent.setAction("com.mydomain.SEND_LOG"); // see step 5.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity(intent);

        System.exit(1); // kill off the crashed app
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updateNotificationSettings();
    }

    public String getCartCount() {
        return count;
    }

    public ArrayList getProductid() {
        return productid;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new BitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }


    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


    private void updateNotificationSettings() {
        Log.i(TAG, "Updating Batch notification settings");

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        EnumSet<PushNotificationType> pushNotificationTypes = EnumSet.allOf(PushNotificationType.class);

        if (!prefs.getBoolean(PREF_NOTIF_ALERT, true)) {
            // Removing ALERT is enough, no sound or vibration will be emitted.
            pushNotificationTypes.remove(PushNotificationType.ALERT);
        }
        if (!prefs.getBoolean(PREF_NOTIF_SOUND, true)) {
            pushNotificationTypes.remove(PushNotificationType.SOUND);
        }
        if (!prefs.getBoolean(PREF_NOTIF_VIBRATE, true)) {
            pushNotificationTypes.remove(PushNotificationType.VIBRATE);
        }
        if (!prefs.getBoolean(PREF_NOTIF_LIGHTS, true)) {
            pushNotificationTypes.remove(PushNotificationType.LIGHTS);
        }

        Batch.Push.setNotificationsType(pushNotificationTypes);
    }


    public DatabaseHandler getDatabaseHandler() {
        return db;
    }

    /*public void printHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.dealmaar.customer",
                    PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {


        } catch (NoSuchAlgorithmException e) {


        }
    }
*/

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Parse.initialize(getApplicationContext(), getString(R.string.parse_app_id), getString(R.string.parse_client_key));
                PushService.setDefaultPushCallback(getApplicationContext(), HomeActivity.class);
                PushService.subscribe(getApplicationContext(), getString(R.string.key_1), HomeActivity.class);

				/*PushService.setDefaultPushCallback(getApplicationContext(), MainActivity.class);
                PushService.subscribe(getApplicationContext(),getString(R.string.key_1), MainActivity.class);*/


                PushService.startServiceIfRequired(getApplicationContext());


                String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                //ParseInstallation.getCurrentInstallation().saveInBackground();

                ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                installation.put("UniqueId", android_id);
                installation.saveInBackground();


            } catch (Exception e) {

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

    }

}
