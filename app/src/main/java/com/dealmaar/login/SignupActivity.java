package com.dealmaar.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by omm on 11/13/2015.
 */
public class SignupActivity extends AppCompatActivity {
    EditText firstname, lastname, email, password, repassword;
    String Firstname, Lastname, Email, Password, Repassword,uid;
    Button signup;
    String user;
    ProgressDialog progressDialog;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_signup);
        firstname = (EditText) findViewById(R.id.signup_firstname);
        lastname = (EditText) findViewById(R.id.signup_lastname);
        email = (EditText) findViewById(R.id.signup_email);
        password = (EditText) findViewById(R.id.signup_password);
        repassword = (EditText) findViewById(R.id.signup_repassword);
        signup = (Button) findViewById(R.id.signup_signupbtn);
        back=(Button)findViewById(R.id.back);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Firstname = firstname.getText().toString();
                Lastname = lastname.getText().toString();
                Email = email.getText().toString();
                Password = password.getText().toString();
                Repassword = repassword.getText().toString();

                if (Firstname.equals("") || Lastname.equals("") || Email.equals("")|| Password.equals("") ||Repassword.equals("")) {
                    Toast.makeText(SignupActivity.this, "Please enter your crendentials", Toast.LENGTH_SHORT).show();
                }else if (!signupEmail(Email)){
                    Toast.makeText(SignupActivity.this, "Invalid email", Toast.LENGTH_LONG).show();
                }
                else {
                    new PostUser().executeOnExecutor(MyApplication.threadPoolExecutor);
                }


               /* Intent intent=new Intent(SignupActivity.this, HomeActivity.class);
                startActivity(intent);*/
            }
        });


    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
    private class PostUser extends AsyncTask<Void,Void,Void>
    {
        JSONObject jsonObject=new JSONObject();
        BufferedReader bufferedReader=null;
        StringBuffer stringBuffer = new StringBuffer();
        String message;
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SignupActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                jsonObject.put("firstname",Firstname);
                jsonObject.put("lastname",Lastname);
                /*jsonObject.put("user_login",Firstname+""+Lastname);*/
                jsonObject.put("email",Email);
                jsonObject.put("company_id","0");
                jsonObject.put("status","A");
                jsonObject.put("password1",Password);
                jsonObject.put("password2",Repassword);
                jsonObject.put("user_type","C");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());

                URI uri = new URI(getString(R.string.url)+"users");
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("PostUser-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("PostUser-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
               // HttpEntity resultEntity = httpResponse.getEntity();
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try
            {
                JSONObject jobj=new JSONObject(String.valueOf(stringBuffer));
                if(jobj.has("message"))
                {

                    message = jobj.getString("message");
                    Log.e("msg", message);
                    user=null;
                   /* if(message.equals("Not Found") || (message.equals("Not Found: Password Invalid")))
                    {
                        user="-1001";
                    }*/
                }
                if(jobj.has("user_id")) {
                    user = jobj.getString("user_id");
                    Log.e("id", jobj.getString("user_id"));
                }


                Preferences.add(Preferences.PrefType.User_id,user,getApplicationContext());
                db=((MyApplication)getApplicationContext()).getDatabaseHandler();
                mainCardModel=new MainCardModel(Firstname);
                db.addName(mainCardModel);
                //Preferences.add(Preferences.PrefType.First_name,Firstname,getApplicationContext());
                Preferences.add(Preferences.PrefType.Last_name,Lastname,getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            uid=Preferences.getString(Preferences.PrefType.User_id,getApplicationContext());
            try {
                if (uid == null) {
                    if (message.equals("Bad Request: The username or email you have chosen already exists. Please try another one.")) {
                        Toast.makeText(SignupActivity.this, "An user exists with this mail id!", Toast.LENGTH_SHORT).show();
                    }
                    if (message.equals("Bad Request: The passwords do not match.")) {
                        Toast.makeText(SignupActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    // uid=Preferences.getString(Preferences.PrefType.User_id,getApplicationContext());
                }
            }catch (Exception e)
            {
                Toast.makeText(SignupActivity.this, "Please Check your internet connection!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class GetUsers extends AsyncTask<String,Void,Void> {
        String User_id;
        private StringBuffer stringBuffer=new StringBuffer();
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SignupActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        public GetUsers(String userid) {
            this.User_id = userid;
        }

        @Override
        protected Void doInBackground(String... params) {

            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "users/" + User_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Users-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Users-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try
            {
                JSONObject jobj=new JSONObject(String.valueOf(stringBuffer));
                String name=jobj.getString("firstname");
                Log.e("Name",name);
                db=((MyApplication)getApplicationContext()).getDatabaseHandler();
                mainCardModel=new MainCardModel(name);
                db.addName(mainCardModel);
               // Preferences.add(Preferences.PrefType.First_name,name,getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
        }
    }

    //Return true if email is valid and false if email is invalid
    private boolean signupEmail(String email) {
        String emailPattern =
                "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

}
