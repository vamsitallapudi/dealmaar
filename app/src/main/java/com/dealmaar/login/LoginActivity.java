package com.dealmaar.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;

public class LoginActivity extends AppCompatActivity implements DialogInterface.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);


        Button login = (Button) findViewById(R.id.login_signin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(LoginActivity.this, SigninActivity.class);
                startActivity(myintent);
                //finish();

            }
        });

        Button signup = (Button) findViewById(R.id.login_signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(myintent);
                //finish();
            }
        });

        Button skip = (Button) findViewById(R.id.login_skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(myintent);
            }
        });
        //finish();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
*/

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }
}
