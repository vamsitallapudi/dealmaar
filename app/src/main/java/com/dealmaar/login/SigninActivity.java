package com.dealmaar.login;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by omm on 11/13/2015.
 */
public class SigninActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 3;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 4;
    private CallbackManager mCallbackmanager;
    ProgressDialog progressDialog;
    private TextView mTextDetails;
    private String ffname, flname, user;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.e("fb", "fb login success: result " + loginResult);
            AccessToken accessToken = loginResult.getAccessToken();

            if (Profile.getCurrentProfile() == null) {


                Toast.makeText(getApplicationContext(), "Please try again !",
                        Toast.LENGTH_LONG).show();
            } else {
                Profile profile = Profile.getCurrentProfile();
                Log.e("Access", "Token" + Profile.getCurrentProfile());
                // displayMessage(profile);
                // Log.e("Success", String.valueOf(profile.getFirstName()));
                ffname = Preferences.getString(Preferences.PrefType.FUSER, getApplicationContext());
                ffname = profile.getFirstName();
                flname = profile.getLastName();
                Log.e("facebook", profile.getId());

                Email = ffname + "." + flname + "@facebook.com";
                password = ffname + "." + flname + "@facebook.com";
                new GetFLoginTask().executeOnExecutor(MyApplication.threadPoolExecutor);

            }
        }

        @Override
        public void onCancel() {
            Log.e("fb", "fb login cancelled");
        }

        @Override
        public void onError(FacebookException e) {
            Log.e("fb", "Login error");

            if (e instanceof FacebookAuthorizationException) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }
            }
        }
    };
    private static final int RC_SIGN_IN = 1;
    // Logcat tag
    private static final String TAG = ".SigninActivity";

    // Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 400;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private AccessTokenTracker mFacebookAccessTokenTracker;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    private boolean mIntentInProgress;

    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;
    private ImageButton btnSignIn;
    private Button btnSignOut, btnRevokeAccess, signin, forgotpwd;
    private ImageView imgProfilePic;
    private TextView txtName, txtEmail, email, pass;
    private LinearLayout llProfileLayout;
    private String Email, password, uid, userid;
    EditText emailId;
    private TextView tvSkip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        mCallbackmanager = CallbackManager.Factory.create();
        setContentView(R.layout.login_signin);
        email = (TextView) findViewById(R.id.login_signin_email);
        pass = (TextView) findViewById(R.id.login_signin_password);
        tvSkip = (TextView) findViewById(R.id.tv_skip);

        forgotpwd = (Button) findViewById(R.id.login_signin_forgotpwdbutton);
        //facebook.setFragment(this);
        LoginManager.getInstance().registerCallback(mCallbackmanager, mCallback);
        //info = (TextView) findViewById(R.id.profile_name);
        ImageButton login_fb = (ImageButton) findViewById(R.id.login_signin_facebook);

        //Runtime Permissions

        if (
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) ||
                        (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) ||
                        (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST);
        }


        login_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions((SigninActivity) v.getContext(), Arrays.asList("public_profile", "user_friends"));
            }
        });


        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SigninActivity.this, HomeActivity.class);
                startActivity(i);
            }
        });

        signin = (Button) findViewById(R.id.b_signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = email.getText().toString();
                password = pass.getText().toString();
                if (Email.equals("") || password.equals("")) {
                    Toast.makeText(SigninActivity.this, "Please check your credentials", Toast.LENGTH_SHORT).show();
                } else {
                    new GetLoginTask().executeOnExecutor(MyApplication.threadPoolExecutor);

                }

            }
        });
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        forgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SigninActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View alertlayout = inflater.inflate(R.layout.profile_alert_editemail, null);
                alertDialog.setView(alertlayout);

                alertDialog.setTitle("");
                emailId = (EditText) alertlayout.findViewById(R.id.profile_editemail);


                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String Eemail = emailId.getText().toString();

                        new GetPassword(Eemail).executeOnExecutor(MyApplication.threadPoolExecutor);


                    }
                });


                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        return;

                    }
                });
                alertDialog.show();

            }
        });

       /* ImageButton login_google = (ImageButton) findViewById(R.id.login_signin_google);
        login_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });*/
        Button signin_signup = (Button) findViewById(R.id.b_signup);
        signin_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(SigninActivity.this, SignupActivity.class);
                startActivity(myintent);
            }
        });
        btnSignIn = (ImageButton) findViewById(R.id.login_signin_google);
        // btnSignOut = (Button) findViewById(R.id.btn_sign_out);
        //btnRevokeAccess = (Button) findViewById(R.id.btn_revoke_access);
        imgProfilePic = (ImageView) findViewById(R.id.imgProfilePic);
        txtName = (TextView) findViewById(R.id.txtName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        llProfileLayout = (LinearLayout) findViewById(R.id.llProfile);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }



    /*protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }*/

    protected void onStop() {
        Batch.onStop(this);

        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);

        Log.e("request code", String.valueOf(requestCode));
        Log.e("result code", String.valueOf(resultCode));
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Log.e("Data", " D" + data);
            mCallbackmanager.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onConnected(Bundle arg0) {
        mSignInClicked = false;
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();

        Log.d(TAG, "User is connected");

        // Get user's information
        getProfileInformation();
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                ffname = currentPerson.getName().getGivenName();
                flname = currentPerson.getName().getFamilyName();
                Email = ffname + "." + flname + "@google.com";
                password = ffname + "." + flname + "@google.com";
                new GetFLoginTask().executeOnExecutor(MyApplication.threadPoolExecutor);

                //Log.e("Fname", ffname);
                //Log.e("Name", String.valueOf(currentPerson.getName().getFamilyName()));


                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
            }
        } catch (Exception e) {

        }

        //String un=String.valueOf(txtName);
        //Preferences.add(Preferences.PrefType.USERNAME, un, getApplicationContext());

        // Update the UI after signin
        updateUI(true);

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();

        updateUI(false);
    }

    private class GetPassword extends AsyncTask<String, Void, Void> {

        private StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        Boolean readLine;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SigninActivity.this);

        String mail;

        GetPassword(String email) {
            this.mail = email;
        }

        @Override
        protected Void doInBackground(String... params) {

            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Forgotpassword?email=" + mail);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Address-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Address-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                if (mail.equals("")) {
                    readLine = false;
                } else {
                    readLine = Boolean.valueOf(bufferedReader.readLine());
                    Log.e("readline", String.valueOf(readLine));
                }

                /*while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (Boolean.TRUE.equals(readLine))

            {
                Log.e("Enteres", "checking");
                // AlertDialog.Builder alertDialog = new AlertDialog.Builder(SigninActivity.this);
                alertDialog.setMessage("Please check your mail to reset your password");

                alertDialog.setCancelable(true);
                alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            } else

            {
                Log.e("Entered", "else");
                alertDialog.setMessage("Please enter a valid MailId");

                alertDialog.setCancelable(true);
                alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        }
    }

    public void dialog() {
        Log.e("Entered", "dialog");

    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.VISIBLE);
//            btnRevokeAccess.setVisibility(View.VISIBLE);
            // llProfileLayout.setVisibility(View.VISIBLE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
            // btnSignOut.setVisibility(View.GONE);
            // btnRevokeAccess.setVisibility(View.GONE);
            llProfileLayout.setVisibility(View.GONE);
        }
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                Log.e(TAG, "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);

                txtName.setText(personName);
                txtEmail.setText(email);
                String g_un = String.valueOf(personName);
                //Log.e("name", String.valueOf(personName));
                // Preferences.add(Preferences.PrefType.GOOGLEUSERNAME, g_un, getApplicationContext());
                // Log.e("g_un",g_un);


                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X
                personPhotoUrl = personPhotoUrl.substring(0,
                        personPhotoUrl.length() - 2)
                        + PROFILE_PIC_SIZE;

                new LoadProfileImage(imgProfilePic).execute(personPhotoUrl);

            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }

   /* @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.login_signin_google) {
            signInWithGplus();

        }


    }*/

    private void signInWithGplus() {
        mSignInClicked = true;
        if (!mGoogleApiClient.isConnecting()) {

            if (mConnectionResult != null) {

                resolveSignInError();
            } else if (mGoogleApiClient.isConnected()) {
                revokeGplusAccess();
            } else {
                Log.d(TAG, "Trying to connect to Google API");
                mGoogleApiClient.connect();
            }

            // signInWithGplus();
        }
    }

    /**
     * Sign-out from google
     */
    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            updateUI(false);
        }
    }

    /**
     * Revoking access from google
     */
    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Log.e(TAG, "User access revoked!");
                            mGoogleApiClient.connect();
                            updateUI(false);
                        }


                    });
        }
    }

    @Override
    public void onClick(View v) {

    }


    /**
     * Background Async task to load user profile picture from url
     */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private class GetLoginTask extends AsyncTask<Void, Void, Void> {

        private StringBuffer stringBuffer = new StringBuffer();
        String message;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SigninActivity.this);
            progressDialog.setMessage("Processing login... please wait..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "login?username=" + Email + "&password=" + password + "&user_type=C");
                Log.e("Url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Login-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Login-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("Error", "" + e);
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(stringBuffer.toString());
                Log.e("buffer", stringBuffer.toString());

                if (jobj.has("message")) {

                    message = jobj.getString("message");
                    Log.e("msg", message);
                    if (message.equals("Not Found") || (message.equals("Not Found: Password Invalid"))) {
                        userid = null;
                    }
                }
                if (jobj.has("user_id")) {
                    userid = jobj.getString("user_id");
                    Log.e("id", jobj.getString("user_id"));
                }


                Preferences.add(Preferences.PrefType.User_id, userid, getApplicationContext());
                //Log.e("useer", userid);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
            // Log.e("uid",uid);
            try {
                if (uid == null) {
                    if (message.equals("Not Found")) {
                        Toast.makeText(SigninActivity.this, "Invalid Mail id or Password!", Toast.LENGTH_SHORT).show();
                    }
                    if (message.equals("Not Found: Password Invalid")) {
                        Toast.makeText(SigninActivity.this, "Invalid Password!", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    //uid=Preferences.getString(Preferences.PrefType.User_id,getApplicationContext());
                    new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                }

            } catch (Exception e) {
                Toast.makeText(SigninActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }

    }

    private class GetFLoginTask extends AsyncTask<Void, Void, Void> {

        private StringBuffer stringBuffer = new StringBuffer();
        String message;
        JSONObject jsonObject = new JSONObject();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SigninActivity.this);
            progressDialog.setMessage("Processing login... please wait..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                jsonObject.put("email", Email);
                jsonObject.put("status", "A");
                jsonObject.put("password1", password);
                jsonObject.put("password2", password);
                jsonObject.put("company_id", "0");
                jsonObject.put("user_type", "C");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            BufferedReader bufferedReader = null;
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "/users");
                Log.e("Url", String.valueOf(uri));
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));


                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);

                Log.e("Login-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("Login-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("Error", "" + e);
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("buffer", String.valueOf(jobj));

                if (jobj.has("message")) {

                    message = jobj.getString("message");
                    Log.e("msg", message);
                    if (message.equals("Not Found") || (message.equals("Not Found: Password Invalid"))) {
                        userid = null;
                    }
                }
                if (jobj.has("user_id")) {
                    userid = jobj.getString("user_id");
                    Log.e("id", jobj.getString("user_id"));
                }


                Preferences.add(Preferences.PrefType.User_id, userid, getApplicationContext());
                //Log.e("useer", userid);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
            // Log.e("uid",uid);
            try {
                if (uid == null) {
                    if (message.equals("Not Found")) {
                        // Toast.makeText(SigninActivity.this, "Invalid Mail id!", Toast.LENGTH_SHORT).show();
                        new PostUser().execute();

                    }
                    if (message.equals("Not Found: Password Invalid")) {
                        Toast.makeText(SigninActivity.this, "Invalid Password!", Toast.LENGTH_SHORT).show();
                    }

                    if (message.equalsIgnoreCase("Bad Request: The username or email you have chosen already exists. Please try another one.")) {
                        new GetLoginTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                    }


                } else {
                    //uid=Preferences.getString(Preferences.PrefType.User_id,getApplicationContext());
                    new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                }
               /* if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }*/
            } catch (Exception e) {
                Toast.makeText(SigninActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


        }

    }

    private class PostUser extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject = new JSONObject();
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = new StringBuffer();
        String message;

        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog = new ProgressDialog(SigninActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                jsonObject.put("firstname", ffname);
                jsonObject.put("lastname", flname);
               /* jsonObject.put("user_login",ffname+""+flname);*/
                jsonObject.put("email", Email);
                jsonObject.put("company_id", "0");
                jsonObject.put("status", "A");
                jsonObject.put("password1", password);
                jsonObject.put("password2", password);
                jsonObject.put("user_type", "C");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());

                URI uri = new URI(getString(R.string.url) + "users");
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("PostUser-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("PostUser-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                // HttpEntity resultEntity = httpResponse.getEntity();
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                if (jobj.has("message")) {

                    message = jobj.getString("message");
                    Log.e("msg", message);
                    user = null;
                   /* if(message.equals("Not Found") || (message.equals("Not Found: Password Invalid")))
                    {
                        user="-1001";
                    }*/
                }
                if (jobj.has("user_id")) {
                    user = jobj.getString("user_id");
                    Log.e("id", jobj.getString("user_id"));
                }


                Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                String n = db.getName();
                db.deleteName(n);
                mainCardModel = new MainCardModel(ffname);
                db.addName(mainCardModel);
                //Preferences.add(Preferences.PrefType.First_name, ffname, getApplicationContext());
                Preferences.add(Preferences.PrefType.Last_name, flname, getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
            try {
                if (uid == null) {
                    if (message.equals("Bad Request: The username or email you have chosen already exists. Please try another one.")) {
                        Toast.makeText(SigninActivity.this, "An user exists with this mail id!", Toast.LENGTH_SHORT).show();
                    }
                    if (message.equals("Bad Request: The passwords do not match.")) {
                        Toast.makeText(SigninActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);

                    // uid=Preferences.getString(Preferences.PrefType.User_id,getApplicationContext());
                }
               /* if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }*/
            } catch (Exception e) {
                Toast.makeText(SigninActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }
    }

    private class PutAddress extends AsyncTask<String, Void, Void> {

        String User_id;

        public PutAddress(String user_id) {
            this.User_id = user_id;

        }

        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("b_country", "IN");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "address/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("CheckoutPut-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("CheckoutPut-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();

            Preferences.add(Preferences.PrefType.COUNTRY, "IN", getApplicationContext());


        }
    }

    private class GetUsers extends AsyncTask<String, Void, Void> {
        String User_id;
        private StringBuffer stringBuffer = new StringBuffer();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SigninActivity.this);
            progressDialog.setMessage("Processing login... please wait..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        public GetUsers(String userid) {
            this.User_id = userid;
        }

        @Override
        protected Void doInBackground(String... params) {

            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "users/" + userid);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Users-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Users-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                String name = jobj.getString("firstname");
                String last = jobj.getString("lastname");

                Log.e("Name", name);
                db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                String n = db.getName();
                db.deleteName(n);
                mainCardModel = new MainCardModel(name);
                db.addName(mainCardModel);
                Log.e("name", db.getName());
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {

                }
                Intent intent = new Intent(SigninActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();

                //Preferences.add(Preferences.PrefType.First_name, name, getApplicationContext());
                Preferences.add(Preferences.PrefType.Last_name, last, getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();
            Batch.User.getEditor().setIdentifier(userid).setAttribute("user_id", userid).save();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Batch.onStart(this);
    }

   /* @Override
    protected void onStop()
    {
        Batch.onStop(this);

        super.onStop();
    }*/

    @Override
    protected void onDestroy() {
        Batch.onDestroy(this);

        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Batch.onNewIntent(this, intent);

        super.onNewIntent(intent);
    }

}



