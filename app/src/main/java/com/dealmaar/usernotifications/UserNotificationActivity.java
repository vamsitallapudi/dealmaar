package com.dealmaar.usernotifications;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.batch.android.Batch;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

public class UserNotificationActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    String uid;
    private String count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usernotification_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
        //  toolbar.setNavigationIcon(R.drawable.menu);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        //displayView(0);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    Log.e("View", String.valueOf(syncItemView));
                    try{
                        count = db.getCartCount(uid);
                    }catch (Exception e){
                        if (count == null || (count.equals("null"))) {
                            count = "0";
                        }
                        Log.e("HomeActivity", e.toString());
                    }

                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(UserNotificationActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        UserNotificationActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {

            case R.id.action_cart:
                Intent cartintent = new Intent(UserNotificationActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }
}
