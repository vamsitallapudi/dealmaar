package com.dealmaar.productpage;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dealmaar.customer.R;

import java.util.ArrayList;

/**
 * Created by ${Bharath} on 12/31/2015.
 */
public class CommentsListAdapter extends ArrayAdapter<Comments> {
    ArrayList<Comments> ArrayListComments;
    int Resource;
    Context context;
    LayoutInflater vi;

    public CommentsListAdapter(Context context, int resource, ArrayList<Comments> objects) {
        super(context, resource, objects);
        ArrayListComments = objects;
        Resource = resource;
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

   /* @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }*/

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = vi.inflate(R.layout.productpage_comment_items, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.vendor_comments_name);
            holder.message = (TextView) convertView.findViewById(R.id.vendor_comments_message);
            holder.rating = (RatingBar) convertView.findViewById(R.id.vendorcomments_ratingbar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(ArrayListComments.get(position).getName());
        holder.message.setText(ArrayListComments.get(position).getMessage());
        try {
            holder.rating.setRating(Float.parseFloat(ArrayListComments.get(position).getRating()));
        } catch (Exception e) {
            Log.e("Exception", " " + e);
        }
        return convertView;
    }

    static class ViewHolder {
        public TextView name;
        public TextView message;
        public RatingBar rating;
    }
}
