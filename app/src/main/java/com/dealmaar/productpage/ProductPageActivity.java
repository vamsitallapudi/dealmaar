package com.dealmaar.productpage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.BaseActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.Home;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.models.Product;
import com.dealmaar.productslist.ProductslistActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.ratings.RatingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProductPageActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {

    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    TextView name;
    String rev;
    TextView oldprice, newprice, discount;
    ImageView productimage;
    WebView prodabout;
    ImageButton wishlist, cart;
    Button buynow, writeareview, viewall;
    String uid, reviews, review, id;
    LinearLayout reviewlayout;
    String product_id, product_name, productold_price, productnew_price, product_shipping, amount, product_desc;
    String catid;
    ArrayList<Product> pro_id = new ArrayList<>();
    LinearLayout[] linearLayout;
    TextView[] like_name, like_price;
    LinearLayout write;
    ImageView[] like_image;
    DrawerLayout drawerLayout;
    ProgressDialog progressDialog;
    ArrayList<String> size;


    private DatabaseHandler db;
    private MainCardModel mainCardModel;
    private String count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productpage_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
       /*final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        size = new ArrayList<>();
        linearLayout = new LinearLayout[20];
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
       /* CardsPagerAdapter mCustomPagerAdapter = new CardsPagerAdapter(this);

        mCardsViewPager = (ViewPager) findViewById(R.id.pager);
        mCardsViewPager.setAdapter(mCustomPagerAdapter);
*/

        linearLayout[0] = (LinearLayout) findViewById(R.id.like_linear1);
        linearLayout[1] = (LinearLayout) findViewById(R.id.like_linear2);
        linearLayout[2] = (LinearLayout) findViewById(R.id.like_linear3);
        linearLayout[3] = (LinearLayout) findViewById(R.id.like_linear4);
        linearLayout[4] = (LinearLayout) findViewById(R.id.like_linear5);
        linearLayout[5] = (LinearLayout) findViewById(R.id.like_linear6);
        linearLayout[6] = (LinearLayout) findViewById(R.id.like_linear7);
        linearLayout[7] = (LinearLayout) findViewById(R.id.like_linear8);
        linearLayout[8] = (LinearLayout) findViewById(R.id.like_linear9);
        linearLayout[9] = (LinearLayout) findViewById(R.id.like_linear10);


        ArrayList<Home> electronics = (ArrayList<Home>) getIntent().getSerializableExtra("Electronics");
        ArrayList<Home> sports = (ArrayList<Home>) getIntent().getSerializableExtra("Sports");
        ArrayList<Home> books = (ArrayList<Home>) getIntent().getSerializableExtra("Books");
        ArrayList<Home> newProducts = (ArrayList<Home>) getIntent().getSerializableExtra("NewProducts");
        ArrayList<Home> bestSellers = (ArrayList<Home>) getIntent().getSerializableExtra("BestSellers");
        ArrayList<Home> music = (ArrayList<Home>) getIntent().getSerializableExtra("Music");
        ArrayList<Home> apparel = (ArrayList<Home>) getIntent().getSerializableExtra("Apparel");
        product_id = getIntent().getExtras().getString("Prodid");

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());

        name = (TextView) findViewById(R.id.productpage_name);
        // company = (TextView) findViewById(R.id.productpage_company);
        productimage = (ImageView) findViewById(R.id.productpage_image);
        prodabout = (WebView) findViewById(R.id.productpage_about);
        wishlist = (ImageButton) findViewById(R.id.productpage_wishlist);
        cart = (ImageButton) findViewById(R.id.productpage_cart);
        buynow = (Button) findViewById(R.id.productpage_buynow);
        viewall = (Button) findViewById(R.id.productpage_viewall);
        oldprice = (TextView) findViewById(R.id.productpage_oldprice);
        newprice = (TextView) findViewById(R.id.productpage_newprice);
        discount = (TextView) findViewById(R.id.productpage_discount);

        writeareview = (Button) findViewById(R.id.productpage_writeareview);
        writeareview.setVisibility(View.GONE);
        write = (LinearLayout) findViewById(R.id.productpage_linear);
        reviewlayout = (LinearLayout) findViewById(R.id.reviewlayout);
        like_image = new ImageView[20];
        like_name = new TextView[20];
        like_price = new TextView[20];
        like_image[0] = (ImageView) findViewById(R.id.productpage_image1);
        like_image[1] = (ImageView) findViewById(R.id.productpage_image2);
        like_image[2] = (ImageView) findViewById(R.id.productpage_image3);
        like_image[3] = (ImageView) findViewById(R.id.productpage_image4);
        like_image[4] = (ImageView) findViewById(R.id.productpage_image5);
        like_image[5] = (ImageView) findViewById(R.id.productpage_image6);
        like_image[6] = (ImageView) findViewById(R.id.productpage_image7);
        like_image[7] = (ImageView) findViewById(R.id.productpage_image8);
        like_image[8] = (ImageView) findViewById(R.id.productpage_image9);
        like_image[9] = (ImageView) findViewById(R.id.productpage_image10);

        like_name[0] = (TextView) findViewById(R.id.productpage_name1);
        like_name[1] = (TextView) findViewById(R.id.productpage_name2);
        like_name[2] = (TextView) findViewById(R.id.productpage_name3);
        like_name[3] = (TextView) findViewById(R.id.productpage_name4);
        like_name[4] = (TextView) findViewById(R.id.productpage_name5);
        like_name[5] = (TextView) findViewById(R.id.productpage_name6);
        like_name[6] = (TextView) findViewById(R.id.productpage_name7);
        like_name[7] = (TextView) findViewById(R.id.productpage_name8);
        like_name[8] = (TextView) findViewById(R.id.productpage_name9);
        like_name[9] = (TextView) findViewById(R.id.productpage_name10);


        like_price[0] = (TextView) findViewById(R.id.productpage_price1);
        like_price[1] = (TextView) findViewById(R.id.productpage_price2);
        like_price[2] = (TextView) findViewById(R.id.productpage_price3);
        like_price[3] = (TextView) findViewById(R.id.productpage_price4);
        like_price[4] = (TextView) findViewById(R.id.productpage_price5);
        like_price[5] = (TextView) findViewById(R.id.productpage_price6);
        like_price[6] = (TextView) findViewById(R.id.productpage_price7);
        like_price[7] = (TextView) findViewById(R.id.productpage_price8);
        like_price[8] = (TextView) findViewById(R.id.productpage_price9);
        like_price[9] = (TextView) findViewById(R.id.productpage_price10);

        String fromhome = getIntent().getExtras().getString("FromHome");
        if (fromhome.equalsIgnoreCase("Home")) {

            new GetHomeProductsPage(product_id).executeOnExecutor(MyApplication.threadPoolExecutor);
            wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("tvk ID", product_id);

                    if (uid == null || uid.equals("")) {
                        Toast.makeText(ProductPageActivity.this, "User not found. Please login to continue!", Toast.LENGTH_SHORT).show();
                        mIntent = new Intent(ProductPageActivity.this, SigninActivity.class);
                        startActivity(mIntent);
                        finish();
                    } else {
                        new PostWishlistProduct(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);
                    }


                }
            });
            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (uid == null || uid.equals("")) {
                        Toast.makeText(ProductPageActivity.this, "User not found. Please login to continue!", Toast.LENGTH_SHORT).show();
                        mIntent = new Intent(ProductPageActivity.this, SigninActivity.class);
                        startActivity(mIntent);
                        finish();
                    } else {

                        if (Integer.parseInt(amount) > 0) {

                            new PostCart(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);

                            try {


                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        View syncItemView = findViewById(R.id.action_cart);
                                        Log.e("View1", String.valueOf(syncItemView));
                                        try {
                                            count = db.getCartCount(uid);
                                        } catch (Exception e) {
                                            if (count == null || (count.equals("null"))) {
                                                //Log.e("If", "if");
                                                count = "0";

                                            }
                                            Log.e("ProductPageActivity", e.toString());
                                        }
                                        if (count == null || count.equals("null")) {
                                            count = "0";
                                        } else if (Integer.parseInt(count) == 0) {
                                            count = "0";
                                        } else {
                                            Log.e("C", "Count" + count);
                                            //Log.e("View1", String.valueOf(syncItemView[0]));
                                            BadgeView badge = new BadgeView(getApplicationContext(), syncItemView);
                                            badge.setText(count);
                                            badge.show();
                                            ProductPageActivity.this.invalidateOptionsMenu();
                                        }


                                    }
                                });
                            } catch (Exception e) {

                            }


                        } else {
                            Toast.makeText(ProductPageActivity.this, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });

            viewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Cat_id", catid);
                    if (pro_id.isEmpty()) {
                        // viewall.setVisibility(View.GONE);
                        Toast.makeText(ProductPageActivity.this, "No items found !!", Toast.LENGTH_LONG).show();
                    } else {
                        viewall.setVisibility(View.VISIBLE);
                        Intent myintent = new Intent(ProductPageActivity.this, ProductslistActivity.class);
                        myintent.putExtra("Calledfromproductpage", "productpage");
                        myintent.putExtra("Category", catid);
                        startActivity(myintent);
                    }

                }
            });

            buynow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(amount) > 0) {
                        new PostCart(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);

                        try {
                            //String c = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                           /* String c=db.getCartCount();
                            if (c == null || c.equals("null")) {
                                c = "0";

                            } else if (Integer.parseInt(c)==0) {
                                c = "0";
                            }

                            String count = String.valueOf(Integer.parseInt(c) + 1);
                            db.deleteCartCount(c);
                            mainCardModel=new MainCardModel(count);
                            db.addCartCount(mainCardModel);*/

                            //Preferences.add(Preferences.PrefType.CART_count, c, getApplicationContext());
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    View syncItemView = findViewById(R.id.action_cart);
                                    Log.e("View1", String.valueOf(syncItemView));
                                    try {
                                        count = db.getCartCount(uid);
                                    } catch (Exception e) {
                                        if (count == null || (count.equals("null"))) {
                                            //Log.e("If", "if");
                                            count = "0";

                                        }
                                        Log.e("ProductPageActivity", e.toString());
                                    }
                                    if (count == null || count.equals("null")) {
                                        count = "0";
                                    } else if (Integer.parseInt(count) == 0) {
                                        count = "0";
                                    } else {
                                        Log.e("C", "Count" + count);

                                        //Log.e("View1", String.valueOf(syncItemView[0]));
                                        BadgeView badge = new BadgeView(getApplicationContext(), syncItemView);
                                        badge.setText(count);
                                        badge.show();
                                        ProductPageActivity.this.invalidateOptionsMenu();
                                    }


                                }
                            });
                        } catch (Exception e) {

                        }
                        Intent myintent = new Intent(ProductPageActivity.this, CartActivity.class);
                        startActivity(myintent);
                        finish();
                    } else {
                        Toast.makeText(ProductPageActivity.this, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            reviewlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetInvProReviewsTask(product_id).executeOnExecutor(MyApplication.threadPoolExecutor);


                }
            });
            //
            writeareview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetProReviewsTask(product_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                    //Log.e("rating",reviews);

                }
            });

        } else {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(2);
            new GetProductsPage(product_id).execute();
            product_name = getIntent().getExtras().getString("Prodname");
            name.setText(product_name);
            String productold_price = getIntent().getExtras().getString("Prodoldprice");
            oldprice.setText(nf.format(Double.parseDouble(productold_price)));
            productnew_price = getIntent().getExtras().getString("Prodprice1");
            newprice.setText(nf.format(Double.parseDouble(productnew_price)));
            product_shipping = getIntent().getExtras().getString("Prodshippingparams");
            amount = getIntent().getExtras().getString("Amount");


            String product_image = getIntent().getExtras().getString("Prodimg");
            Picasso.with(ProductPageActivity.this)
                    .load(product_image)
                    .error(R.drawable.ic_launcher)
                    .resize(300, 300)
                    .into(productimage);


            wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new PostWishlistProduct(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);
                }
            });

            viewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Cat_id", catid);
                    if (pro_id.isEmpty()) {
                        // viewall.setVisibility(View.GONE);
                        Toast.makeText(ProductPageActivity.this, "No items found !!", Toast.LENGTH_LONG).show();
                    } else {
                        viewall.setVisibility(View.VISIBLE);
                        Intent myintent = new Intent(ProductPageActivity.this, ProductslistActivity.class);
                        myintent.putExtra("Calledfromproductpage", "productpage");
                        myintent.putExtra("Category", catid);
                        startActivity(myintent);
                        finish();
                    }

                }
            });

            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(amount) > 0) {

                        if (uid == null) {
                            Toast.makeText(ProductPageActivity.this, "User not found. Please login to continue!", Toast.LENGTH_SHORT).show();
                            mIntent = new Intent(ProductPageActivity.this, SigninActivity.class);
                            startActivity(mIntent);
                            finish();
                        } else {
                            new PostCart(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);
                        }


                    } else {
                        Toast.makeText(ProductPageActivity.this, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            buynow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(amount) > 0) {
                        new PostCart(uid, product_id, product_name, productnew_price, product_shipping).executeOnExecutor(MyApplication.threadPoolExecutor);

                        try {
                           /* String c=db.getCartCount();
                           // String c = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                            if (c == null ||c.equals("null")) {
                                c = "0";

                            } else if (Integer.parseInt(c)==0) {
                                c = "0";
                            }

                            String count = String.valueOf(Integer.parseInt(c) + 1);
                            db.deleteCartCount(c);
                            mainCardModel=new MainCardModel(count);
                            db.addCartCount(mainCardModel);*/

                            // Preferences.add(Preferences.PrefType.CART_count, c, getApplicationContext());
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    View syncItemView = findViewById(R.id.action_cart);
                                    Log.e("View1", String.valueOf(syncItemView));
                                    try {
                                        count = db.getCartCount(uid);
                                    } catch (Exception e) {
                                        if (count == null || (count.equals("null"))) {
                                            //Log.e("If", "if");
                                            count = "0";

                                        }
                                        Log.e("ProductPageActivity", e.toString());
                                    }
                                    //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                                    if (count == null || count.equals("null")) {
                                        count = "0";
                                    } else if (Integer.parseInt(count) == 0) {
                                        count = "0";
                                    } else {
                                        Log.e("C", "Count" + count);

                                        //Log.e("View1", String.valueOf(syncItemView[0]));
                                        BadgeView badge = new BadgeView(getApplicationContext(), syncItemView);
                                        badge.setText(count);
                                        badge.show();
                                        ProductPageActivity.this.invalidateOptionsMenu();
                                    }


                                }
                            });
                        } catch (Exception e) {

                        }
                        Intent myintent = new Intent(ProductPageActivity.this, CartActivity.class);
                        startActivity(myintent);
                        finish();
                    } else {
                        Toast.makeText(ProductPageActivity.this, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            reviewlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetInvProReviewsTask(product_id).executeOnExecutor(MyApplication.threadPoolExecutor);


                }
            });
            //
            writeareview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetProReviewsTask(product_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                    //Log.e("rating",reviews);

                }
            });
        }


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        ProductPageActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    public void getPPage(View v) {
        String prod_id = String.valueOf(v.getTag());
        Log.e("p", prod_id);
        if (v.getTag() != null) {
            Intent myintent = new Intent(this, ProductPageActivity.class);
            myintent.putExtra("FromHome", "Home");
            myintent.putExtra("Prodid", (String) v.getTag());
//            intent.putExtra("Prodid","2");
//            intent.putExtra("Prodname","no");
//            intent.putExtra("Prodoldprice","200");
//            intent.putExtra("Prodprice1","3");
//            intent.putExtra("Prodshippingparams","data");
//            intent.putExtra("Amount","500");
//            intent.putExtra("Prodimg","https:\\/\\/www.dealmaar.com\\/upgrade\\/images\\/detailed\\/3\\/apple.jpg");
//            intent.putExtra("FromHome", "H");
            startActivity(myintent);
            finish();
        }
    }


    private class PostCart extends AsyncTask<String, Void, Void> {
        String product_id, product_name, productnew_price, product_shipping;
        private StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        String message = null;

        String User_id;

        public PostCart(String user_id, String Product_id, String Product_name, String Productnew_price, String Product_shipping) {
            this.User_id = user_id;
            this.product_id = Product_id;
            this.product_name = Product_name;
            this.productnew_price = Productnew_price;
            this.product_shipping = Product_shipping;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Adding to cart...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", product_id);
                jsonObject.put("amount", "1");

                jsonObject.put("price", productnew_price);
                jsonObject.put("extra", "");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(getString(R.string.url) + "Addtocart");//refer to all orders. Only GET and POST are supported.
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("CartPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("CartPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();

                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("my data", String.valueOf(jobj));
                //jArray = jobj.optJSONObject("wishlist");
                if (jobj.has("message")) {
                    message = jobj.getString("message");
                    Log.e("messa", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (message == null) {
                try {
                    String c = db.getCartCount(uid);
                    // String c = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (c == null || c.equals("null")) {
                        c = "0";

                    } else if (Integer.parseInt(c) == 0) {
                        c = "0";
                    }

                    String count = String.valueOf(Integer.parseInt(c) + 1);
                    db.deleteCartCount(uid);
                    mainCardModel = new MainCardModel(uid, count);
                    Log.e("Count", count);
                    db.addCartCount(mainCardModel);

                    (ProductPageActivity.this).invalidateOptionsMenu();
                    //Preferences.add(Preferences.PrefType.CART_count, c, getApplicationContext());

                } catch (Exception e) {

                }

                Toast.makeText(getApplicationContext(), "Added to Cart !",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_LONG).show();
            }

            try {
                progressDialog.dismiss();
            } catch (Exception e) {

            }

        }
    }

    private class PostWishlistProduct extends AsyncTask<Void, Void, Void> {
        String product_id, product_name, productnew_price, product_shipping;
        String User_id;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Adding to wishlist...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        public PostWishlistProduct(String user_id, String Product_id, String Product_name, String Productnew_price, String Product_shipping) {
            this.User_id = user_id;
            this.product_id = Product_id;
            this.product_name = Product_name;
            this.productnew_price = Productnew_price;
            this.product_shipping = Product_shipping;
        }

        @Override
        protected Void doInBackground(Void... params) {
            JSONObject jsonObject;
            jsonObject = new JSONObject();

            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", product_id);
                jsonObject.put("amount", "1");
                jsonObject.put("price", productnew_price);
                jsonObject.put("extra", product_shipping);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(getString(R.string.url) + "Wishlist");//refer to all orders. Only GET and POST are supported.
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("WishlistPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("WishlistPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                progressDialog.dismiss();
                Toast.makeText(ProductPageActivity.this, "Added to wishlist !",
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    Log.e("View", String.valueOf(syncItemView));
                    //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("ProductPageActivity", e.toString());
                    }
                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(ProductPageActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

           /* case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(ProductPageActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(ProductPageActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }

    private class GetProductsPage extends AsyncTask<String, Void, Void> {
        String prodname;
        String companyname;
        String prodimg;
        String proddes;
        String productoldprice;
        String productnewprice, Prod_id;

        GetProductsPage(String product_id) {

            this.Prod_id = product_id;
        }

        ArrayList<Home> prod = (ArrayList<Home>) getIntent().getSerializableExtra("Prod");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }


        @Override
        protected Void doInBackground(String... params) {

            StringBuffer stringBuffer = new StringBuffer("");
            JSONObject jArray = new JSONObject();
            //StringBuffer stringBuffer = new StringBuffer("");
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products/" + Prod_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test1", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                // JSONObject jobj1=new JSONObject(String.valueOf(prod));
                Log.e("Objects", String.valueOf(jobj));
                // Log.e("Obj", String.valueOf(jobj1));
                jArray = jobj.put("Product", "true");
                Log.e("Array", String.valueOf(jArray));
                for (int i = 0; i < jArray.length(); ++i) {

                    prodname = jobj.getString("product");
                    //companyname = jobj.getJSONObject("product_features").getJSONObject("18").getString("variant");
                    try {
                        prodimg = jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path");
                    } catch (Exception e) {

                    }
                    proddes = jobj.getString("full_description");
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    productoldprice = nf.format(Double.parseDouble(jobj.getString("list_price")));
                    productnewprice = nf.format(Double.parseDouble(jobj.getString("price")));
                    catid = jobj.getString("main_category");
                    amount = jobj.getString("amount");
                    rev = jobj.getString("Reviews");
                    mainCardModel = new MainCardModel(product_id, product_name, productold_price, productnew_price, product_desc, productimage);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    db.addRecentlyViewed(mainCardModel);


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            // progressDialog.dismiss();
            super.onPostExecute(result);
            try {
            /*name.setText(prodname);
            company.setText(companyname);*/
                //name.setText(prodname);
                prodabout.loadData(proddes, "text/html", null);
                if (rev.equals("false")) {
                    write.setVisibility(View.GONE);
                }
                new GetCategoryProducts(catid).execute();
            /*Picasso.with(ProductPageActivity.this)
                    .load(prodimg)
                    .error(R.drawable.ic_launcher)
                    .resize(400, 400)
                    .into(productimage);*/
                // oldprice.setText(productoldprice);
                // newprice.setText(productnewprice);
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private class GetHomeProductsPage extends AsyncTask<String, Void, Void> {
        String prodname;
        String companyname;
        String prodimg;
        String proddes;
        String productoldprice;
        String productnewprice, Prod_id;

        GetHomeProductsPage(String product_id) {

            this.Prod_id = product_id;
        }

        ArrayList<Home> prod = (ArrayList<Home>) getIntent().getSerializableExtra("Prod");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(String... params) {

            StringBuffer stringBuffer = new StringBuffer("");
            JSONObject jArray = new JSONObject();
            //StringBuffer stringBuffer = new StringBuffer("");
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products/" + Prod_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test1", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                // JSONObject jobj1=new JSONObject(String.valueOf(prod));
                Log.e("Objects", String.valueOf(jobj));
                // Log.e("Obj", String.valueOf(jobj1));
                jArray = jobj.put("Product", "true");
                Log.e("Array", String.valueOf(jArray));
                for (int i = 0; i < jArray.length(); ++i) {

                    prodname = jobj.getString("product");
                    //companyname = jobj.getJSONObject("product_features").getJSONObject("18").getString("variant");
                    try {
                        prodimg = jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path");
                    } catch (Exception e) {

                    }

                    proddes = jobj.getString("full_description");
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(2);
                    productoldprice = jobj.getString("list_price");
                    Log.e("old", productoldprice);
                    productnew_price = jobj.getString("price");
                    catid = jobj.getString("main_category");
                    mainCardModel = new MainCardModel(product_id, product_name, productold_price, productnew_price, product_desc, productimage);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    db.addRecentlyViewed(mainCardModel);
                    product_shipping = jobj.getString("shipping_params");
                    amount = jobj.getString("amount");
                    rev = jobj.getString("Reviews");

                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ProductPageActivity.this.invalidateOptionsMenu();
            try {
                progressDialog.dismiss();
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMinimumFractionDigits(2);
                nf.setMaximumFractionDigits(2);
            /*name.setText(prodname);
            company.setText(companyname);*/
                if (rev.equals("false")) {
                    write.setVisibility(View.GONE);
                }


                name.setText(prodname);
                oldprice.setText(nf.format(Double.parseDouble(productoldprice)));
                newprice.setText(nf.format(Double.parseDouble(productnew_price)));

                prodabout.loadData(proddes, "text/html", null);

                Picasso.with(ProductPageActivity.this)
                        .load(prodimg)
                        .error(R.drawable.ic_launcher)
                        .resize(300, 300)
                        .into(productimage);
                // oldprice.setText(productoldprice);
                // newprice.setText(productnewprice);
                new GetCategoryProducts(catid).execute();
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }


        }
    }

    private class GetCategoryProducts extends AsyncTask<String, Void, Void> {

        private StringBuffer stringBuffer = new StringBuffer();
        String Cat_Id;
        JSONArray jArray = new JSONArray();

        public GetCategoryProducts(String cat_id) {
            this.Cat_Id = cat_id;
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "categories/" + catid + "/products");//refer to all categories. Only GET and POST are supported.

                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("BeforeCategoryproducts", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Aftercategoryproducts", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("", "" + e);
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("buffer", String.valueOf(jobj));
                jArray = jobj.optJSONArray("products");

                for (int i = 0; i < 9; i++) {
                    Product pro = new Product("product", "my_product");
                    jobj = jArray.getJSONObject(i);
                    String p = jobj.getString("product_id");
                    if (p.equals(product_id)) {
                        Log.e("entered", "if");
                        pro.setProduct_id(p);
                    } else {
                        Log.e("entered", "else");
                        pro.setProduct_id(jobj.getString("product_id"));
                        pro.setProduct_name(jobj.getString("product"));
                        pro.setBase_price(jobj.getString("price"));
                        pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));

                        pro_id.add(pro);
                        Log.e("P", String.valueOf(pro_id.size()));
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }

        protected void onPostExecute(Void result) {
            //progressDialog.dismiss();
            super.onPostExecute(result);
            try {
                if (pro_id.size() > 0) {
                    for (int i = 0; i < pro_id.size(); ++i) {
                        linearLayout[i].setVisibility(View.VISIBLE);

                    }
                    for (int i = 0; i < pro_id.size(); ++i) {
                        NumberFormat nf = NumberFormat.getInstance();
                        nf.setMinimumFractionDigits(2);
                        nf.setMaximumFractionDigits(2);
                        like_name[i].setText(pro_id.get(i).getProduct_name());
                        like_price[i].setText(nf.format(Double.parseDouble(pro_id.get(i).getBase_price())));
                        String[] image = new String[20];
                        image[i] = pro_id.get(i).getProduct_image();
                        ((View) like_name[i].getParent()).setTag(pro_id.get(i).getProduct_id());
                        Picasso.with(getApplicationContext()).load(image[i]).error(R.drawable.ic_launcher).resize(400, 400).into(like_image[i]);
                    }
                } else {
                    TextView text = (TextView) findViewById(R.id.like_no_items_found);
                    text.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private class GetInvProReviewsTask extends AsyncTask<Void, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        String proId;

        public GetInvProReviewsTask(String productId) {
            this.proId = productId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products/" + proId);//refer to all categories. Only GET and POST are supported.
                Log.e("ProductPageComments", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("BeforeComments", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("AfterComments", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Comments", "Reviews:" + stringBuffer);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("", "" + e);
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("buffer", String.valueOf(jobj));
                try {
                    id = jobj.getString("product_id");
                    String rev = jobj.getString("Reviews");
                    Log.e("Rev", "" + rev);
                    if ("false".equals(rev)) {
                        review = String.valueOf(jobj.getString("Reviews"));
                       /* reviews = String.valueOf(jobj.getJSONObject("Reviews").getString("average_rating"));
                        Log.e("Reviews",""+reviews);*/
                    } else {
                        /*review = String.valueOf(jobj.getString("Reviews"));*/
                        reviews = String.valueOf(jobj.getJSONObject("Reviews").getString("average_rating"));
                        Log.e("Review", "" + reviews);

                    }
                } catch (Exception e) {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
            super.onPostExecute(result);
            try {
                if ("false".equals(reviews) || "0".equals(reviews) || "false".equals(review)) {
                    Toast.makeText(ProductPageActivity.this, "No Reviews Posted yet !!", Toast.LENGTH_SHORT).show();
                } else {
                    Intent reviewl = new Intent(ProductPageActivity.this, ProductPageComments.class);
                    // String rproductId = getIntent().getExtras().getString("Prodid");
                    reviewl.putExtra("product_Id", id);
                    //Log.e("individual", "productId reviews:" + rproductId);
                    startActivity(reviewl);
                    //finish();
                }
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }


        }
    }

    private class GetProReviewsTask extends AsyncTask<String, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        String proId;

        public GetProReviewsTask(String productId) {
            this.proId = productId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "products/" + proId);
                Log.e("ProductPageActivity", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("BeforeReview", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("AfterReview", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Comments", "Reviews:" + stringBuffer);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("", "" + e);
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("buffer", String.valueOf(jobj));
                try {
                    id = jobj.getString("product_id");
                    String rev = jobj.getString("Reviews");
                    Log.e("Rev", "" + rev);
                    if ("false".equals(rev)) {
                        review = String.valueOf(jobj.getString("Reviews"));
                       /* reviews = String.valueOf(jobj.getJSONObject("Reviews").getString("average_rating"));
                        Log.e("Reviews",""+reviews);*/
                    } else {
                        /*review = String.valueOf(jobj.getString("Reviews"));*/
                        reviews = String.valueOf(jobj.getJSONObject("Reviews").getString("average_rating"));
                        Log.e("Review", "" + reviews);

                    }
                } catch (Exception e) {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
            super.onPostExecute(result);
            try {
                Log.e("Reviews", "" + reviews);
                if ("false".equals(reviews) || "false".equals(review)) {
                    Toast.makeText(ProductPageActivity.this, "You cant write a review !", Toast.LENGTH_SHORT).show();

                } else {
                    Intent review = new Intent(ProductPageActivity.this, RatingsActivity.class);
                    //String id = getIntent().getExtras().getString("Prodid");
                    Log.e("Id", " " + id);
                    review.putExtra("Id", id);
                    startActivity(review);
                }
            } catch (Exception e) {
                // Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }
    }
}
