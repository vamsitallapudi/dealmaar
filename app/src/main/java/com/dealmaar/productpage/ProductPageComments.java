package com.dealmaar.productpage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ${Bharath} on 12/31/2015.
 */
public class ProductPageComments extends AppCompatActivity {
    ProgressDialog progressDialog;
    ListView list;
    ArrayList<Comments> ArrayListCommentsList;

    public ProductPageComments() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productpage_comments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo=toolbar.getChildAt(1);
        /*toolbar.setNavigationIcon(R.drawable.menu);*/
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        list = (ListView) findViewById(R.id.reviewslist_listView);
        ArrayListCommentsList = new ArrayList<Comments>();
        new GetReviewsTask().executeOnExecutor(MyApplication.threadPoolExecutor);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    Log.e("View", String.valueOf(syncItemView));
                    String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (count == null ||count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(ProductPageComments.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        }catch (Exception e){

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(ProductPageComments.this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(ProductPageComments.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;
            /*case R.id.action_search:
                Intent searchintent = new Intent(ProductPageComments.this, SearchMainActivity.class);
                startActivity(searchintent);
                return true;*/


        }

        return super.onOptionsItemSelected(item);
    }

    private class GetReviewsTask extends AsyncTask<Void, Void, List<JSONObject>> {
        private StringBuffer stringBuffer = new StringBuffer();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductPageComments.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }


        @Override
        protected List<JSONObject> doInBackground(Void... params) {
            String revProductId = getIntent().getExtras().getString("product_Id");
            Log.e("ProductPageComments", "revProductId" + revProductId);
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url)+"products/" + revProductId);//refer to all categories. Only GET and POST are supported.
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("BeforeComments", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("AfterComments", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Comments", "Reviews:" + stringBuffer);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("", "" + e);
                    }
                }
            }
            try {
                Log.e("Hi", "This is try");
                //*Log.e("Jcomments","jobj"+jobj);
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("JOBJ", "::::" + jobj);

                JSONObject jsonObject = jobj.optJSONObject("Reviews");
                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.optJSONArray("posts");

                    Log.e("Array", String.valueOf(jsonObject));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Log.e("Hi", "This is for");
                        JSONObject jobj1 = jsonArray.getJSONObject(i);
                        Log.e("JArray", String.valueOf(jsonArray));
                        Comments com = new Comments();
                        //String status = jobj1.getString("status");

                        com.setName(jobj1.getString("name"));
                        com.setMessage(jobj1.getString("message"));
                        com.setRating(jobj1.getString("rating_value"));
                        String status=jobj1.getString("status");
                        if(status.equals("A")) {

                            ArrayListCommentsList.add(com);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }




                Log.e("Reviews:::", "ArrayListCommentsList:::" + ArrayListCommentsList);


                // Log.e("List", String.valueOf(productsList));
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(List<JSONObject> result) {
            super.onPostExecute(result);
            CommentsListAdapter adapter = new CommentsListAdapter(getApplicationContext(), R.layout.productpage_comment_items, ArrayListCommentsList);
            adapter.notifyDataSetChanged();
            list.setAdapter(adapter);
            progressDialog.dismiss();
        }
    }
}
