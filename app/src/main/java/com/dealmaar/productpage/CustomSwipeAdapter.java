package com.dealmaar.productpage;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dealmaar.customer.R;

/**
 * Created by omm on 2/10/2016.
 */
public class CustomSwipeAdapter extends PagerAdapter {
    private int[] resources={R.drawable.acer,R.drawable.apple};
    private boolean mIsDefaultItemSelected = false;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    ProductPageActivity activity;

    public CustomSwipeAdapter(Context mContext) {
        this.mContext=mContext;
        this.activity=activity;
        //mLayoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view=mLayoutInflater.inflate(R.layout.productpage_image,container,false);
        ImageView imageView=(ImageView)item_view.findViewById(R.id.productpage_image);
        imageView.setImageResource(resources[position]);
        container.addView(item_view);


        return item_view;

}


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view ==(LinearLayout) object);
    }


}
