package com.dealmaar.productpage;

/**
 * Created by ${Bharath} on 12/31/2015.
 */
public class Comments {

    private String name;
    private String message;
    private String rating;
    private String status;

    public Comments() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
