package com.dealmaar.payment.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.customer.R;
import com.dealmaar.payment.utility.AvenuesParams;
import com.dealmaar.payment.utility.ServiceUtility;

/**
 * Created by rockey on 8/12/15.
 */
public class InitialScreenActivity extends Activity {

    private EditText accessCode, merchantId, currency, amount, rsaKeyUrl, redirectUrl, cancelUrl;
    TextView orderId;

    private void init() {
        //accessCode = (EditText) findViewById(R.id.accessCode);
        //merchantId = (EditText) findViewById(R.id.merchantId);
        orderId = (TextView) findViewById(R.id.orderId);
        currency = (EditText) findViewById(R.id.currency);
        amount = (EditText) findViewById(R.id.amount);
        //rsaKeyUrl = (EditText) findViewById(R.id.rsaUrl);
        //redirectUrl = (EditText) findViewById(R.id.redirectUrl);
        //cancelUrl = (EditText) findViewById(R.id.cancelUrl);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_screen);
        init();

        //generating order number
        Integer randomNum = ServiceUtility.randInt(0, 9999999);
        orderId.setText(randomNum.toString());
        String price = getIntent().getExtras().getString("Price");
        Log.e("price", price);
        amount.setText(price);

    }

    public void onClick(View view) {
        //Mandatory parameters. Other parameters can be added if required.
        //String vAccessCode = "4YRUXLSRO20O8NIH";
        //String redirectUrl="http://122.182.6.216/merchant/ccavResponseHandler.jsp";
        //String cancelUrl="http://122.182.6.216/merchant/ccavResponseHandler.jsp";
        //String rsaKey="http://122.182.6.216/merchant/GetRSA.jsp";

        String vAccessCode = "AVRP07CK36BL21PRLB";
        String redirectUrl = "http://dealmaar.com/upgrade/50/ccavResponseHandler.php";
        String cancelUrl = "http://dealmaar.com/upgrade/50/ccavResponseHandler.php";
        String rsaKey = "http://dealmaar.com/upgrade/50/GetRSA2.php";

        String vMerchantId = "78272";


        String vCurrency = ServiceUtility.chkNull(currency.getText()).toString().trim();
        String vAmount = ServiceUtility.chkNull(amount.getText()).toString().trim();


        if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
            Log.e("Access", vAccessCode);
            Log.e("Merchant", vMerchantId);
            Log.e("Currency", vCurrency);
            Log.e("vAmount", vAmount);
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(AvenuesParams.ACCESS_CODE, vAccessCode);
            intent.putExtra(AvenuesParams.MERCHANT_ID, vMerchantId);
            intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(orderId.getText()).toString().trim());
            intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull(currency.getText()).toString().trim());
            intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(amount.getText()).toString().trim());

            intent.putExtra(AvenuesParams.REDIRECT_URL, redirectUrl);
            intent.putExtra(AvenuesParams.CANCEL_URL, cancelUrl);
            intent.putExtra(AvenuesParams.RSA_KEY_URL, rsaKey);

            startActivity(intent);
        } else {
            showToast("All parameters are mandatory.");
        }
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }
}

