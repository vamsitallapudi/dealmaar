package com.dealmaar.payment.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.cart.Cart;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StatusActivity extends AppCompatActivity {
    Button click;
    ArrayList<Cart> cartArray = new ArrayList<>();
    String prodid, uid;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_status);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());

        Intent mainIntent = getIntent();
        Log.e("Status", String.valueOf(mainIntent));
        String Oid = mainIntent.getExtras().getString("Order_id");
        String tracking_id = mainIntent.getExtras().getString("track_id");
        String transStatus = mainIntent.getStringExtra("transStatus");
        cartArray = (ArrayList<Cart>) getIntent().getSerializableExtra("Cart");
        /*Log.e("Id",tracking_id);
        Log.e("status1",transStatus);
		Log.e("order_id",Oid);*/

        TextView tv4 = (TextView) findViewById(R.id.textView1);
        tv4.setText(mainIntent.getStringExtra("transStatus"));
        if (transStatus.equals("Transaction Successful!")) {
            new DeleteCart(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
        }
        else
        {
            Log.e("entered","else");
            new PutOrder(Oid, tracking_id, transStatus).executeOnExecutor(MyApplication.threadPoolExecutor);
        }

        click = (Button) findViewById(R.id.status_clickhere);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StatusActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //new PutOrder(Oid,tracking_id,transStatus).execute();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(StatusActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    private class DeleteCart extends AsyncTask<String, Void, Void> {
        ArrayList<Cart> cartArray = (ArrayList<Cart>) getIntent().getSerializableExtra("Cart");
        String User_id;

        public DeleteCart(String uid) {
            this.User_id = uid;

        }


        @Override
        protected Void doInBackground(String... params) {
            for (int i = 0; i < cartArray.size(); ++i) {
                prodid = cartArray.get(i).getProduct_id();
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpDelete delete = new HttpDelete();

                    URI uri = new URI(getApplicationContext().getString(R.string.url) + "Addtocart/" + User_id + "?product_id=" + prodid);
                    delete.setURI(uri);
                    delete.addHeader(BasicScheme.authenticate(
                            new UsernamePasswordCredentials(getApplicationContext().getString(R.string.username), getApplicationContext().getString(R.string.password)),
                            HTTP.UTF_8, false));
                    delete.setHeader("content-type", "application/json");
                    Log.e("CartDelete-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    HttpResponse httpResponse = httpClient.execute(delete);
                    Log.e("CartDelete-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                    HttpEntity httpEntity = httpResponse.getEntity();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }

    private class PutOrder extends AsyncTask<String, String, String> {
        String orderid;
        String trackingid;
        String status;
        JSONObject jsonObject = new JSONObject();

        public PutOrder(String Oid, String tracking_id, String transStatus) {
            this.orderid = Oid;
            this.trackingid = tracking_id;
            this.status = transStatus;

        }

        @Override
        protected String doInBackground(String... params) {
            InputStream inputStream = null;
            String result = "";
            Log.e("OID", orderid);

            try {

                    Log.e("status1", status);
                    jsonObject.put("status", "I");
                    Log.e("Entered", "Put");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());

               URI uri = new URI(getString(R.string.url)+"orders/" + orderid);
                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getApplicationContext().getString(R.string.username), getApplicationContext().getString(R.string.password)),
                        HTTP.UTF_8, false));

                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);


                Log.e("OrderPut-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpclient.execute(put);
                Log.e("OrderPut-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity httpEntity = httpResponse.getEntity();

                return null;
            } catch (Exception e) {
                Log.e("InputStream", e.getLocalizedMessage());
            }
            return null;
        }
        }
    }