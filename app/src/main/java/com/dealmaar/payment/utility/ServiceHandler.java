package com.dealmaar.payment.utility;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ServiceHandler {
 
    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
 
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }
 
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
            List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;
            
            //Setting user agent
           // httpClient.getParams().setParameter(
           // 	    HttpProtocolParams.USER_AGENT,
           // 	    "Mozilla/5.0 (Linux; U; Android-4.0.3; en-us; Galaxy Nexus Build/IML74K) AppleWebKit/535.7 (KHTML, like Gecko) CrMo/16.0.912.75 Mobile Safari/535.7"
           // 	);
             
            // Checking http request method type
            if (method == POST) {
                //url = "http://google.com";
                HttpPost httpPost = new HttpPost(url);
                Log.e("url",url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }
                httpResponse = httpClient.execute(httpPost);
                //Log.e("HttpResponse", String.valueOf(httpResponse.toString()));
            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils.format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
                Log.e("Url", url);
                httpResponse = httpClient.execute(httpGet);

            }
            //httpEntity = httpResponse.getEntity();

            //Log.e("Entity", String.valueOf(httpEntity.getContent()));

            //response = EntityUtils.toString(httpEntity);
            BufferedReader bufferedReader = null;
            StringBuffer stringBuffer = new StringBuffer("");
            try{
            InputStream inputStream = httpResponse.getEntity().getContent();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine = bufferedReader.readLine();


            while (readLine != null) {
                stringBuffer.append(readLine);
                stringBuffer.append("\n");
                readLine = bufferedReader.readLine();
                //Log.e("Test", String.valueOf(stringBuffer));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {

                }
            }
        }
            response = String.valueOf(stringBuffer);

            //Log.e("Servicehandler Response",response);
        } catch (UnsupportedEncodingException e) {
            //Log.e("error1","unsupported");
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            //Log.e("error2","clientprotocol");
            e.printStackTrace();
        } catch (IOException e) {
            //Log.e("err","ioe",e);
            //Log.e("error1","ioexception");
            e.printStackTrace();
        }
        return response;
    }
}