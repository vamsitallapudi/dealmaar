package com.dealmaar.profile;

import java.io.Serializable;

/**
 * Created by omm on 4/1/2016.
 */
public class Countries implements Serializable {
    String code;
    String country;
    String status;

    public Countries(String code,String country)
    {

    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status=status;
    }
    public String getCode()
    {
        return code;
    }
    public  void setCode(String code)
    {
        this.code=code;
    }
    public String getCountry()
    {
        return country;
    }
    public void setCountry(String country)
    {
        this.country=country;
    }
}
