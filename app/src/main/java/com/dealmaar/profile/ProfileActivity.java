package com.dealmaar.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class ProfileActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    String firstname, addr, address2, cityname, statename, countryname, zipcode, phoneno, uid, Email, fname, lname, f, City, Country, addres, addres2, State, Pin, Phoneum;
    TextView name;
    TextView address;
    TextView address_2;
    TextView city;
    TextView zip;
    TextView phone, email, country, state;
    EditText firstName, lastName, EMail, FirstName, LastName, Address, Address2, CityName, StateName, CountryName, Zipcode, Phone;
    ImageButton editname, editemail, editaddress;
    String editfirstname, editlastname, editaddress1, editaddress2, editcity, editstate, editcountry, editzip, editphone;
    Button check, cance1;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    DrawerLayout drawerLayout;
    Spinner spinnerCountry, spinnerState;
    ArrayList<Countries> coname = new ArrayList<>();
    ArrayList<String> cname = new ArrayList<>();
    ArrayList<String> stname = new ArrayList<>();
    ArrayList<State> sname = new ArrayList<>();
    String countryCode, stateCode,coName,stName;
    JSONObject s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });
        countryCode = "IN";
        new GetCountries().executeOnExecutor(MyApplication.threadPoolExecutor);

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());



        new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
        new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
        new GetEmail(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
        editname = (ImageButton) findViewById(R.id.profile_editname);
        editemail = (ImageButton) findViewById(R.id.profile_editemail);
        email = (TextView) findViewById(R.id.profile_email);
        editaddress = (ImageButton) findViewById(R.id.profile_editaddress);



        editaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfileActivity.this);
                final LayoutInflater inflater = getLayoutInflater();
                View alertlayout = inflater.inflate(R.layout.profile_alert_editaddress, null);
                //final AlertDialog dialog=alertDialog.create();
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                alertDialog.setView(alertlayout);
                final AlertDialog dialog = alertDialog.create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


                /*Toolbar toolbar = (Toolbar)alertlayout.findViewById(R.id.app_bar);
                setSupportActionBar(toolbar);
                toolbar.setLogo(R.drawable.logo2);
                toolbar.getMenu().clear();*/
                //dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                dialog.show();


                check = (Button) alertlayout.findViewById(R.id.profile_address_check);
                cance1 = (Button) alertlayout.findViewById(R.id.profile_address_cancel);
                FirstName = (EditText) alertlayout.findViewById(R.id.profile_address_editfname);
                LastName = (EditText) alertlayout.findViewById(R.id.profile_address_editlname);
                Address = (EditText) alertlayout.findViewById(R.id.profile_address_editaddress);
                Address2 = (EditText) alertlayout.findViewById(R.id.profile_address_editaddress1);
                CityName = (EditText) alertlayout.findViewById(R.id.profile_address_editcity);
                /*StateName = (EditText) alertlayout.findViewById(R.id.profile_address_editstate);
                CountryName = (EditText) alertlayout.findViewById(R.id.profile_address_editcountry);*/
                spinnerCountry = (Spinner) alertlayout.findViewById(R.id.profile_countryname);
                spinnerState = (Spinner) alertlayout.findViewById(R.id.profile_statename);

                //new GetStates(countryCode).executeOnExecutor(MyApplication.threadPoolExecutor);
                Zipcode = (EditText) alertlayout.findViewById(R.id.profile_address_editzip);
                Phone = (EditText) alertlayout.findViewById(R.id.profile_address_editphone);
                // FirstName.setText(Preferences.getString(Preferences.PrefType.First_name, getApplicationContext()));
                db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                FirstName.setText(db.getName());
                LastName.setText(Preferences.getString(Preferences.PrefType.Last_name, getApplicationContext()));
                Address.setText(Preferences.getString(Preferences.PrefType.ADDRESS, getApplicationContext()));
                Address2.setText(Preferences.getString(Preferences.PrefType.ADDRESS2, getApplicationContext()));
                CityName.setText(Preferences.getString(Preferences.PrefType.CITY, getApplicationContext()));
                //StateName.setText(Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext()));
                try {
                    if (coname.size() > 0) {
                        if (cname != null) {
                            final ArrayAdapter<String> a = new ArrayAdapter<String>(ProfileActivity.this, R.layout.spinner_item, cname);
                            a.setDropDownViewResource(R.layout.spinner_item);

                            final int spinnerPosition = a.getPosition("India");

                            // spinnerCountry.setSelection(a.getPosition("India"));
                            spinnerCountry.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (spinnerCountry.getSelectedItem().toString() == null || Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext()) == null) {
                                        spinnerCountry.setSelection(spinnerPosition);
                                    }
                                    spinnerCountry.setSelection(a.getPosition(Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext())));
                                    //Log.e("Select", "" + spinnerCountry.getSelectedItem().toString());
                                }
                            });

                            Log.e("Value", "" + spinnerPosition);
                            spinnerCountry.setAdapter(a);
                            spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    coName = spinnerCountry.getSelectedItem().toString();
                                    Log.e("Selected", "" + coName);
                                    for (int i = 0; i < coname.size(); ++i) {
                                        if (coname.get(i).getCountry() == coName || coname.get(i).getCountry().equals(coName)) {
                                            countryCode = coname.get(i).getCode();
                                            Log.e("Code", "" + countryCode);
                                        }


                                    }

                                    // Log.e("States","size"+stname.size());
                                    stname.removeAll(stname);
                                    sname.removeAll(sname);



                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            new GetStates(countryCode).executeOnExecutor(MyApplication.threadPoolExecutor);

                        }
                    }


                } catch (Exception e) {

                }

                //CountryName.setText(Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext()));
                Zipcode.setText(Preferences.getString(Preferences.PrefType.ZIP, getApplicationContext()));
                Phone.setText(Preferences.getString(Preferences.PrefType.PHONE, getApplicationContext()));
                // StateName.setText(Preferences.getString(Preferences.PrefType.STATE, getApplicationContext()));

                check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Boolean wantToCloseDialog = true;
                        editfirstname = FirstName.getText().toString();
                        editlastname = LastName.getText().toString();
                        editaddress1 = Address.getText().toString();
                        editaddress2 = Address2.getText().toString();
                        editcity = CityName.getText().toString();
                        try {
                            Iterator<String> keys = s.keys();
                            for (int i = 0; i < s.length(); ++i) {
                                String key = keys.next();
                                String value = s.getString(key);
                                if (stName.equals(value) || stName == value) {
                                    stateCode = key;
                                    Log.e("Code", stateCode);
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("tvk exception occured", e.toString());
                        }

                        // editstate = stName;
                       /* editstate=StateName.getText().toString();
                        editcountry = CountryName.getText().toString();*/
                        editzip = Zipcode.getText().toString();
                        editphone = Phone.getText().toString();


                        if (editaddress1.length() <= 1) {

                            Toast.makeText(ProfileActivity.this, "Address 1 cannot be empty", Toast.LENGTH_SHORT).show();

                        } else if (editcity.length() <= 1) {

                            Toast.makeText(ProfileActivity.this, "City Name cannot be empty", Toast.LENGTH_SHORT).show();
                        } else if (editphone.length() <= 9 || editphone.length() >= 11) {
                            Toast.makeText(getApplicationContext(), "Please enter a valid mobile number", Toast.LENGTH_LONG).show();
                        } else {


                            new PutAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                            new PutUserFirstname(editfirstname,editlastname,uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                            dialog.dismiss();

                        }


                    }
                });


                cance1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);

                        // finish();


                    }
                });


            }
        });
        editname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LayoutInflater factory=LayoutInflater.from(this);
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfileActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View alertlayout = inflater.inflate(R.layout.profile_alert_editname, null);
                alertDialog.setView(alertlayout);
                firstName = (EditText) alertlayout.findViewById(R.id.profile_firstname);
                lastName = (EditText) alertlayout.findViewById(R.id.profile_lastname);
                //f = Preferences.getString(Preferences.PrefType.First_name, getApplicationContext());
                db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                f = db.getName();
                firstName.setText(f);
                //Log.e("F", f);
                lastName.setText(Preferences.getString(Preferences.PrefType.Last_name, getApplicationContext()));


                alertDialog.setTitle("");

                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        String editfirstname = firstName.getText().toString();
                        String editlastname = lastName.getText().toString();
                        Log.e("",""+editfirstname+editlastname);
                        new PutFirstname(editfirstname, editlastname, uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                        new PutUserFirstname(editfirstname,editlastname,uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                        new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);

                    }
                });


                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                        new GetUsers(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    }
                });


                alertDialog.show();
            }
        });



        name = (TextView) findViewById(R.id.profile_name);
        address = (TextView) findViewById(R.id.profile_address);
        address_2 = (TextView) findViewById(R.id.profile_address2);
        city = (TextView) findViewById(R.id.profile_city);
        zip = (TextView) findViewById(R.id.profile_zip);
        phone = (TextView) findViewById(R.id.profile_phone);
        state = (TextView) findViewById(R.id.profile_state);

        country = (TextView) findViewById(R.id.profile_country);
        editname = (ImageButton) findViewById(R.id.profile_editname);
    }

    public void onBackPressed() {
        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private class GetUsers extends AsyncTask<String, Void, Void> {
        String User_id;
        private StringBuffer stringBuffer = new StringBuffer();

        public GetUsers(String userid) {
            this.User_id = userid;
        }


        @Override
        protected Void doInBackground(String... params) {

            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "users/" + User_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Users-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Users-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                Log.e("tvk Exception occured",e.toString());
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                String name = jobj.getString("firstname");
                String last = jobj.getString("lastname");
                Log.e("Name",""+ name);




                //Preferences.add(Preferences.PrefType.First_name, name, getApplicationContext());
                Preferences.add(Preferences.PrefType.Last_name, last, getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }

    private class PutEmail extends AsyncTask<String, Void, Void> {
        String User_id, EditEmail;

        public PutEmail(String user_id, String editemail) {
            this.User_id = user_id;
            this.EditEmail = editemail;
        }


        @Override
        protected Void doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("email", EditEmail);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "users/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("EmailGet-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("Email-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //progressDialog.dismiss();
            Preferences.add(Preferences.PrefType.EMAIL, EditEmail, getApplicationContext());

        }

    }

    private class PutAddress extends AsyncTask<String, Void, Void> {
        String User_id;

        public PutAddress(String user_id) {
            this.User_id = user_id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... params) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("b_firstname", editfirstname);
                jsonObject.put("b_lastname", editlastname);
                /*jsonObject.put("firstname", editfirstname);
                jsonObject.put("lastname", editlastname);*/
                jsonObject.put("b_address", editaddress1);
                jsonObject.put("b_address_2", editaddress2);
                jsonObject.put("b_city", editcity);
                jsonObject.put("b_state", stateCode);
                Log.e("State","code"+stateCode);
                jsonObject.put("b_country", countryCode);
                jsonObject.put("b_zipcode", editzip);
                jsonObject.put("b_phone", editphone);
                jsonObject.put("s_firstname", editfirstname);
                jsonObject.put("s_lastname", editlastname);
                jsonObject.put("s_address", editaddress1);
                jsonObject.put("s_address_2", editaddress2);
                jsonObject.put("s_city", editcity);
                jsonObject.put("s_state", stateCode);
                jsonObject.put("s_country", countryCode);
                jsonObject.put("s_zipcode", editzip);
                jsonObject.put("s_phone", editphone);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "address/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("AddressPut-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("AddressPut-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.e("result",""+result);
            // progressDialog.dismiss();

            db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
            String name = db.getName();
            db.deleteName(name);
            mainCardModel = new MainCardModel(editfirstname);
            db.addName(mainCardModel);
            // Preferences.add(Preferences.PrefType.First_name, editfirstname, getApplicationContext());
            Preferences.add(Preferences.PrefType.Last_name, editlastname, getApplicationContext());
            Preferences.add(Preferences.PrefType.ADDRESS, editaddress1, getApplicationContext());
            Preferences.add(Preferences.PrefType.ADDRESS2, editaddress2, getApplicationContext());
            Preferences.add(Preferences.PrefType.CITY, editcity, getApplicationContext());
            Preferences.add(Preferences.PrefType.STATE, stateCode, getApplicationContext());
            Preferences.add(Preferences.PrefType.COUNTRY, countryCode, getApplicationContext());
            Preferences.add(Preferences.PrefType.ZIP, editzip, getApplicationContext());
            // Preferences.add(Preferences.PrefType.STATE,editstate,getApplicationContext());
            Preferences.add(Preferences.PrefType.PHONE, editphone, getApplicationContext());
            new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);



        }

    }

    private class GetEmail extends AsyncTask<String, Void, Void> {
        String User_id;
        private StringBuffer stringBuffer = new StringBuffer();

        // JSONObject jsonArray=new JSONArray();
        public GetEmail(String user_id) {
            this.User_id = user_id;
        }


        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;

            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "users/" + uid);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("EmailGet-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("EmailGet-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                    Log.e("string", String.valueOf(stringBuffer));
                    Email = jsonObject.getString("email");
                    Log.e("mail", Email);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            email.setText(Email);
            // progressDialog.dismiss();
            Preferences.add(Preferences.PrefType.EMAIL, Email, getApplicationContext());


        }
    }

    private class PutFirstname extends AsyncTask<String, String, Void> {
        String Firstname, Lastname, User_id;

        public PutFirstname(String firstname, String lastname, String user_id) {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.User_id = user_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... params) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("b_firstname", Firstname);
                jsonObject.put("b_lastname", Lastname);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "address/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                HttpResponse httpResponse = httpClient.execute(put);
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
           // progressDialog.dismiss();
            db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
            String n = db.getName();
            db.deleteName(n);
            mainCardModel = new MainCardModel(Firstname);
            db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
            db.addName(mainCardModel);
            //Preferences.add(Preferences.PrefType.First_name, Firstname, getApplicationContext());
            Preferences.add(Preferences.PrefType.Last_name, Lastname, getApplicationContext());


        }
    }
    private class PutUserFirstname extends AsyncTask<String, String, Void> {
        String Firstname, Lastname, User_id;

        public PutUserFirstname(String firstname, String lastname, String user_id) {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.User_id = user_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... params) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstname", Firstname);
                jsonObject.put("lastname", Lastname);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "users/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                HttpResponse httpResponse = httpClient.execute(put);
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    Log.e("View", String.valueOf(syncItemView));
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String count = db.getCartCount(uid);
                    //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(ProfileActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            //noinspection SimplifiableIfStatement
            case R.id.action_cart:
                Intent cartintent = new Intent(this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        ProfileActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    private class GetCountries extends AsyncTask<Void, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        String User_id;


        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Cslist");
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Country-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Country-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                JSONArray jsonArray = new JSONArray();
                jsonArray = jobj.getJSONArray("country");
                cname.removeAll(cname);
                coname.removeAll(coname);

               /* JSONObject jsonObject=new JSONObject(String.valueOf(jsonArray));
                Log.e("JsonObject", String.valueOf(jsonObject));*/
                for (int i = 0; i < jsonArray.length(); ++i) {
                    JSONArray jsonArray1 = jsonArray.optJSONArray(i);
                    //Log.e("JsonArray", "Length" + jsonArray.length());
                    if (jsonArray1 != null) {
                        try {
                            for (int j = 0; j < jsonArray1.length(); ++j) {
                                Countries countries = new Countries("code", "country");
                                JSONObject jsonObject = jsonArray1.getJSONObject(j);
                               /* Iterator<String> keys = jsonObject.keys();
                                while (keys.hasNext()) {
                                    //Log.e("JArray in for loop ", String.valueOf(jArray));
                                    String key = keys.next();
                                    Log.e("Keys", "" + key);
                                }*/
                                String status = jsonObject.getString("status");
                                if (status == "A" || status.equals("A")) {
                                    countries.setCountry(jsonObject.getString("country"));
                                    countries.setCode(jsonObject.getString("code"));
                                    String country = jsonObject.getString("country");
                                    cname.add(country);
                                    coname.add(countries);
                                }
                            }

                        } catch (Exception e) {

                        }
                    }
                }


                JSONArray jsonArray1 = new JSONArray();



                //Checkout cout=new Checkout();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                    //progressDialog.dismiss();

            }catch (Exception e)
            {

            }

        }
    }

    private class GetStates extends AsyncTask<String, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        String c;

        public GetStates(String countryCode) {
            this.c = countryCode;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
           // progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Cslist?code=" + c);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("States-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("States-After1", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));

                stname.removeAll(stname);
                sname.removeAll(sname);

                JSONObject jsonObject = jobj.getJSONObject("states");
                s = jobj.getJSONObject("states");
                Log.e("States", "json" + jsonObject);

                //stname.add("Please select");

                if (jsonObject != null) {
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        State st = new State("Code", "name");
                        String key = keys.next();

                        String value = jsonObject.getString(key);
                        st.setStateCode(key);
                        st.setStateName(jsonObject.getString(key));
                        sname.add(st);
                        stname.add(value);

                    }

                }
                else
                {
                    sname=null;
                    stname=null;
                }

            } catch (Exception e) {
            }
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


            //Checkout cout=new Checkout();


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                if (stname != null && stname.size() > 0) {
                    final ArrayAdapter<String> a1 = new ArrayAdapter<String>(ProfileActivity.this, R.layout.spinner_item, stname);
                    a1.setDropDownViewResource(R.layout.spinner_item);

                    spinnerState.post(new Runnable() {
                        @Override
                        public void run() {
                            if (Preferences.getString(Preferences.PrefType.STATE, getApplicationContext()) != null) {
                                spinnerState.setSelection(a1.getPosition(Preferences.getString(Preferences.PrefType.STATE, getApplicationContext())));
                                Log.e("Selected", "" + a1.getPosition(Preferences.getString(Preferences.PrefType.STATE, getApplicationContext())));

                            }
                        }
                    });
                    spinnerState.setAdapter(a1);


                   // spinnerState.setAdapter(a1);
                    spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            stName = spinnerState.getSelectedItem().toString();
                            Log.e("state ","Name"+stName);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    final ArrayAdapter<String> a = new ArrayAdapter<String>(ProfileActivity.this, R.layout.spinner_item, stname);
                    a.setDropDownViewResource(R.layout.spinner_item);

                    spinnerState.setAdapter(a);


                }


            }catch (Exception e)
            {

            }
            progressDialog.dismiss();

        }
    }

    private class GetUserAddress extends AsyncTask<String, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jsonObject = new JSONObject();
        String User_id;

        public GetUserAddress(String user_id) {
            this.User_id = user_id;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri =  new URI(getString(R.string.url) + "address/" + User_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Address-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Address-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                jsonObject = jobj.put("Checkout", true);

                String f,l;
                //Checkout cout=new Checkout();
                if(jobj.getString("b_firstname").equals("") || jobj.getString("b_lastname").equals("") )
                {
                   if(jobj.getString("b_firstname").equals("")) {
                       f = jobj.getString("firstname");
                   }
                    else
                   {
                       f=jobj.getString("b_firstname");
                   }

                    if(jobj.getString("b_lastname").equals(""))
                    {
                        l=jobj.getString("lastname");
                    }
                    else
                    {
                        l=jobj.getString("b_lastname");
                    }
                    firstname=f + " "+ l;
                }
                else {
                    firstname = jobj.getString("b_firstname") + " " + jobj.getString("b_lastname");
                }
                Log.e("Firstname",""+firstname);
                countryname = jobj.getString("b_country_descr");
                Log.e("Country",""+countryname);
                Country = jobj.getString("b_country_descr");
                Log.e("Country",""+Country);
                addr = jobj.getString("b_address");
                address2 = jobj.getString("b_address_2");
                cityname = jobj.getString("b_city");
                if(jobj.has("b_state_descr")) {
                    statename = jobj.getString("b_state_descr");
                    Log.e("State",""+statename);
                }

                zipcode = jobj.getString("b_zipcode");
                phoneno = jobj.getString("b_phone");
                if(jobj.getString("b_firstname").equals("")) {
                fname=jobj.getString("firstname");
                }
                else {
                    fname = jobj.getString("b_firstname");
                }
                if(jobj.getString("b_lastname").equals(""))
                {
                    lname=jobj.getString("lastname");
                }
                else {
                    lname = jobj.getString("b_lastname");
                }
                City = jobj.getString("b_city");
                Country = jobj.getString("b_country_descr");
                addres = jobj.getString("b_address");
                addres2 = jobj.getString("b_address_2");
                if(jobj.has("b_state_descr")) {
                    State = jobj.getString("b_state_descr");
                    Log.e("state","name"+State);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {

                progressDialog.dismiss();

                name.setText(firstname);
                address.setText(addr);
                address_2.setText(address2);
                city.setText(cityname);

                state.setText(statename);
                country.setText(Country);
                zip.setText(zipcode);
                phone.setText(phoneno);

                String n = db.getName();
                db.deleteName(n);
                mainCardModel = new MainCardModel(fname);
                db.addName(mainCardModel);
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                //Preferences.add(Preferences.PrefType.First_name, fname, getApplicationContext());
                Preferences.add(Preferences.PrefType.Last_name, lname, getApplicationContext());
                Preferences.add(Preferences.PrefType.ADDRESS, addres, getApplicationContext());
                Preferences.add(Preferences.PrefType.ADDRESS2, addres2, getApplicationContext());
                Preferences.add(Preferences.PrefType.PHONE, phoneno, getApplicationContext());
                Preferences.add(Preferences.PrefType.ZIP, zipcode, getApplicationContext());
                Preferences.add(Preferences.PrefType.CITY, City, getApplicationContext());
                Preferences.add(Preferences.PrefType.STATE, statename, getApplicationContext());
                Preferences.add(Preferences.PrefType.COUNTRY, Country, getApplicationContext());

            } catch (Exception e) {
                Log.e("tvk Exception occured",(e.getMessage()==null)?"failed!":e.getMessage());
                Toast.makeText(ProfileActivity.this, "Please Check your internet connection!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
