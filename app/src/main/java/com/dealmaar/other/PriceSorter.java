package com.dealmaar.other;

import com.dealmaar.models.Product;

import java.util.Comparator;

/**
 * Created by vamsi on 27-Jun-16.
 */
public class PriceSorter implements Comparator<Product>{

    public int compare(Product one, Product another){
        int returnVal = 0;

        if(Integer.parseInt(one.getBase_price()) <  Integer.parseInt(another.getBase_price())){
            returnVal =  -1;
        }else if(Integer.parseInt(one.getBase_price()) >  Integer.parseInt(another.getBase_price())){
            returnVal =  1;
        }else if(Integer.parseInt(one.getBase_price()) == Integer.parseInt( another.getBase_price())){
            returnVal =  0;
        }
        return returnVal;
    }
}
