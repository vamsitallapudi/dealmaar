// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.dealmaar.common;

import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;

public class MainCardModel
    implements Serializable
{

    public static String KEY_CART_ARGS = "FAVORITE_KEY";
    public static String KEY_FAVORITE_ARGS = "FAVORITE_KEY";
    private boolean addToCart;
    private boolean addToFavorite;
    private String brandDesc;
    private String discount;
    private String headerTitle;
    private String id;
    private String imageUrl;
    private ArrayList imagesDetailUrl;
    private String listImageUrl;
    private String modelDesc;
    public String price1;
    public String price2;
    private String productDesc;
    private String quantity;
    private int quantityIndex;
    private float rating;
    public String recentlyImageURL;
    private ArrayList relatedProductList;
    private String secondaryTitle;
    private String size;
    private int sizeIndex;
    public String status;
    public String productname;
    public String name;
    public String cartcount;
    public String userid;
    

    public MainCardModel()
    {
        status = "Available";
        imagesDetailUrl = new ArrayList();
        relatedProductList = new ArrayList();
        addToFavorite = false;
        addToCart = false;
        quantityIndex = 0;
    }

    public MainCardModel(String product_id, String product_name, String productold_price, String productnew_price, String product_desc, ImageView productimage) {
        this.id=product_id;
        this.productname=product_name;
        this.price1=productold_price;
        this.price2=productnew_price;
        this.productDesc=product_desc;
        this.imageUrl= String.valueOf(productimage);

    }

    public MainCardModel(String name) {
        this.name=name;
    }
    public MainCardModel(String user,String count)
    {
        this.userid=user;
        this.cartcount=count;

    }

    public String getUserid()
    {
        return userid;
    }
    public String getCartCount()
    {
        return cartcount;
    }
    public void setCartcount(String cartcount)
    {
        this.cartcount=cartcount;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }


    public String getProductname()
    {
        return productname;
    }

    public String getBrandDesc()
    {
        return brandDesc;
    }

    public String getDiscount()
    {
        return discount;
    }

    public String getHeaderTitle()
    {
        return headerTitle;
    }

    public String getId()
    {
        return id;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public ArrayList getImagesDetailUrl()
    {
        return imagesDetailUrl;
    }

    public String getListImageUrl()
    {
        return listImageUrl;
    }

    public String getModelDesc()
    {
        return modelDesc;
    }

    public String getPrice1()
    {
        return price1;
    }

    public String getPrice2()
    {
        return price2;
    }

    public String getProductDesc()
    {
        return productDesc;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public int getQuantityIndex()
    {
        return quantityIndex;
    }

    public float getRating()
    {
        return rating;
    }

    public String getRecentlyImageURL()
    {
        return recentlyImageURL;
    }

    public ArrayList getRelatedProductList()
    {
        return relatedProductList;
    }

    public String getSecondaryTitle()
    {
        return secondaryTitle;
    }

    public String getSize()
    {
        return size;
    }

    public int getSizeIndex()
    {
        return sizeIndex;
    }

    public String getStatus()
    {
        return status;
    }

    public boolean isAddToCart()
    {
        return addToCart;
    }

    public boolean isAddToFavorite()
    {
        return addToFavorite;
    }

    public void setAddToCart(boolean flag)
    {
        addToCart = flag;
    }

    public void setAddToFavorite(boolean flag)
    {
        addToFavorite = flag;
    }

    public void setBrandDesc(String s)
    {
        brandDesc = s;
    }

    public void setDiscount(String s)
    {
        discount = s;
    }

    public void setHeaderTitle(String s)
    {
        headerTitle = s;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setImageUrl(String s)
    {
        imageUrl = s;
    }

    public void setImagesDetailUrl(ArrayList arraylist)
    {
        imagesDetailUrl = arraylist;
    }

    public void setListImageUrl(String s)
    {
        listImageUrl = s;
    }

    public void setModelDesc(String s)
    {
        modelDesc = s;
    }

    public void setPrice1(String bigdecimal)
    {
        price1 = bigdecimal;
    }

    public void setPrice2(String bigdecimal)
    {
        price2 = bigdecimal;
    }

    public void setProductDesc(String s)
    {
        productDesc = s;
    }

    public void setQuantity(String s)
    {
        quantity = s;
    }

    public void setQuantityIndex(int i)
    {
        quantityIndex = i;
    }

    public void setRating(float f)
    {
        rating = f;
    }
    public void setProductname(String name)
    {
        productname=name;
    }

    public void setRecentlyImageURL(String s)
    {
        recentlyImageURL = s;
    }

    public void setSecondaryTitle(String s)
    {
        secondaryTitle = s;
    }

    public void setSize(String s)
    {
        size = s;
    }

    public void setSizeIndex(int i)
    {
        sizeIndex = i;
    }

    public void setStatus(String s)
    {
        status = s;
    }

}
