package com.dealmaar.common;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by omm on 2/13/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final String CREATE_TABLE_CART = "CREATE TABLE cart(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER,index_size INTEGER, index_quantity INTEGER);";
    private static final String CREATE_TABLE_FAVORITE = "CREATE TABLE favorite(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER );";
    private static final String CREATE_TABLE_RECENTLY_VIEWED = "CREATE TABLE recently_viewed(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER);";
    private static final String CREATE_TABLE_NAME="CREATE TABLE name(id INTEGER PRIMARY KEY AUTOINCREMENT,name STRING);";
    private static final String CREATE_TABLE_CARTCOUNT="CREATE TABLE cartcount(id INTEGER PRIMARY KEY AUTOINCREMENT,user STRING,cartcount STRING);";
    public static String DATABASE_NAME = "dealmaar_database";
    private static final int DATABASE_VERSION = 1;
    private static final String KEY_ID = "id";
    private static final String KEY_INDEX_QUANTITY = "index_quantity";
    private static final String KEY_INDEX_SIZE = "index_size";
    private static final String KEY_MAIN_CARD_MODEL_ID = "main_card_model_id";
    private static final String KEY_PRODNAME = "name";
    private static final String KEY_OLD_PRICE = "price1";
    private static final String KEY_NEW_PRICE = "price2";
    private static final String KEY_PROD_DESC = "description";
    private static final String KEY_IMAGE_URL="imageurl";
    private static final String TABLE_CART = "cart";
    private static final String TABLE_FAVORITE = "favorite";
    private static final String TABLE_RECENTLY_VIEWED = "recently_viewed";

    public DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    public DatabaseHandler(Context context, String s, android.database.sqlite.SQLiteDatabase.CursorFactory cursorfactory, int i)
    {
        super(context, s, cursorfactory, i);
    }

    public DatabaseHandler(Context context, String s, android.database.sqlite.SQLiteDatabase.CursorFactory cursorfactory, int i, DatabaseErrorHandler databaseerrorhandler)
    {
        super(context, s, cursorfactory, i, databaseerrorhandler);
    }

   /* public long addCart(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("main_card_model_id", maincardmodel.getId());
        contentvalues.put("index_size", Integer.valueOf(maincardmodel.getSizeIndex()));
        contentvalues.put("index_quantity", Integer.valueOf(maincardmodel.getQuantityIndex()));
        return sqlitedatabase.insert("cart", null, contentvalues);
    }*/

   /* public long addFavorite(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("main_card_model_id", Integer.valueOf(maincardmodel.getId()));
        Log.e("INSERT", maincardmodel.getId());
        long l = sqlitedatabase.insert("favorite", null, contentvalues);
        sqlitedatabase.close();
        return l;
    }*/

    public long addCartCount(MainCardModel mainCardModel)
    {
        SQLiteDatabase sqLitedatabase=getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put("user",mainCardModel.getUserid());
        contentvalues.put("cartcount",mainCardModel.getCartCount());
        long l;
        l=sqLitedatabase.insert("cartcount",null,contentvalues);
        Log.e("Content", String.valueOf(contentvalues));
        sqLitedatabase.close();
        return l;
    }
    public long addName(MainCardModel mainCardModel)
    {
        SQLiteDatabase sqlitedatabase=getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        Log.e("Name", mainCardModel.getName());
        contentvalues.put("name", mainCardModel.getName());
        long l;
        l = sqlitedatabase.insert("name", null, contentvalues);
        Log.e("Content", String.valueOf(contentvalues));
        sqlitedatabase.close();
        return l;
    }

    public long addRecentlyViewed(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("main_card_model_id", maincardmodel.getId());
        Log.e("id", maincardmodel.getId());
        String id=maincardmodel.getId();
        long l;
       /* contentvalues.put("name", maincardmodel.getProductname());
        contentvalues.put("price1", String.valueOf(maincardmodel.getPrice1()));
        contentvalues.put("price2", String.valueOf(maincardmodel.getPrice2()));
        contentvalues.put("description",maincardmodel.getProductDesc());
        contentvalues.put("imageurl",maincardmodel.getImageUrl());*/
        ArrayList arrayList=new ArrayList<>();
        arrayList=getAllRecentlyViewedList();
        Log.e("arrayList", String.valueOf(arrayList));
        Log.e("arrayid",id);
        if(arrayList.contains(id)) {
            l=sqlitedatabase.getPageSize();
            sqlitedatabase.close();
        }
        else {
            l = sqlitedatabase.insert("recently_viewed", null, contentvalues);
            Log.e("Content", String.valueOf(contentvalues));
            sqlitedatabase.close();
           // return l;
        }
        if(arrayList.size()>10)
        {


            Log.e("Entered","if");
            deleteRecentlyViewed(arrayList.get(10).toString());
           // deleteRecentlyViewed(arrayList.get(1).toString());
        }

        //sqlitedatabase.close();
        return l;
    }
    public void deleteRecentlyViewed(String s)
    {
        getWritableDatabase().delete("recently_viewed","main_card_model_id=?",new String[]
                {
                   s
                });
    }
    public void deleteCartCount(String s)
    {
        getWritableDatabase().delete("cartcount","user=?",new String[]
        {
                s
        });
    }
    public void deleteName(String s)
    {
        getWritableDatabase().delete("name","name=?",new String[]
        {
            s
        });
    }
/*
    public void deleteCartEntry(String s)
    {
        getWritableDatabase().delete("cart", "main_card_model_id = ?", new String[] {
                s
        });
    }


    public void deleteFavoriteEntry(String s)
    {
        getWritableDatabase().delete("favorite", "main_card_model_id = ?", new String[] {
                s
        });
    }*/

    /*public ArrayList getAllCartList()
    {
        ArrayList arraylist = new ArrayList();
        Cursor cursor = getReadableDatabase().rawQuery("SELECT  * FROM cart", null);
        if (cursor.moveToFirst())
        {
            do
            {
                cursor.getString(0);
                Object obj = cursor.getString(1);
                String s = cursor.getString(2);
                String s1 = cursor.getString(3);
                //obj = ApplicationService.getMainCardModel(Integer.valueOf(((String) (obj))).intValue());
                ((MainCardModel) (obj)).setSizeIndex(Integer.valueOf(s).intValue());
                ((MainCardModel) (obj)).setQuantityIndex(Integer.valueOf(s1).intValue());
                arraylist.add(obj);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arraylist;
    }*/

   /* public ArrayList getAllFavoriteList()
    {
        ArrayList arraylist = new ArrayList();
        Cursor cursor = getReadableDatabase().rawQuery("SELECT  * FROM favorite", null);
        if (cursor.moveToFirst())
        {
            do
            {
                cursor.getString(0);
               // arraylist.add(ApplicationService.getMainCardModel(Integer.valueOf(cursor.getString(1)).intValue()));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arraylist;
    }
*/
    /*public ArrayList getAllFavoriteList(int i, int j)
    {
        ArrayList arraylist = new ArrayList();
        Object obj = (new StringBuilder()).append("SELECT  * FROM favorite LIMIT ").append(i).append(", ").append(j).toString();
        obj = getReadableDatabase().rawQuery(((String) (obj)), null);
        if (((Cursor) (obj)).moveToFirst())
        {
            do
            {
              //  arraylist.add(ApplicationService.getMainCardModel(((Cursor) (obj)).getColumnIndex("main_card_model_id")));
            } while (((Cursor) (obj)).moveToNext());
        }
        ((Cursor) (obj)).close();
        return arraylist;
    }*/
    public String getName()
    {
        String s=null;
        Cursor cursor=getReadableDatabase().rawQuery("SELECT * FROM name",null);
        if(cursor.moveToFirst())
        {
            do {
                cursor.getString(0);
                s=cursor.getString(1);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return s;
    }

    public String getCartCount(String id)
    {
        String s=null;
        Cursor cursor=getReadableDatabase().rawQuery("SELECT * FROM cartcount where user =? ",new String[]
                {
                        id
                });
        if(cursor.moveToFirst())
        {
            do {
                cursor.getString(0);
                s=cursor.getString(2);

            }while (cursor.moveToNext());
        }
        cursor.close();
        return s;
    }
    public ArrayList getAllRecentlyViewedList()
    {
        ArrayList arraylist = new ArrayList<>();
        Cursor cursor = getReadableDatabase().rawQuery("SELECT  * FROM recently_viewed ORDER BY id DESC", null);
        if (cursor.moveToFirst())
        {
            do
            {
                cursor.getString(0);
                Object object=cursor.getString(1);
               /* String s=cursor.getString(2);
                String s1=cursor.getString(3);
                String s2=cursor.getString(4);
                String s3=cursor.getString(5);*/

                //arraylist.add(ApplicationService.getMainCardModel(Integer.valueOf(cursor.getString(1)).intValue()));
               /* ((MainCardModel)(object)).setProductname(String.valueOf(Integer.valueOf(s).intValue()));
                ((MainCardModel)(object)).setPrice1(String.valueOf((Integer.valueOf(s).intValue())));
                ((MainCardModel)(object)).setPrice2(String.valueOf(Integer.valueOf(s).intValue()));
                ((MainCardModel)(object)).setProductDesc(String.valueOf(Integer.valueOf(s).intValue()));
                ((MainCardModel)(object)).setImageUrl(String.valueOf(Integer.valueOf(s).intValue()));*/
                arraylist.add(object);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arraylist;
    }

   /* public MainCardModel getCart(String s)
    {
        //Object obj = null;
        MainCardModel obj=null;
        Cursor cursor = getReadableDatabase().rawQuery((new StringBuilder()).append("SELECT  * FROM cart WHERE main_card_model_id = ").append(s).toString(), null);
       // s = obj;
        if (cursor != null)
        {
            //s = obj;
            if (cursor.moveToFirst())
            {
                int i = cursor.getInt(cursor.getColumnIndex("main_card_model_id"));
                int j = cursor.getInt(cursor.getColumnIndex("index_size"));
                int k = cursor.getInt(cursor.getColumnIndex("index_quantity"));
                s = null;//ApplicationService.getMainCardModel(i);
                s.setSizeIndex(j);
                s.setQuantityIndex(k);
                cursor.close();
            }
        }
        return obj;
    }*/

  /*  public int getCartCount()
    {
        return Long.valueOf(DatabaseUtils.queryNumEntries(getReadableDatabase(), "cart")).intValue();
    }*/

   /* public MainCardModel getFavorite(String s)
    {
       // Object obj = null;
        MainCardModel obj=null;
        Cursor cursor = getReadableDatabase().rawQuery((new StringBuilder()).append("SELECT  * FROM favorite WHERE main_card_model_id = ").append(s).toString(), null);
        //s = obj;
        if (cursor != null)
        {
           // s = obj;
            if (cursor.moveToFirst())
            {
                //s = ApplicationService.getMainCardModel(cursor.getInt(cursor.getColumnIndex("main_card_model_id")));
                cursor.close();
            }
        }
        return obj;
    }*/

   /* public int getFavoriteCount()
    {
        return Long.valueOf(DatabaseUtils.queryNumEntries(getReadableDatabase(), "favorite")).intValue();
    }*/

  /*  public MainCardModel getRecentlyViewed(String s)
    {
        //Object obj = null;
        MainCardModel obj=null;
        Cursor cursor = getReadableDatabase().rawQuery((new StringBuilder()).append("SELECT  * FROM recently_viewed WHERE main_card_model_id = ").append(s).toString(), null);
       // s = obj;
        if (cursor != null)
        {
           // s = obj;
            if (cursor.moveToFirst())
            {
                int i = cursor.getInt(cursor.getColumnIndex("main_card_model_id"));
                int j = cursor.getInt(cursor.getColumnIndex("name"));
                int k = cursor.getInt(cursor.getColumnIndex("price1"));
                int l = cursor.getInt(cursor.getColumnIndex("price2"));
                int m = cursor.getInt(cursor.getColumnIndex("description"));
                int n = cursor.getInt(cursor.getColumnIndex("imageurl"));
                //s = ApplicationService.getMainCardModel(cursor.getInt(cursor.getColumnIndex("main_card_model_id")));
                s=
                cursor.close();
            }
        }
        return obj;
    }*/
    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        sqlitedatabase.execSQL("CREATE TABLE favorite(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER );");
        sqlitedatabase.execSQL("CREATE TABLE cart(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER,index_size INTEGER, index_quantity INTEGER);");
        sqlitedatabase.execSQL("CREATE TABLE recently_viewed(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER);");
        sqlitedatabase.execSQL("CREATE TABLE name(id INTEGER PRIMARY KEY AUTOINCREMENT,name STRING);");
        sqlitedatabase.execSQL("CREATE TABLE cartcount(id INTEGER PRIMARY KEY AUTOINCREMENT,user STRING,cartcount STRING);");
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS favorite");
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS cart");
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS name");
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS cartcount");
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS CREATE TABLE recently_viewed(id INTEGER PRIMARY KEY AUTOINCREMENT,main_card_model_id INTEGER,name STRING,price1 STRING,price2 STRING,description STRING,imageurl STRING);");
        onCreate(sqlitedatabase);
    }

    public int updateCartQtyEntry(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("index_quantity", Integer.valueOf(maincardmodel.getQuantityIndex()));
        return sqlitedatabase.update("cart", contentvalues, "main_card_model_id = ?", new String[] {
                maincardmodel.getId()
        });
    }

    public int updateCartSizeEntry(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("index_size", Integer.valueOf(maincardmodel.getSizeIndex()));
        return sqlitedatabase.update("cart", contentvalues, "main_card_model_id = ?", new String[] {
                maincardmodel.getId()
        });
    }

    public int updateEntry(MainCardModel maincardmodel)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("main_card_model_id", maincardmodel.getId());
        return sqlitedatabase.update("favorite", contentvalues, "main_card_model_id = ?", new String[] {
                maincardmodel.getId()
        });
    }
}
