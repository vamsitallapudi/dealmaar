package com.dealmaar.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.batch.android.Batch;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.SigninActivity;

/**
 * Created by omm on 1/24/2016.
 */
public class LaunchActivity extends AppCompatActivity{
    String uid, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Batch batch;


        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        Log.e("uid", uid == null ? "Null" : "Not null");

        if (uid == null) {
           /* user_id="-1001";
            Preferences.add(Preferences.PrefType.User_id,user_id,getApplicationContext());*/
            Intent intent = new Intent(LaunchActivity.this, SigninActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LaunchActivity.this, HomeActivity.class);
            Batch.onStart(this);
            startActivity(intent);
            finish();
        }
        //onBackPressed();

       // Log.e("Batch", String.valueOf(Batch.User));

    }
    @Override
    protected void onStart()
    {
        super.onStart();

        Batch.onStart(this);
    }

    @Override
    protected void onStop()
    {
        Batch.onStop(this);

        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        Batch.onDestroy(this);

        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        Batch.onNewIntent(this, intent);

        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    // Before 2.0
    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/
}
