package com.dealmaar.checkout;

import java.io.Serializable;

/**
 * Created by omm on 12/22/2015.
 */
public class Checkout implements Serializable {

    private String b_firstname;
    private String b_lastname;
    private String b_phone, b_address, b_address_2, b_city, b_state, b_country, b_zip;

    public String getB_firstname() {
        return b_firstname;
    }

    public void setB_firstname(String b_firstname) {
        this.b_firstname = b_firstname;
    }

    public String getB_lastname() {
        return b_lastname;

    }

    public void setB_lastname(String b_lastname) {
        this.b_lastname = b_lastname;
    }

    public String getB_phone() {
        return b_phone;
    }

    public void setB_phone(String b_phone) {
        this.b_phone = b_phone;
    }

    public String getB_address() {
        return b_address;

    }

    public void setB_address(String b_address) {
        this.b_address = b_address;
    }

    public String getB_address_2() {
        return b_address_2;
    }

    public void setB_address_2(String b_address_2) {
        this.b_address_2 = b_address_2;
    }

    public String getB_city() {
        return b_city;
    }

    public void setB_city(String b_city) {
        this.b_city = b_city;
    }

    public String getB_state() {
        return b_state;
    }

    public void setB_state(String b_state) {
        this.b_state = b_state;
    }

    public String getB_country() {
        return b_country;
    }

    public void setB_country(String b_country) {
        this.b_country = b_country;
    }

    public String getB_zip() {
        return b_zip;
    }

    public void setB_zip(String b_zip) {
        this.b_zip = b_zip;
    }

}
