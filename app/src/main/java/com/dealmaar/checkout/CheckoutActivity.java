package com.dealmaar.checkout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dealmaar.cart.Cart;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.payment.activity.WebViewActivity;
import com.dealmaar.payment.utility.AvenuesParams;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

public class CheckoutActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    ListView list;
    ArrayList<Checkout> Category = new ArrayList<>();
    ArrayList<Checkout> Category1 = new ArrayList<>();
    Button next, previous;
    String order_id, shipping,uid,Firstname, Lastname, Address, Address1, City, State, Country, Zip, Phone,fname, lname, address, address1, city, state, country, zip, phone,shippingId,countryCode;
    ArrayList<Cart> arrayCartList = new ArrayList<>();
    double total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        countryCode = "IN";
        final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        list = (ListView) findViewById(R.id.checkout_address_list);
        //editAddress=(Button) findViewById(R.id.ch)
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        //total=getIntent().getExtras().getDouble("Total");
        if (getIntent().getExtras() != null) {
            String CalledFromDelivery = getIntent().getExtras().getString("CalledfromDeliveryAddress");
            String CalledfromDeliveryadd = getIntent().getExtras().getString("CalledFromEditDelivery");
            String CalledFromCart = getIntent().getExtras().getString("CalledfromCart");
            String CalledFromCancel = getIntent().getExtras().getString("CalledfromCancel");
            arrayCartList = (ArrayList<Cart>) getIntent().getSerializableExtra("Cartlist");
            /*arrayCartList=(ArrayList<Cart>)getIntent().getSerializableExtra("Cartlist");
            Log.e("l", String.valueOf(arrayCartList.size()));
            total=getIntent().getExtras().getDouble("Total");*/
            shipping = getIntent().getStringExtra("Shipping");
            fname = getIntent().getExtras().getString("Firstname");
            lname = getIntent().getExtras().getString("Lastname");
            address = getIntent().getExtras().getString("Address");
            address1 = getIntent().getExtras().getString("Address1");
            city = getIntent().getExtras().getString("City");
            state = getIntent().getExtras().getString("State");
            country = getIntent().getExtras().getString("Country");
            zip = getIntent().getExtras().getString("Zip");
            phone = getIntent().getExtras().getString("Phone");
            shippingId=getIntent().getExtras().getString("shippingId");


            if (CalledFromDelivery != null) {
                new EditAddress(uid).execute();
                next = (Button) findViewById(R.id.checkout_next);
                previous = (Button) findViewById(R.id.checkout_previous);
                previous.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myintent = new Intent(CheckoutActivity.this, CartActivity.class);
                        startActivity(myintent);
                        finish();
                    }
                });

            }
            if (CalledFromCart != null || CalledfromDeliveryadd != null || CalledFromCancel != null) {
                new GetUserAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);

                next = (Button) findViewById(R.id.checkout_next);
                previous = (Button) findViewById(R.id.checkout_previous);
                previous.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myintent = new Intent(CheckoutActivity.this, CartActivity.class);
                        startActivity(myintent);
                        finish();
                    }
                });
                next.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if ((Firstname.equals("")) || (Lastname.equals("")) || (Address.equals("")) || (City.equals("")) || (State.equals(""))) {
                            Toast.makeText(getApplicationContext(), "Address is required !",
                                    Toast.LENGTH_LONG).show();
                        } else {

                            new CreateOrder().executeOnExecutor(MyApplication.threadPoolExecutor);
                        }
                    }
                });
            }
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
        finish();
    }

    private class EditAddress extends AsyncTask<String, Void, Void> {

        String userid;

        public EditAddress(String user_id) {
            this.userid = user_id;
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Checkout cout = new Checkout();
                cout.setB_firstname(fname + " " + lname);
                cout.setB_phone(phone);
                cout.setB_address(address + ",");
                cout.setB_address_2(address1);
                cout.setB_city(city);
                cout.setB_state(state);
                cout.setB_country(country);
                cout.setB_zip(zip);
                Category.add(cout);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                // totalAmt= String.valueOf(total+Double.parseDouble(shipping));
                CheckoutAdapter adapter = new CheckoutAdapter(getApplicationContext(), R.layout.checkout_deliveryaddress_item, Category, arrayCartList, total, shipping,shippingId);
                // productsList.clear();
                list.setAdapter(adapter);
            } catch (Exception e) {
               // Toast.makeText(CheckoutActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private class CreateOrder extends AsyncTask<Void, Void, String> {

        private String GetLocalIpAddress() {
            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            } catch (SocketException ex) {
                return "ERROR Obtaining IP";
            }
            return "No IP Available";
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CheckoutActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            JSONObject orderjsonObject = new JSONObject();


            JSONObject productsjsonObject = new JSONObject();
            try {


                arrayCartList = (ArrayList<Cart>) getIntent().getSerializableExtra("Cartlist");
                for (int i = 0; i < arrayCartList.size(); ++i) {
                    int amt = Integer.parseInt(arrayCartList.get(i).getAmount());
                    double Tot, price;
                    price = Double.parseDouble(arrayCartList.get(i).getPrice());
                    Tot = price;
                    if (amt > 1) {
                        for (int j = 1; j < amt; ++j) {
                            Tot += price;
                        }
                    }
                    total += Tot;
                }
                for (int i = 0; i < arrayCartList.size(); i++) {
                    JSONObject productjsonObject = new JSONObject();
                    productjsonObject.put("item_id", arrayCartList.get(i).getItem_id());
                    productjsonObject.put("product_id", arrayCartList.get(i).getProduct_id());
                    productjsonObject.put("price", arrayCartList.get(i).getPrice());
                    productjsonObject.put("amount", arrayCartList.get(i).getAmount());
                    productjsonObject.put("status", "0");
                    productsjsonObject.put(arrayCartList.get(i).getItem_id(), productjsonObject);
                }

                orderjsonObject.put("user_id", uid);
                orderjsonObject.put("payment_id", "1");
                orderjsonObject.put("ip_address", GetLocalIpAddress());
                orderjsonObject.put("shipping_id",shippingId);
                orderjsonObject.put("products", productsjsonObject);

                Log.d("orders",orderjsonObject.toString());


                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost();
                    StringEntity se = new StringEntity(orderjsonObject.toString());
                    URI uri = new URI(getString(R.string.url) + "orders");//refer to all orders. Only GET and POST are supported.
                    httpPost.setURI(uri);
                    httpPost.addHeader(BasicScheme.authenticate(
                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                            HTTP.UTF_8, false));
                    se.setContentType("application/json;charset=UTF-8");
                    se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                    httpPost.setEntity(se);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity resultEntity = httpResponse.getEntity();
                    //resultEntity.getContent();
                    String jsonString = EntityUtils.toString(resultEntity);

                    JSONObject jsonObject = new JSONObject(jsonString);
                    order_id = jsonObject.getString("order_id");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return order_id;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Log.e("order", order_id);

            if (order_id == null) {
                Toast.makeText(getApplicationContext(), "There is a problem with order creation Please try again !", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                Intent myintent = new Intent(CheckoutActivity.this, CartActivity.class);
                startActivity(myintent);
                finish();
                return;
            } else {
                String vAccessCode = getString(R.string.accessCode);
                String redirectUrl = getString(R.string.redirectUrl);
                String cancelUrl = getString(R.string.cancelUrl);
                String rsaKey = getString(R.string.rsaKey);
                String vMerchantId = getString(R.string.merchantId);
                String vCurrency = getString(R.string.Currency);
                //String Order_id=order_id;

                Intent intent = new Intent(CheckoutActivity.this, WebViewActivity.class);
                //double vAmount=total;


                if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("")) {

                    String totalAmount = null;
                    if (shipping != null) {
                        totalAmount = String.valueOf(total + Double.parseDouble(shipping));
                    }
                    intent.putExtra(AvenuesParams.ACCESS_CODE, vAccessCode);
                    intent.putExtra(AvenuesParams.MERCHANT_ID, vMerchantId);
                    intent.putExtra(AvenuesParams.ORDER_ID, order_id);
                    intent.putExtra(AvenuesParams.CURRENCY, vCurrency);
                    intent.putExtra(AvenuesParams.AMOUNT, String.valueOf(totalAmount));

                    intent.putExtra(AvenuesParams.REDIRECT_URL, redirectUrl);
                    intent.putExtra(AvenuesParams.CANCEL_URL, cancelUrl);
                    intent.putExtra(AvenuesParams.RSA_KEY_URL, rsaKey);
                    intent.putExtra("ArrayCart", arrayCartList);
                    progressDialog.dismiss();

                    startActivity(intent);
                    finish();
                    //intent.putExtra("Order_id",order_id);
                } else {
                    showToast("All parameters are mandatory.");
                }

            }
        }
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);
                    String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(CheckoutActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private class GetUserAddress extends AsyncTask<String, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CheckoutActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        String uid;

        public GetUserAddress(String userid) {
            this.uid = userid;
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "address/" + uid);
                Log.e("url", String.valueOf(uri));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("CheckoutAddress-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("CheckoutAddress-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                jArray = jobj.put("Checkout", "true");

                Checkout cout = new Checkout();
                Firstname = jobj.getString("b_firstname");
                Lastname = jobj.getString("b_lastname");
                Log.e("Country", "" + jobj.getString("b_country_descr"));
                cout.setB_country(jobj.getString("b_country_descr"));

                cout.setB_firstname(jobj.getString("b_firstname") + " " + jobj.getString("b_lastname"));
                cout.setB_phone(jobj.getString("b_phone"));
                cout.setB_address(jobj.getString("b_address"));
                cout.setB_address_2(jobj.getString("b_address_2"));
                cout.setB_city(jobj.getString("b_city"));
                if(jobj.has("b_state_descr")) {
                    cout.setB_state(jobj.getString("b_state_descr"));
                }


                cout.setB_zip(jobj.getString("b_zipcode"));
                Category1.add(cout);

                Address = jobj.getString("b_address");
                Address1 = jobj.getString("b_address_2");
                City = jobj.getString("b_city");
                if(jobj.has("b_state_descr")) {
                    State = jobj.getString("b_state_descr");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                CheckoutAdapter adapter = new CheckoutAdapter(getApplicationContext(), R.layout.checkout_deliveryaddress_item, Category1, arrayCartList, total, shipping, shippingId);
                // productsList.clear();
                list.setAdapter(adapter);
                if(progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                Toast.makeText(CheckoutActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
