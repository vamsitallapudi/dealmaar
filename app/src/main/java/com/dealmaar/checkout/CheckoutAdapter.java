package com.dealmaar.checkout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.dealmaar.cart.Cart;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.login.MyApplication;
import com.dealmaar.profile.Countries;
import com.dealmaar.profile.State;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by omm on 12/30/2015.
 */
public class CheckoutAdapter extends ArrayAdapter<Checkout> {
    ArrayList<Checkout> ArrayListAddress;
    ArrayList<Cart> ArrayCart;
    int Resource;
    Context context;
    LayoutInflater vi;
    double Total;
    String shipping;
    Spinner spinnerState;
    ArrayList<Countries> coname;
    ArrayList<String> cname = new ArrayList<>();
    ArrayList<String> stname = new ArrayList<>();
    ArrayList<com.dealmaar.profile.State> sname = new ArrayList<>();
    String countryCode, stName, shippingId;
    JSONObject s;

    public CheckoutAdapter(Context context, int resource, ArrayList<Checkout> objects, ArrayList<Cart> objects1, double total, String totalAmt, String shippingId)

    {
        super(context, resource, objects);
        ArrayListAddress = objects;
        ArrayCart = objects1;
        Resource = resource;
        this.context = context;
        this.Total = total;
        this.shipping = totalAmt;
        this.shippingId = shippingId;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.checkout_deliveryaddress_item, parent, false);

            countryCode = "IN";
            holder = new ViewHolder();
            holder.b_firstname = (TextView) convertView.findViewById(R.id.checkout_name);
            holder.b_phone = (TextView) convertView.findViewById(R.id.checkout_phone);
            holder.b_address = (TextView) convertView.findViewById(R.id.checkout_address);
            holder.b_address_2 = (TextView) convertView.findViewById(R.id.checkout_address2);
            holder.b_city = (TextView) convertView.findViewById(R.id.checkout_city);
            holder.b_state = (TextView) convertView.findViewById(R.id.checkout_state);
            holder.b_country = (TextView) convertView.findViewById(R.id.checkout_country);
            holder.b_zip = (TextView) convertView.findViewById(R.id.checkout_zip);
            holder.editaddress = (Button) convertView.findViewById(R.id.checkout_address_edit);
            holder.uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
            coname=new ArrayList<>();
            cname=new ArrayList<>();
            stname=new ArrayList<>();
            sname=new ArrayList<>();
            new GetCountries().executeOnExecutor(MyApplication.threadPoolExecutor);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        //
        //new GetStates(countryCode).executeOnExecutor(MyApplication.threadPoolExecutor);
        holder.b_firstname.setText(ArrayListAddress.get(position).getB_firstname());
        holder.b_country.setText(ArrayListAddress.get(position).getB_country());
        holder.b_phone.setText(ArrayListAddress.get(position).getB_phone());
        holder.b_address.setText(ArrayListAddress.get(position).getB_address());
        holder.b_address_2.setText(ArrayListAddress.get(position).getB_address_2());
        holder.b_city.setText(ArrayListAddress.get(position).getB_city());
        holder.b_state.setText(ArrayListAddress.get(position).getB_state());
        holder.b_zip.setText(ArrayListAddress.get(position).getB_zip());
        holder.editaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //new GetCountries().executeOnExecutor(MyApplication.threadPoolExecutor);
                Intent myintent = new Intent(getContext(), CheckoutDeliveryAddress.class);

                myintent.putExtra("Arraylist", ArrayListAddress);
                myintent.putExtra("Cart", ArrayCart);
                myintent.putExtra("Total", Total);
                myintent.putExtra("Shipping", shipping);
                myintent.putExtra("shippingId", shippingId);
                Log.e("COname",""+coname);
                myintent.putExtra("Countries", coname);
                myintent.putExtra("Country", cname);
                myintent.putExtra("State", stname);
                myintent.putExtra("States", sname);

                myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                v.getContext().startActivity(myintent);

            }
        });
        return convertView;
    }



    private class GetCountries extends AsyncTask<Void, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();

        private JSONObject jArray = new JSONObject();


        protected void onPreExecute() {
            super.onPreExecute();
        /*ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getContext().getString(R.string.url) + "Cslist");
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("Country-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("Country-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                JSONArray jsonArray = new JSONArray();
                jsonArray = jobj.getJSONArray("country");
                   /* if (coname != null) {
                        coname.removeAll(coname);
                        //coname.clear();
                    }
                    if (cname != null) {
                        cname.removeAll(cname);
                        //cname.clear();
                    }*/

                for (int i = 0; i < jsonArray.length(); ++i) {
                    JSONArray jsonArray1 = jsonArray.optJSONArray(i);
                    if (jsonArray1 != null) {
                        try {
                            for (int j = 0; j < jsonArray1.length(); ++j) {
                                Countries countries = new Countries("code", "country");
                                JSONObject jsonObject = jsonArray1.getJSONObject(j);
                                String status = jsonObject.getString("status");
                                if (status == "A" || status.equals("A")) {
                                    countries.setCountry(jsonObject.getString("country"));
                                    countries.setCode(jsonObject.getString("code"));
                                    String country = jsonObject.getString("country");
                                    cname.add(country);
                                    coname.add(countries);
                                }
                            }

                        } catch (Exception e) {

                        }
                    }
                }

                Log.e("Arraylength", String.valueOf(jsonArray.length()));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private class GetStates extends AsyncTask<String, Void, Void> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        String c;

        public GetStates(String countryCode) {
            this.c = countryCode;
        }


        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(CheckoutDeliveryAddress.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            // progressDialog.setCancelable(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getContext().getString(R.string.url) + "Cslist?code=" + c);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("States-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("States-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));

                stname.removeAll(stname);
                sname.removeAll(sname);

                JSONObject jsonObject = jobj.getJSONObject("states");
                s = jobj.getJSONObject("states");

                if (jsonObject != null) {
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        State st = new State("Code", "name");
                        String key = keys.next();

                        String value = jsonObject.getString(key);
                        st.setStateCode(key);
                        st.setStateName(jsonObject.getString(key));
                        sname.add(st);
                        stname.add(value);

                    }
                    //progressDialog.dismiss();
                    for (int i = 0; i < sname.size(); ++i) {
                        Log.e("StateCode", "" + sname.get(i).stateCode);
                    }

                    Log.e("States", stname.toString());
                } else {
                    sname = null;
                    stname = null;
                }

            } catch (Exception e) {
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (stname != null && stname.size() > 0) {
                    final ArrayAdapter<String> a1 = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, stname);
                    a1.setDropDownViewResource(R.layout.spinner_item);
                    spinnerState.setAdapter(a1);
                    spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            stName = spinnerState.getSelectedItem().toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    final ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, stname);
                    a.setDropDownViewResource(R.layout.spinner_item);

                    spinnerState.setAdapter(a);
                }
            } catch (Exception e) {

            }


        }
    }


    static class ViewHolder {
        public TextView b_firstname, b_phone, b_address, b_address_2, b_city, b_state, b_country, b_zip;
        public Button editaddress;
        public String uid;

    }


}
