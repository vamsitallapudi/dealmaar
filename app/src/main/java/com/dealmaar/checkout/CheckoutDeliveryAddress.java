package com.dealmaar.checkout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.dealmaar.cart.Cart;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.profile.Countries;
import com.dealmaar.profile.State;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by omm on 1/11/2016.
 */
public class CheckoutDeliveryAddress extends AppCompatActivity {
    private static final String TAG = CheckoutDeliveryAddress.class.getSimpleName();
    String Firstname, Lastname, Address, Address1, City, Zip, Phone, uid, shipping, shippingId, countryCode, stateCode, coName, stName;
    EditText FirstName, LastName, Addr, Addr1, CityName, ZipCode, Phonenum;
    Button replace;
    ImageButton ok, cancel;
    ProgressDialog progressDialog;
    ArrayList<Checkout> checkoutAddress = new ArrayList<>();
    ArrayList<Cart> arrayCartList = new ArrayList<>();
    double total;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    Spinner spinnerCountry, spinnerState;
    ArrayList<Countries> coname;
    ArrayList<String> cname;
    ArrayList<String> stname;
    ArrayList<State> sname;
    JSONObject s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_alert_editaddress);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo = toolbar.getChildAt(1);
        countryCode = "IN";
        coname = new ArrayList<>();
        cname = new ArrayList<>();
        stname = new ArrayList<>();
        sname = new ArrayList<>();
       /* final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        progressDialog = new ProgressDialog(CheckoutDeliveryAddress.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        // progressDialog.setCancelable(false);
        progressDialog.show();


        arrayCartList = (ArrayList<Cart>) getIntent().getSerializableExtra("Cart");
        checkoutAddress = (ArrayList<Checkout>) getIntent().getSerializableExtra("Arraylist");
        coname = (ArrayList<Countries>) getIntent().getSerializableExtra("Countries");
        cname = (ArrayList<String>) getIntent().getSerializableExtra("Country");
        stname.add("Select state");
        ArrayList<String> tempstate = (ArrayList<String>) getIntent().getSerializableExtra("State");
        stname.addAll(tempstate);
        sname = (ArrayList<State>) getIntent().getSerializableExtra("States");
        //Log.e("array", String.valueOf(arrayCartList.size()));
        total = getIntent().getExtras().getDouble("Total");
        shippingId = getIntent().getExtras().getString("shippingId");

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        FirstName = (EditText) findViewById(R.id.checkout_address_editfname);
        LastName = (EditText) findViewById(R.id.checkout_address_editlname);
        Addr = (EditText) findViewById(R.id.checkout_address_editaddress);
        Addr1 = (EditText) findViewById(R.id.checkout_address_editaddress1);
        CityName = (EditText) findViewById(R.id.checkout_address_editcity);
        /*StateName = (EditText) findViewById(R.id.checkout_address_editstate);
        CountryName = (EditText) findViewById(R.id.checkout_address_editcountry);*/
        spinnerCountry = (Spinner) findViewById(R.id.checkout_countryname);
        spinnerState = (Spinner) findViewById(R.id.checkout_statename);
        ZipCode = (EditText) findViewById(R.id.checkout_address_editzip);
        Phonenum = (EditText) findViewById(R.id.checkout_address_editphone);
        replace = (Button) findViewById(R.id.checkout_address_replace);
        ok = (ImageButton) findViewById(R.id.checkout_address_ok);
        cancel = (ImageButton) findViewById(R.id.checkout_address_cancel);
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
        FirstName.setText(db.getName());
        //FirstName.setText(Preferences.getString(Preferences.PrefType.First_name, getApplicationContext()));

        LastName.setText(Preferences.getString(Preferences.PrefType.Last_name, getApplicationContext()));
       /* Addr.setText(Preferences.getString(Preferences.PrefType.ADDRESS,getApplicationContext()));
        Addr1.setText(Preferences.getString(Preferences.PrefType.ADDRESS2,getApplicationContext()));
        CityName.setText(Preferences.getString(Preferences.PrefType.CITY,getApplicationContext()));
        ZipCode.setText(Preferences.getString(Preferences.PrefType.ZIP,getApplicationContext()));
        Phonenum.setText(Preferences.getString(Preferences.PrefType.PHONE,getApplicationContext()));*/
        if (checkoutAddress.size() > 0) {
            Addr.setText(checkoutAddress.get(0).getB_address());
            Addr1.setText(checkoutAddress.get(0).getB_address_2());
            CityName.setText(checkoutAddress.get(0).getB_city());
            // spinnerCountry.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        /*StateName.setText(checkoutAddress.get(0).getB_state());
        CountryName.setText(checkoutAddress.get(0).getB_country());*/
            ZipCode.setText(checkoutAddress.get(0).getB_zip());
            Phonenum.setText(checkoutAddress.get(0).getB_phone());
        }

        try {
            if (coname != null) {
                if (cname != null) {
                    final ArrayAdapter<String> a = new ArrayAdapter<String>(CheckoutDeliveryAddress.this, R.layout.spinner_item, cname);
                    a.setDropDownViewResource(R.layout.spinner_item);

                    final int spinnerPosition = a.getPosition("India");

                    // spinnerCountry.setSelection(a.getPosition("India"));
                    spinnerCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (spinnerCountry.getSelectedItem().toString() == null || Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext()) == null) {
                                    spinnerCountry.setSelection(spinnerPosition);
                                }
                                spinnerCountry.setSelection(a.getPosition(Preferences.getString(Preferences.PrefType.COUNTRY, getApplicationContext())));
                                //Log.e("Select", "" + spinnerCountry.getSelectedItem().toString());
                            } catch (Exception e) {

                            }
                        }
                    });
                    spinnerCountry.setAdapter(a);
                    spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            coName = spinnerCountry.getSelectedItem().toString();
                            for (int i = 0; i < coname.size(); ++i) {
                                if (coname.get(i).getCountry() == coName || coname.get(i).getCountry().equals(coName)) {
                                    countryCode = coname.get(i).getCode();
                                    Log.d(TAG, countryCode);
                                }
                            }

                            new GetStates(countryCode).executeOnExecutor(MyApplication.threadPoolExecutor);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    //new GetStates(countryCode).executeOnExecutor(MyApplication.threadPoolExecutor);

                }
            }

        } catch (Exception e) {

        }

        replace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Firstname = FirstName.getText().toString();
                Lastname = LastName.getText().toString();
                Address = Addr.getText().toString();
                Address1 = Addr1.getText().toString();
                City = CityName.getText().toString();
                try {
                    Iterator<String> keys = s.keys();
                    for (int i = 0; i < s.length(); ++i) {
                        String key = keys.next();
                        String value = s.getString(key);
                        if (stName.equals(value) || stName == value) {
                            stateCode = key;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*State = StateName.getText().toString();
                Country = CountryName.getText().toString();*/
                Zip = ZipCode.getText().toString();
                Phone = Phonenum.getText().toString();

                if ((Firstname.equals("")) || (LastName.equals("")) || (Address.equals("")) || (Address1.equals("")) || (City.equals("")) || (stateCode.equals("")) || (countryCode.equals(""))) {
                    Toast.makeText(getApplicationContext(), "All Fields are required !",
                            Toast.LENGTH_LONG).show();

                } else if (Phone.length() <= 9 || (Phone.length() >= 11)) {
                    Toast.makeText(getApplicationContext(), "Please enter a valid mobile number", Toast.LENGTH_LONG).show();
                }else if (Phone.startsWith("0")){
                    Toast.makeText(getApplicationContext(), "Mobile number must not start with 0", Toast.LENGTH_LONG).show();
                }
                else {
                    new GetShippingrate(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    new PutAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    new PutUserFirstname(Firstname, Lastname, uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    //new GetUserAddress(uid).execute();
                    Intent myintent = new Intent(CheckoutDeliveryAddress.this, CheckoutActivity.class);
                    myintent.putExtra("CalledFromEditDelivery", "Edit");
                    myintent.putExtra("shippingId", shippingId);
                    myintent.putExtra("Total", total);
                    myintent.putExtra("Shipping", shipping);
                    myintent.putExtra("Cartlist", arrayCartList);
                    startActivity(myintent);
                    finish();
                }

            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Firstname = FirstName.getText().toString();
                Lastname = LastName.getText().toString();
                Address = Addr.getText().toString();
                Address1 = Addr1.getText().toString();
                City = CityName.getText().toString();
                try {
                    Iterator<String> keys = s.keys();
                    for (int i = 0; i < s.length(); ++i) {
                        String key = keys.next();
                        String value = s.getString(key);
                        if (stName.equals(value) || stName == value) {
                            stateCode = key;

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               /* State = StateName.getText().toString();
                Country = CountryName.getText().toString();*/
                Zip = ZipCode.getText().toString();
                Phone = Phonenum.getText().toString();
                if ((Firstname.equals("")) || (Lastname.equals("")) || (Address.equals("")) || (Address1.equals("")) || (City.equals("")) || (stateCode.equals("")) || (countryCode.equals(""))) {
                    Toast.makeText(getApplicationContext(), "All Fields are required !",
                            Toast.LENGTH_LONG).show();
                } else if (Phone.length() <= 9 || (Phone.length() >= 11)) {
                    Toast.makeText(getApplicationContext(), "Please enter a valid mobile number", Toast.LENGTH_LONG).show();
                }else if (Phone.startsWith("0")){
                    Toast.makeText(getApplicationContext(), "Mobile number must not start with 0", Toast.LENGTH_LONG).show();
                }
                else {
                    new PutAddress(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    new GetShippingrate(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                    Intent intent = new Intent(CheckoutDeliveryAddress.this, CheckoutActivity.class);
                    intent.putExtra("CalledfromDeliveryAddress", "Delivery");
                    intent.putExtra("Firstname", Firstname);
                    intent.putExtra("Lastname", Lastname);
                    intent.putExtra("Address", Address);
                    intent.putExtra("Address1", Address1);
                    intent.putExtra("City", City);
                    intent.putExtra("State", stateCode);
                    intent.putExtra("Country", countryCode);
                    intent.putExtra("Zip", Zip);
                    intent.putExtra("Phone", Phone);
                    intent.putExtra("shippingId", shippingId);
                    // intent.putExtra("CalledFromEditDelivery","Edit");

                    intent.putExtra("Total", total);
                    intent.putExtra("Shipping", shipping);
                    intent.putExtra("Cartlist", arrayCartList);
                    startActivity(intent);
                    finish();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new GetShippingrate(uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                Intent intent = new Intent(CheckoutDeliveryAddress.this, CheckoutActivity.class);

                intent.putExtra("CalledfromCancel", "Cancel");
                intent.putExtra("Total", total);
                intent.putExtra("Shipping", shipping);
                intent.putExtra("shippingId", shippingId);
                intent.putExtra("Cartlist", arrayCartList);
                startActivity(intent);
                finish();

            }
        });
    }

    private class PutUserFirstname extends AsyncTask<String, String, Void> {
        String Firstname, Lastname, User_id;

        public PutUserFirstname(String firstname, String lastname, String user_id) {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.User_id = user_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstname", Firstname);
                jsonObject.put("lastname", Lastname);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "users/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                HttpResponse httpResponse = httpClient.execute(put);
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }


        }
    }

    private class GetShippingrate extends AsyncTask<String, Void, Void> {
        // String shipping=getIntent().getExtras().getString("Prodshippingparams");


        String user_id;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();

        GetShippingrate(String User_id) {
            this.user_id = User_id;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Shippingrate/" + user_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Cart-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Cart-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                if (jsonObject.has("price")) {
                    if (shipping == "false" || shipping == null) {
                        shipping = "0.0";
                    }

                    shipping = jsonObject.getString("price");
                    JSONObject jsonObject1 = jsonObject.optJSONObject("keys");
                    shippingId = jsonObject1.getString("shipping_id");
                }

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }
    }

    private class PutAddress extends AsyncTask<String, Void, Void> {

        String User_id;

        public PutAddress(String user_id) {
            this.User_id = user_id;

        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("b_firstname", Firstname);
                jsonObject.put("b_lastname", Lastname);
                jsonObject.put("b_address", Address);
                jsonObject.put("b_address_2", Address1);
                jsonObject.put("b_city", City);
                jsonObject.put("b_state", stateCode);
                jsonObject.put("b_country", countryCode);
                jsonObject.put("b_zipcode", Zip);
                jsonObject.put("b_phone", Phone);
                jsonObject.put("s_firstname", Firstname);
                jsonObject.put("s_lastname", Lastname);
                jsonObject.put("s_address", Address);
                jsonObject.put("s_address_2", Address1);
                jsonObject.put("s_city", City);
                jsonObject.put("s_state", stateCode);
                jsonObject.put("s_country", countryCode);
                jsonObject.put("s_zipcode", Zip);
                jsonObject.put("s_phone", Phone);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getString(R.string.url) + "address/" + User_id);
                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("Entity", "" + jsonObject.toString());
                Log.e("CheckoutPut-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("CheckoutPut-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {

            }
            //progressDialog.dismiss();
            db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
            String n = db.getName();
            db.deleteName(n);
            mainCardModel = new MainCardModel(Firstname);
            db.addName(mainCardModel);
            //Preferences.add(Preferences.PrefType.First_name, Firstname, getApplicationContext());
            Preferences.add(Preferences.PrefType.Last_name, Lastname, getApplicationContext());
            Preferences.add(Preferences.PrefType.ADDRESS, Address, getApplicationContext());
            Preferences.add(Preferences.PrefType.ADDRESS2, Address1, getApplicationContext());
            Preferences.add(Preferences.PrefType.CITY, City, getApplicationContext());
            Preferences.add(Preferences.PrefType.STATE, stName, getApplicationContext());
            Preferences.add(Preferences.PrefType.COUNTRY, coName, getApplicationContext());
            Preferences.add(Preferences.PrefType.ZIP, Zip, getApplicationContext());
            Preferences.add(Preferences.PrefType.PHONE, Phone, getApplicationContext());


        }
    }


    private class GetStates extends AsyncTask<String, Void, Void> {
        String c;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();

        public GetStates(String countryCode) {
            this.c = countryCode;
            Log.d(TAG, countryCode);
        }


        protected void onPreExecute() {
            super.onPreExecute();

            if(!progressDialog.isShowing()){
                // progressDialog.setCancelable(false);
                progressDialog.show();
            }

        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Cslist?code=" + c);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                Log.e("States-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                httpResponse = httpClient.execute(httpGet);
                Log.e("States-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));

                Log.d("test", jobj.toString());

                stname.removeAll(stname);
                sname.removeAll(sname);

                JSONObject jsonObject = jobj.getJSONObject("states");
                s = jobj.getJSONObject("states");
                if (jsonObject != null) {
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        State st = new State("Code", "name");
                        String key = keys.next();

                        String value = jsonObject.getString(key);
                        st.setStateCode(key);
                        st.setStateName(jsonObject.getString(key));
                        sname.add(st);
                        stname.add(value);

                    }


                } else {
                    sname = null;
                    stname = null;
                }

            } catch (Exception e) {
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                if (stname != null && stname.size() > 0) {
                    final ArrayAdapter<String> a1 = new ArrayAdapter<String>(CheckoutDeliveryAddress.this, R.layout.spinner_item, stname);
                    a1.setDropDownViewResource(R.layout.spinner_item);

                    Log.e("State", "" + Preferences.getString(Preferences.PrefType.STATE, getApplicationContext()));
                    spinnerState.post(new Runnable() {
                        @Override
                        public void run() {
                            if (Preferences.getString(Preferences.PrefType.STATE, getApplicationContext()) != null) {
                                spinnerState.setSelection(a1.getPosition(Preferences.getString(Preferences.PrefType.STATE, getApplicationContext())));
                                Log.e("Selected", "" + a1.getPosition(Preferences.getString(Preferences.PrefType.STATE, getApplicationContext())));

                            }
                        }
                    });
                    spinnerState.setAdapter(a1);

                    spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            stName = spinnerState.getSelectedItem().toString();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    final ArrayAdapter<String> a = new ArrayAdapter<String>(CheckoutDeliveryAddress.this, R.layout.spinner_item, stname);
                    a.setDropDownViewResource(R.layout.spinner_item);

                    spinnerState.setAdapter(a);
                }


            } catch (Exception e) {

            }
        }
    }


}
