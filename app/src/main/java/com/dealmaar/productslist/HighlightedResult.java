package com.dealmaar.productslist;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by omm on 3/17/2016.
 */
public class HighlightedResult<T>
{
    private T result;
    private Map<String, Highlight> highlights = new HashMap<>();

    public HighlightedResult(T result)
    {
        this.result = result;
    }

    public T getResult()
    {
        return result;
    }

    public Highlight getHighlight(String attributeName)
    {
        return highlights.get(attributeName);
    }

    public void addHighlight(String attributeName, Highlight highlight)
    {
        highlights.put(attributeName, highlight);
    }
}
