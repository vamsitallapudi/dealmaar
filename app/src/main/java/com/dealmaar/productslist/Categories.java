package com.dealmaar.productslist;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by OMM on 10/11/2015.
 */
public class Categories implements Parcelable{
    private String category;
    private String category_id;
    private String parent_id;
    private String path_id;
    private String position;
    private String status;
    private String product_count;
    private String seo_name;
    private String seo_path;

    public Categories(String category) {

    }

    public Categories() {

    }

    protected Categories(Parcel in) {
        category = in.readString();
        category_id = in.readString();
        parent_id = in.readString();
        path_id = in.readString();
        position = in.readString();
        status = in.readString();
        product_count = in.readString();
        seo_name = in.readString();
        seo_path = in.readString();
    }

    public static final Creator<Categories> CREATOR = new Creator<Categories>() {
        @Override
        public Categories createFromParcel(Parcel in) {
            return new Categories(in);
        }

        @Override
        public Categories[] newArray(int size) {
            return new Categories[size];
        }
    };

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String setCategory_id(String category_id) {
        this.category_id = category_id;
        return category_id;
    }

    public String getParent_id() {
        return this.parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getPath_id() {
        return path_id;
    }

    public void setPath_id(String path_id) {
        this.path_id = path_id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProduct_count() {
        return product_count;
    }

    public void setProduct_count(String product_count) {
        this.product_count = product_count;
    }

    public String getSeo_name() {
        return seo_name;
    }

    public void setSeo_name(String seo_name) {
        this.seo_name = seo_name;
    }

    public String getSeo_path() {
        return seo_path;
    }

    public void setSeo_path(String seo_path) {
        this.seo_path = seo_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(category_id);
        dest.writeString(parent_id);
        dest.writeString(path_id);
        dest.writeString(position);
        dest.writeString(status);
        dest.writeString(product_count);
        dest.writeString(seo_name);
        dest.writeString(seo_path);
    }
}
