package com.dealmaar.productslist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SettingsActivity;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;

/**
 * Created by omm on 11/12/2015.
 */
public class ShopbyCategoryActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, View.OnClickListener {
    ListView list;
    public String cat_id;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    TextView allcategories;
    public ProgressDialog progressDialog;
    String uid;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    TextView shopby;

    private CategoryResultsJsonParser resultsParser = new CategoryResultsJsonParser();
    private int lastSearchedSeqNo;
    private int lastDisplayedSeqNo;
    private int lastRequestedPage;
    private int lastDisplayedPage;
    private boolean endReached;
    private static final int HITS_PER_PAGE = 40000;
    Categories adapter;
    ArrayList<Category> categoriesList;
    TextView noItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fragment fragment = null;
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);


        toolbar.setLogo(R.drawable.logo2);
        getSupportActionBar().setHomeButtonEnabled(true);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
        shopby = (TextView) findViewById(R.id.shopby_noitems);

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        //displayView(0);


        allcategories = (TextView) findViewById(R.id.productslist_shopbycategories_all);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        allcategories.setTypeface(custom_font);

        categoriesList = new ArrayList<>();
        //cat_id = new ArrayList<Categories>();
        // new GetCategoriesTask().executeOnExecutor(MyApplication.threadPoolExecutor);
        list = (ListView) findViewById(R.id.lv_category_list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ShopbyCategoryActivity.this, SubCatActivity.class);

                intent.putExtra("id2", categoriesList.get(position).getCategory_id());

                startActivity(intent);
                //finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        ShopbyCategoryActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View syncItemView = findViewById(R.id.action_cart);

                    String count = db.getCartCount(uid);
                    //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (count == null || count.equals("null")) {
                        count = "0";
                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {

                        BadgeView badge = new BadgeView(ShopbyCategoryActivity.this, syncItemView);
                        badge.setText(count);
                        badge.show();
                    }

                }
            });
        } catch (Exception e) {

        }
        return true;
    }
    
    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, SettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }


    //List<Map<String,String>> categoriesList = new ArrayList<Map<String,String>>();
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(ShopbyCategoryActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;


            case R.id.action_posts:
                Intent postsintent = new Intent(ShopbyCategoryActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;
            case R.id.action_search:
//                Intent searchintent = new Intent(ShopbyCategoryActivity.this, ProductSearchActivity.class);
//                startActivity(searchintent);

                return true;


            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }

    @Override
    public void onClick(View v) {
    }

}
