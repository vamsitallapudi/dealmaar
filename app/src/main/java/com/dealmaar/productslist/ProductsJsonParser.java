package com.dealmaar.productslist;

import com.dealmaar.models.Product;

import org.json.JSONObject;

/**
 * Created by omm on 4/20/2016.
 */
public class ProductsJsonParser{
    public Product Parse(JSONObject jsonObject) {
        if (jsonObject == null)
            return null;
        String title = jsonObject.optString("product");
        String brand = jsonObject.optString("variant");
        String image = jsonObject.optString("image_path");
        String shippingparams=jsonObject.optString("shipping_params");
        String amount=jsonObject.optString("amount");
        String id = jsonObject.optString("product_id");
        String price = jsonObject.optString("price");
        String category_id=jsonObject.optString("main_category");
        String list_price=jsonObject.optString("list_price");
        String status=jsonObject.optString("status");
        if (title != null && brand != null && id!=null && price != null && category_id!=null && list_price!=null && amount!=null && status!=null) {
            return new Product(id,title,price,brand,image,shippingparams,category_id,list_price,amount,status);
        }
        return null;
    }
}