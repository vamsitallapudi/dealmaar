package com.dealmaar.productslist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.BaseActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProductslistActivity {

//
//    ArrayList<Product> productslists;
//    ArrayList<Product> productslists1;
//    ArrayList<Product> electronicsViewall;
//    ArrayList<Product> category1 = new ArrayList<>();
//    ArrayList<Product> sports = new ArrayList<>();
    String fromFilter;


    JSONObject jsonObject=new JSONObject();
    String variantString,priceString;
    private ProductsResultsJsonParser resultsParser = new ProductsResultsJsonParser();
    private int lastSearchedSeqNo;
    private int lastDisplayedSeqNo;
    private int lastRequestedPage;
    private int lastDisplayedPage;
    private boolean endReached;
    private static final int HITS_PER_PAGE = 5000;
    ListView list, list1;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    Button ShopByCategories, Filter, sort;
    String uid;
    String text;
    TextView noitems;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    String Sub_cat_Id;


    public ProgressDialog progressDialog;


//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.productslist_activity);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
//        setSupportActionBar(toolbar);
//        toolbar.setLogo(R.drawable.logo2);
//        toolbar.setNavigationIcon(R.drawable.menu);
//
//        // getSupportActionBar().setDisplayShowHomeEnabled(true);
//        final Drawable navigationDrawer = getResources().getDrawable(R.drawable.menu);
//        navigationDrawer.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(navigationDrawer);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        View logo = toolbar.getChildAt(1);
//        logo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });
//
//        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
//        list = (ListView) findViewById(R.id.productslist_listView);
//        db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
//        noitems = (TextView) findViewById(R.id.productslist_noitems);
//
//        productslists = new ArrayList<Product>();
//        productslists1 = new ArrayList<Product>();
//        electronicsViewall = new ArrayList<Product>();
//        sports = new ArrayList<Product>();
//        Button shopbycategory = (Button) findViewById(R.id.productslist_shopbycategories_button);
//        shopbycategory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent myintent = new Intent(ProductslistActivity.this, ShopbyCategoryActivity.class);
//                startActivity(myintent);
//            }
//        });
//
//        Button filter = (Button) findViewById(R.id.productslist_filter_button);
//        filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(ProductslistActivity.this, ProductslistFilterActivity.class);
//                intent.putExtra("filters", jsonObject.toString());
//                startActivityForResult(intent, 1);
//                // finish();
//            }
//        });
//
//
//        sort = (Button) findViewById(R.id.productslist_sort_button);
//        sort.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//
//                callSortPopUp();
//                /*Intent myintent=new Intent(ProductslistActivity.this,ProductslistActivity.class);
//                myintent.putExtra("CalledFromSort","Sort");
//               myintent.putExtra("CalledFromCategorySort", "CategorySort");*/
//
//                // startActivity(myintent);
//            }
//        });
//
//        if (getIntent().getExtras() != null) {
//            String calledFromSort = getIntent().getExtras().getString("CalledFromSort");
//            Sub_cat_Id = getIntent().getExtras().getString("id2");
//            String calledFromFilter = getIntent().getExtras().getString("CalledFromFilter");
//            String calledFromSearch = getIntent().getExtras().getString("calledFrom");
//            String calledFromViewall = getIntent().getExtras().getString("CalledFromViewall");
//            String calledFromCategories = getIntent().getExtras().getString("CalledFromCategories");
//            String calledFromCategory1 = getIntent().getExtras().getString("CalledFromCategory1");
//            String calledFromRatings = getIntent().getExtras().getString("Calledfromratings");
//            String calledFromSportsViewall = getIntent().getExtras().getString("CalledFromSportsViewall");
//            String calledFromProductpage = getIntent().getExtras().getString("Calledfromproductpage");
//            //String calledFromCategoryFilter=getIntent().getExtras().getString("CalledFromCategoryFilter");
//            //String calledFromCategorySort=getIntent().getExtras().getString("CalledFromCategorySort");
//            final String text = getIntent().getExtras().getString("SearchResult");
//            Log.e("text", "" + text);
//            if (calledFromProductpage != null) {
//                String id = getIntent().getExtras().getString("Category");
//                new GetCategoryProducts(id).executeOnExecutor(MyApplication.threadPoolExecutor);
//            }
//
//
//            if (calledFromRatings != null) {
//                new GetProductsTask().executeOnExecutor(MyApplication.threadPoolExecutor);
//                callSortPopUp();
//
//            }
//
//            if (Sub_cat_Id != null) {
//                sort.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        callCategorySortPopUp();
//                    }
//                });
//            }
//
//            if (calledFromSearch != null) {
//                Log.e("text", "T" + text.length());
//                if (text.length() == 0 || text.equals("null")) {
//                    //new GetProductsTask().execute();
//                   /* Intent intent = new Intent(ProductslistActivity.this, NoItemsFoundActivity.class);
//                    startActivity(intent);
//                    return;*/
//                    noitems.setVisibility(View.VISIBLE);
//
//                } else {
//
//                    new GetProductFilters(text).executeOnExecutor(MyApplication.threadPoolExecutor);
//                    sort.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//                            View popupView = layoutInflater.inflate(R.layout.productslist_sort, null);
//                            final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
//                            popupWindow.setTouchable(true);
//                            popupWindow.setFocusable(true);
//                            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//                            // RadioButton asc = (RadioButton) findViewById(R.id.productslist_sort_asc);
//                            // RadioButton dsc = (RadioButton) findViewById(R.id.productslist_sort_dsc);
//                            ((RadioButton) popupView.findViewById(R.id.productslist_sort_asc)).setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    popupWindow.dismiss();
//                                    new GetProductFiltersAsc(text).execute();
//
//                                }
//
//
//                            });
//
//                            ((RadioButton) popupView.findViewById(R.id.productslist_sort_dsc)).setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    popupWindow.dismiss();
//                                    new GetProductFiltersDsc(text).execute();
//
//                                }
//                            });
//
//                        }
//                    });
//                }
//
//
//            }
//            if (calledFromViewall != null) {
//                Log.e("Hi", "Entered");
//                new GetElectronicsViewall().executeOnExecutor(MyApplication.threadPoolExecutor);
//                sort.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//                        View popupView = layoutInflater.inflate(R.layout.productslist_sort, null);
//                        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
//                        popupWindow.setTouchable(true);
//                        popupWindow.setFocusable(true);
//                        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//                        // RadioButton asc = (RadioButton) findViewById(R.id.productslist_sort_asc);
//                        // RadioButton dsc = (RadioButton) findViewById(R.id.productslist_sort_dsc);
//                        ((RadioButton) popupView.findViewById(R.id.productslist_sort_asc)).setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                popupWindow.dismiss();
//                                new GetElectronicsViewallAsc().execute();
//
//                            }
//
//
//                        });
//
//                        ((RadioButton) popupView.findViewById(R.id.productslist_sort_dsc)).setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                popupWindow.dismiss();
//                                new GetElectronicsViewallDsc().execute();
//
//                            }
//                        });
//
//                    }
//                });
//
//
//            }
//            if (calledFromCategories != null || calledFromCategory1 != null) {
//                new GetCategory1().executeOnExecutor(MyApplication.threadPoolExecutor);
//
//
//            }
//
//        }
//
//
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawerFragment = (FragmentDrawer)
//                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
//        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
//        drawerFragment.setDrawerListener(this);
//
//
//        ShopByCategories = (Button) findViewById(R.id.productslist_shopbycategories_button);
//        Typeface custom_font_shop = Typeface.createFromAsset(getAssets(), "font/Roboto-Regular.ttf");
//        ShopByCategories.setTypeface(custom_font_shop);
//
//        Filter = (Button) findViewById(R.id.productslist_filter_button);
//        Typeface custom_font_filter = Typeface.createFromAsset(getAssets(), "font/Roboto-Regular.ttf");
//        Filter.setTypeface(custom_font_filter);
//
//        sort = (Button) findViewById(R.id.productslist_sort_button);
//        Typeface custome_font_sort = Typeface.createFromAsset(getAssets(), "font/Roboto-Regular.ttf");
//        sort.setTypeface(custome_font_sort);
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
//            drawerLayout.closeDrawer(Gravity.LEFT);
//        } else {
//            super.onBackPressed();
//        }
//    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (data != null) {
//            if (data.getStringExtra("FromFilter") != null) {
//                fromFilter = data.getStringExtra("FromFilter");
//            }
//        }
//        switch (requestCode) {
//            case 1: {
//                if ((resultCode == 1) && null != data) {
//                    variantString = data.getStringExtra("VariantString");
//                    //Log.e("VariantString",variantString);
//                    priceString = data.getStringExtra("PriceString");
//                    // Log.e("PriceString",priceString);
//                    if (variantString != null || priceString != null) {
//                        productslists.removeAll(productslists);
//                    }
//                    break;
//                }
//            }
//        }
//    }
//    private void callCategorySortPopUp() {
//
//        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//        final View popupView = layoutInflater.inflate(R.layout.productslist_sort, null);
//        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
//        popupWindow.setTouchable(true);
//        popupWindow.setFocusable(true);
//        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        /*popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/
//        popupWindow.setBackgroundDrawable(null);
//        popupWindow.setFocusable(true);
//
//
//
//        // drawerLayout.setAlpha(0.5f);
//        // RadioButton asc = (RadioButton) findViewById(R.id.productslist_sort_asc);
//        // RadioButton dsc = (RadioButton) findViewById(R.id.productslist_sort_dsc);
//
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_asc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                productslists.removeAll(productslists);
//                popupWindow.dismiss();
//            }
//
//        });
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_dsc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                productslists.removeAll(productslists);
//                popupWindow.dismiss();
//            }
//        });
//
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        try {
//            new Handler().post(new Runnable() {
//                @Override
//                public void run() {
//                    View syncItemView = findViewById(R.id.action_cart);
//                    Log.e("View", String.valueOf(syncItemView));
//                    // String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
//                    String count = db.getCartCount(uid);
//                    if (count == null || count.equals("null")) {
//                        count = "0";
//                    } else if (Integer.parseInt(count) == 0) {
//                        count = "0";
//                    } else {
//                        Log.e("C", "Count" + count);
//                        //Log.e("View1", String.valueOf(syncItemView[0]));
//                        BadgeView badge = new BadgeView(ProductslistActivity.this, syncItemView);
//                        badge.setText(count);
//                        badge.show();
//                    }
//
//                }
//            });
//        } catch (Exception e) {
//
//        }
//        return true;
//    }
//
//    @Override
//    public void onResume() {  // After a pause OR at startup
//        super.onResume();
//        ProductslistActivity.this.invalidateOptionsMenu();
//        //Refresh your stuff here
//    }
//
//
//    private class GetCategoryProducts extends AsyncTask<String, Void, Void> {
//
//        private StringBuffer stringBuffer = new StringBuffer();
//        String Cat_Id;
//        JSONArray jArray = new JSONArray();
//
//
//        public GetCategoryProducts(String cat_id) {
//            this.Cat_Id = cat_id;
//        }
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setCancelable(false);
//            progressDialog.setIndeterminate(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpClient httpClient = new DefaultHttpClient();
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "categories/" + Cat_Id + "/products");//refer to all categories. Only GET and POST are supported.
//
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                Log.e("BeforeCategoryproducts", "Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                HttpResponse httpResponse = httpClient.execute(httpGet);
//                Log.e("Aftercategoryproducts", "New Date " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//                        Log.e("", "" + e);
//                    }
//                }
//            }
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                Log.e("buffer", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//
//                for(int i=0;i<jArray.length();++i) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    String p = jobj.getString("product_id");
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setProduct_name(jobj.getString("product"));
//                    pro.setBase_price(jobj.getString("price"));
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//
//
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//                        productslists1.add(pro);
//                    }
//
//                    productslists.add(pro);
//                }
//                        Log.e("P", String.valueOf(productslists.size()));
//
//
//            } catch (Exception e) {
//
//            }
//            return null;
//        }
//        protected void onPostExecute(Void result)
//        {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            ProductslistAdapter adapter=new ProductslistAdapter(ProductslistActivity.this, R.layout.productslist_item,productslists);
//            list.setAdapter(adapter);
//        }
//
//
//    }
//
//
//
//    private class GetElectronicsViewallDsc extends AsyncTask<String, Void, Void> {
//
//        int pagenumber = 2;
//
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "categories/166/products?sort_by=price&sort_order=desc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("ElecProducts-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("ElecProducts-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//            productslists1.removeAll(productslists1);
//            electronicsViewall.removeAll(electronicsViewall);
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//                    //pro.setBase_price(jobj.getString("base_price"));
//                    //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    pro.setBase_price(String.valueOf(price));
//                    try {
//                        if (jobj.getJSONObject("main_pair") != null) {
//                            pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                        }
//                    } catch (Exception e) {
//
//                    }
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        productslists1.add(pro);
//                    }
//
//                }
//
//                Log.e("List", String.valueOf(productslists1));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            //progressDialog.dismiss();
//            ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//            // productsList.clear();
//            // productslists.removeAll(productslists);
//            list.setAdapter(adapter1);
//            //adapter1.notifyDataSetChanged();
//
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            //new LoadMoreElectronicsViewallDsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreElectronicsViewallDsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//               /* progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();*/
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "categories/166/products?sort_by=price&sort_order=desc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//                            /*NumberFormat nf=NumberFormat.getInstance();
//                            nf.setMinimumFractionDigits(2);
//                            nf.setMaximumFractionDigits(2);
//                            String price=nf.format(Double.parseDouble(jobj.getString("price")));*/
//
//                        pro.setBase_price(String.valueOf(price));
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists1.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                int position = list.getLastVisiblePosition();
//                // Pass the results into ListViewAdapter.java
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                // Binds the Adapter to the ListView
//                adapter1.notifyDataSetChanged();
//                // progressDialog.dismiss();
//                list.setAdapter(adapter1);
//                // Show the latest retrived results on the top
//                list.setSelectionFromTop(position, 0);
//                // Close the progressdialog
//            }
//        }
//    }
//
//
//    private class GetElectronicsViewallAsc extends AsyncTask<Void, Void, Void> {
//
//        int pagenumber = 2;
//
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "categories/166/products?sort_by=price&sort_order=asc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("ElecProducts-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("ElecProducts-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//            electronicsViewall.removeAll(electronicsViewall);
//            productslists1.removeAll(productslists1);
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//                    double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    pro.setBase_price(String.valueOf(price));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//                        productslists1.add(pro);
//                    }
//
//                }
//
//                // Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//            progressDialog.dismiss();
//            //electronicsViewall.removeAll(electronicsViewall);
//            list.setAdapter(adapter1);
//            //adapter1.notifyDataSetChanged();
//
//            Log.e("Products1", String.valueOf(productslists1));
//            /*Intent myintent=new Intent(ProductslistActivity.this,ProductslistActivity.class);
//
//            startActivity(myintent);*/
//
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            // new LoadMoreElectronicsViewallAsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreElectronicsViewallAsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//              /*  progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();*/
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "categories/169/products?sort_by=price&sort_order=asc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        pro.setBase_price(String.valueOf(price));
//
//
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists1.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                int position = list.getLastVisiblePosition();
//                // Pass the results into ListViewAdapter.java
//                //progressDialog.dismiss();
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                // Binds the Adapter to the ListView
//                adapter1.notifyDataSetChanged();
//                list.setAdapter(adapter1);
//                // Show the latest retrived results on the top
//                list.setSelectionFromTop(position, 0);
//                // Close the progressdialog
//            }
//        }
//    }
//
//    private class GetProductFilters extends AsyncTask<Void, Void, Void> {
//        int pagenumber = 2;
//
//        private StringBuffer stringBuffer = new StringBuffer();
//        private JSONArray jArray = new JSONArray();
//        BufferedReader bufferedReader = null;
//        String text;
//
//        public GetProductFilters(String Text) {
//            this.text = Text;
//
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y");
//                Log.e("Filter url", String.valueOf(uri));
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("ProductsFilters-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("ProductsFilters-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//                    //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    pro.setBase_price(jobj.getString("price"));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//
//
//                    if ("A".equals(status)) {
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//                        productslists.add(pro);
//
//                    }
//                }
//
//                Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            try {
//                progressDialog.dismiss();
//                if (productslists.size() <= 0) {
//                    noitems.setVisibility(View.VISIBLE);
//                    sort.setEnabled(false);
//                    Filter.setEnabled(false);
//
//                }
//                ProductslistAdapter adapter1 = new ProductslistAdapter(ProductslistActivity.this, R.layout.productslist_item, productslists);
//                // productsList.clear();
//                list.setAdapter(adapter1);
//            } catch (Exception e) {
//                Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//            }
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreDataFilters(text).execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//
//        }
//
//        private class LoadMoreDataFilters extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//            String text;
//
//            public LoadMoreDataFilters(String Text) {
//                this.text = Text;
//            }
//
//            /*@Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();
//            }*/
//
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        pro.setBase_price(String.valueOf(price));
//
//
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                try {
//                    int position = list.getLastVisiblePosition();
//                    // progressDialog.dismiss();
//                    // Pass the results into ListViewAdapter.java
//                    ProductslistAdapter adapter1 = new ProductslistAdapter(ProductslistActivity.this, R.layout.productslist_item, productslists);
//                    // Binds the Adapter to the ListView
//                    adapter1.notifyDataSetChanged();
//                    list.setAdapter(adapter1);
//                    // Show the latest retrived results on the top
//                    list.setSelectionFromTop(position, 0);
//                    // Close the progressdialog
//                } catch (Exception e) {
//                    Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//        }
//    }
//
//    private class GetProductFiltersDsc extends AsyncTask<String, Void, Void> {
//
//        int pagenumber = 2;
//        String text;
//
//        public GetProductFiltersDsc(String Text) {
//            this.text = Text;
//        }
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y&sort_by=price&sort_order=desc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Filterdsc-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Filterdsc-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//            productslists1.removeAll(productslists1);
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//                    //pro.setBase_price(jobj.getString("base_price"));
//                    //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    //pro.setBase_price(String.valueOf(price));
//                    pro.setBase_price(jobj.getString("price"));
//                    try {
//                        if (jobj.getJSONObject("main_pair") != null) {
//                            pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                        }
//                    } catch (Exception e) {
//
//                    }
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        productslists1.add(pro);
//                    }
//
//                }
//
//                Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            //progressDialog.dismiss();
//            try {
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                // productsList.clear();
//                productslists.removeAll(productslists);
//
//                list.setAdapter(adapter1);
//                adapter1.notifyDataSetChanged();
//            } catch (Exception e) {
//                Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//            }
//
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreProductFiltersDsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreProductFiltersDsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//               /* progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();*/
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y&sort_by=price&sort_order=desc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//                            /*NumberFormat nf=NumberFormat.getInstance();
//                            nf.setMinimumFractionDigits(2);
//                            nf.setMaximumFractionDigits(2);
//                            String price=nf.format(Double.parseDouble(jobj.getString("price")));*/
//
//                        pro.setBase_price(String.valueOf(price));
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists1.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                try {
//                    // Locate listview last item
//                    int position = list.getLastVisiblePosition();
//                    // Pass the results into ListViewAdapter.java
//                    ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                    // Binds the Adapter to the ListView
//                    adapter1.notifyDataSetChanged();
//                    // progressDialog.dismiss();
//                    list.setAdapter(adapter1);
//                    // Show the latest retrived results on the top
//                    list.setSelectionFromTop(position, 0);
//                    // Close the progressdialog
//                } catch (Exception e) {
//                    Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//        }
//    }
//
//    private class GetProductFiltersAsc extends AsyncTask<String, Void, Void> {
//
//        int pagenumber = 2;
//        String text;
//
//        public GetProductFiltersAsc(String Text) {
//            this.text = Text;
//        }
//
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y&sort_by=price&sort_order=asc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//            productslists1.removeAll(productslists1);
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//                    pro.setBase_price(jobj.getString("price"));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        productslists1.add(pro);
//                    }
//
//                }
//
//                Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            try {
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                productslists.removeAll(productslists);
//                progressDialog.dismiss();
//                list.setAdapter(adapter1);
//            } catch (Exception e) {
//                Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//            }
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreProductFiltersAsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreProductFiltersAsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//              /*  progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();*/
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?q=" + text + "&pname=Y&sort_by=price&sort_order=asc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        //pro.setBase_price(String.valueOf(price));
//                        pro.setBase_price(jobj.getString("price"));
//
//
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists1.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                try {
//                    // Locate listview last item
//                    int position = list.getLastVisiblePosition();
//                    // Pass the results into ListViewAdapter.java
//                    //progressDialog.dismiss();
//                    ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                    // Binds the Adapter to the ListView
//                    adapter1.notifyDataSetChanged();
//                    list.setAdapter(adapter1);
//                    // Show the latest retrived results on the top
//                    list.setSelectionFromTop(position, 0);
//                    // Close the progressdialog
//                } catch (Exception e) {
//                    Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//        }
//    }
//
//
//    private class GetCategory1 extends AsyncTask<Void, Void, Void> {
//        private StringBuffer stringBuffer = new StringBuffer();
//        private JSONArray jArray = new JSONArray();
//        BufferedReader bufferedReader = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            try {
//                bufferedReader = new BufferedReader(new InputStreamReader(getApplicationContext().getAssets().open("MainScreen/Category1")));
//                Log.e("Hi", String.valueOf(bufferedReader));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    //stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//
//                }
//                Log.e("Viewall", String.valueOf(stringBuffer));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//            try {
//                JSONArray jsonArray = new JSONArray(String.valueOf(stringBuffer));
//                Log.e("j", String.valueOf(jsonArray));
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    JSONObject jobj = jsonArray.getJSONObject(i);
//                    // jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//
//                    pro.setBase_price(jobj.getString("price"));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//                    category1.add(pro);
//                }
//                Log.e("Length1", String.valueOf(category1.size()));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            ProductslistAdapter adapter1 = new ProductslistAdapter(ProductslistActivity.this, R.layout.productslist_item, category1);
//            // productsList.clear();
//            list.setAdapter(adapter1);
//
//
//        }
//    }
//
//    private class GetElectronicsViewall extends AsyncTask<Void, Void, Void> {
//        private StringBuffer stringBuffer = new StringBuffer();
//        private JSONArray jArray = new JSONArray();
//        BufferedReader bufferedReader = null;
//        int pagenumber = 2;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "categories/166/products");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("ElecViewall-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("ElecViewall-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                Log.e("j", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    // jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//
//                    pro.setBase_price(jobj.getString("price"));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//
//                        electronicsViewall.add(pro);
//                    }
//                }
//                Log.e("Length1", String.valueOf(electronicsViewall.size()));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            if (electronicsViewall.size() <= 0) {
//                /*Intent intent = new Intent(ProductslistActivity.this, NoItemsFoundActivity.class);
//                startActivity(intent);
//                return;*/
//                sort.setEnabled(false);
//                Filter.setEnabled(false);
//                noitems.setVisibility(View.VISIBLE);
//            }
//            ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, electronicsViewall);
//            // productsList.clear();
//            list.setAdapter(adapter1);
//
//            //Log.e("Products1", String.valueOf(electronicsViewall));
//           /* list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreElectronicsViewallTask().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//*/
//        }
//
//        private class LoadMoreElectronicsViewallTask extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//               /* progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.show();*/
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "categories/166/products?page=" + pagenumber);//refer to all products. Only GET and POST are supported.
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price= Double.parseDouble(jobj.getString("base_price"));
//
//                        pro.setBase_price(String.valueOf(jobj.getString("price")));
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                int position = list.getLastVisiblePosition();
//                //progressDialog.dismiss();
//                // Pass the results into ListViewAdapter.java
//                ProductslistAdapter adapter = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//                // Binds the Adapter to the ListView
//                adapter.notifyDataSetChanged();
//                list.setAdapter(adapter);
//                // Show the latest retrived results on the top
//                list.setSelectionFromTop(position, 0);
//                // Close the progressdialog
//                adapter.notifyDataSetChanged();
//            }
//        }
//    }
//
//
//
//
//    private void callSortPopUp() {
//
//        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//        View popupView = layoutInflater.inflate(R.layout.productslist_sort, null);
//        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
//        popupWindow.setTouchable(true);
//        popupWindow.setFocusable(true);
//        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        // RadioButton asc = (RadioButton) findViewById(R.id.productslist_sort_asc);
//        // RadioButton dsc = (RadioButton) findViewById(R.id.productslist_sort_dsc);
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_asc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.dismiss();
//                new GetProductsAsc().execute();
//
//
//            }
//
//
//        });
//
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_dsc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.dismiss();
//                new GetProductsDsc().execute();
//
//            }
//        });
//    }
//
//
//    private void SortPopUp() {
//
//        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//        View popupView = layoutInflater.inflate(R.layout.productslist_sort, null);
//        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
//        popupWindow.setTouchable(true);
//        popupWindow.setFocusable(true);
//        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        // RadioButton asc = (RadioButton) findViewById(R.id.productslist_sort_asc);
//        // RadioButton dsc = (RadioButton) findViewById(R.id.productslist_sort_dsc);
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_asc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.dismiss();
//                // new GetCategory1Asc().execute();
//
//
//            }
//
//
//        });
//
//        ((RadioButton) popupView.findViewById(R.id.productslist_sort_dsc)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.dismiss();
//                // new GetCategory1Dsc().execute();
//
//            }
//        });
//    }
//
//    private class GetProductsAsc extends AsyncTask<Void, Void, Void> {
//
//        int pagenumber = 2;
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products?sort_by=price&sort_order=asc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//
//                    double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    pro.setBase_price(String.valueOf(price));
//
//                    pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//
//
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//                        productslists.add(pro);
//                    }
//
//                }
//
//                Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            try {
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//                progressDialog.dismiss();
//                list.setAdapter(adapter1);
//
//                Log.e("Products1", String.valueOf(productslists));
//            } catch (Exception e) {
//                Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//            }
//
//            /*Intent myintent=new Intent(ProductslistActivity.this,ProductslistActivity.class);
//
//            startActivity(myintent);*/
//
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreDataAsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreDataAsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.setCancelable(false);
//                progressDialog.show();
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?sort_by=price&sort_order=asc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//
//                        pro.setBase_price(String.valueOf(price));
//
//
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                try {
//                    int position = list.getLastVisiblePosition();
//                    // Pass the results into ListViewAdapter.java
//                    progressDialog.dismiss();
//                    ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//                    // Binds the Adapter to the ListView
//                    adapter1.notifyDataSetChanged();
//                    list.setAdapter(adapter1);
//                    // Show the latest retrived results on the top
//                    list.setSelectionFromTop(position, 0);
//                    // Close the progressdialog
//                } catch (Exception e) {
//                    Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//        }
//    }
//
//    private class GetProductsDsc extends AsyncTask<Void, Void, Void> {
//
//        int pagenumber = 2;
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products?sort_by=price&sort_order=desc");
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//                    //pro.setBase_price(jobj.getString("base_price"));
//                    //double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    double price = Double.parseDouble(jobj.getString("base_price"));
//
//                    pro.setBase_price(String.valueOf(price));
//                    try {
//                        if (jobj.getJSONObject("main_pair") != null) {
//                            pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                        }
//                    } catch (Exception e) {
//
//                    }
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        productslists1.add(pro);
//                    }
//
//                }
//
//                Log.e("List", String.valueOf(productslists));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            try {
//                //progressDialog.dismiss();
//                ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                // productsList.clear();
//                list.setAdapter(adapter1);
//            } catch (Exception e) {
//                Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//            }
//
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreDataDsc().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//            progressDialog.dismiss();
//        }
//
//        private class LoadMoreDataDsc extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.setCancelable(false);
//                progressDialog.show();
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?sort_by=price&sort_order=desc&page=" + pagenumber);
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        double price = Double.parseDouble(jobj.getString("base_price"));
//                            /*NumberFormat nf=NumberFormat.getInstance();
//                            nf.setMinimumFractionDigits(2);
//                            nf.setMaximumFractionDigits(2);
//                            String price=nf.format(Double.parseDouble(jobj.getString("price")));*/
//
//                        pro.setBase_price(String.valueOf(price));
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists1.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                try {
//                    int position = list.getLastVisiblePosition();
//                    // Pass the results into ListViewAdapter.java
//                    ProductslistAdapter adapter1 = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists1);
//                    // Binds the Adapter to the ListView
//                    adapter1.notifyDataSetChanged();
//                    progressDialog.dismiss();
//                    list.setAdapter(adapter1);
//                    // Show the latest retrived results on the top
//                    list.setSelectionFromTop(position, 0);
//                    // Close the progressdialog
//                } catch (Exception e) {
//                    Toast.makeText(ProductslistActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//        }
//    }
//
//
//    private void displayView(int position) {
//        Fragment fragment = null;
//        //String title = getString(R.string.app_name);
//        switch (position) {
//            case 0:
//                mIntent = new Intent(this, HomeActivity.class);
//                startActivity(mIntent);
//                //finish();
//                break;
//            case 1:
//                mIntent = new Intent(this, OrdersListActivity.class);
//                startActivity(mIntent);
//                //finish();
//
//                break;
//            case 2:
//                mIntent = new Intent(this, WishlistActivity.class);
//                startActivity(mIntent);
//                //finish();
//
//                break;
//            case 3:
//                mIntent = new Intent(this, NotificationSettingsActivity.class);
//                startActivity(mIntent);
//                //finish();
//                break;
//
//            /*case 4:
//                mIntent = new Intent(this, FeedbackActivity.class);
//                startActivity(mIntent);
//                //finish();
//                break;*/
//            case 4:
//                String url = "market://details?id=com.dealmaar.customer";
//
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
//                // finish();
//                break;
//            case 5:
//                mIntent = new Intent(this, SupportActivity.class);
//                startActivity(mIntent);
//                //finish();
//                break;
//            case 6:
//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
//                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Dealmaar");
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));
//                break;
//            case 7:
//                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
//                if (uid == null) {
//                    mIntent = new Intent(this, LoginActivity.class);
//                    startActivity(mIntent);
//                    finish();
//                } else {
//
//                    String user = null;
//                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
//                    mIntent = new Intent(this, LaunchActivity.class);
//                    Batch.User.getEditor().setIdentifier(null). setAttribute("user_id","null").save();
//                    Batch.onStop(this);
//                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
//                    String s = db.getName();
//                    db.deleteName(s);
//                    Log.e("N", "Name" + db.getName());
//                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
//                    startActivity(mIntent);
//                    finish();
//
//                }
//            default:
//                break;
//        }
//        if (fragment != null) {
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.container_body, fragment);
//            fragmentTransaction.commit();
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        //noinspection SimplifiableIfStatement
//        switch (id) {
//
//            case R.id.action_cart:
//                Intent cartintent = new Intent(ProductslistActivity.this, CartActivity.class);
//                startActivity(cartintent);
//
//                return true;
//
//            case R.id.action_posts:
//                Intent postsintent = new Intent(ProductslistActivity.this, UserNotificationActivity.class);
//                startActivity(postsintent);
//
//                return true;
//            case R.id.action_search:
////                Intent searchintent = new Intent(ProductslistActivity.this, SearchMainActivity.class);
////                startActivity(searchintent);
//
//                return true;
//
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    @Override
//    public void onDrawerItemSelected(View view, int position) {
//        displayView(position);
//    }
//
//    private class GetCategoriesProducts extends AsyncTask<String, Void, Void> {
//        //String Sub_cat_Id = getIntent().getExtras().getString("id2");
//        String Cat_id;
//
//        public GetCategoriesProducts(String sub_cat_id) {
//            this.Cat_id = sub_cat_id;
//
//
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "categories/" + Cat_id + "/products");//refer to all products. Only GET and POST are supported.
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Product-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                // Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    pro.setProduct_name(jobj.getString("product"));
//                    // pro.setBase_price(jobj.getString("base_price"));
//                    double price = Double.parseDouble(jobj.getString("base_price"));
//                    /*NumberFormat nf=NumberFormat.getInstance();
//                    nf.setMinimumFractionDigits(2);
//                    nf.setMaximumFractionDigits(2);
//                    String price=nf.format(Double.parseDouble(jobj.getString("price")));*/
//
//                    pro.setBase_price(String.valueOf(price));
//                    try {
//                        if (jobj.getJSONObject("main_pair") != null) {
//                            pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                        }
//                    } catch (Exception e) {
//
//                    }
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        productslists.add(pro);
//                    }
//
//                }
//                //  Log.e("List", String.valueOf(productsList));
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//
//
//        }
//
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            if (productslists.size() <= 0) {
//
//                noitems.setVisibility(View.VISIBLE);
//                sort.setEnabled(false);
//                Filter.setEnabled(false);
//
//            } else {
//                ProductslistAdapter adapter = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//                list.setAdapter(adapter);
//            }
//
//
//        }
//    }
//
//    private class GetProductsTask extends AsyncTask<Void, Void, Void> {
//
//        int pagenumber = 2;
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductslistActivity.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            StringBuffer stringBuffer = new StringBuffer();
//            JSONArray jArray = new JSONArray();
//            //StringBuffer stringBuffer = new StringBuffer("");
//            BufferedReader bufferedReader = null;
//            try {
//
//                HttpParams httpParams = new BasicHttpParams();
//                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                HttpClient httpClient = new DefaultHttpClient(httpParams);
//                HttpGet httpGet = new HttpGet();
//                URI uri = new URI(getString(R.string.url) + "products");//refer to all products. Only GET and POST are supported.
//                httpGet.setURI(uri);
//                httpGet.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                HttpResponse httpResponse;
//                Log.e("Product-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                httpResponse = httpClient.execute(httpGet);
//                Log.e("Product-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//
//                    }
//                }
//            }
//
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                // Log.e("Objects", String.valueOf(jobj));
//                jArray = jobj.optJSONArray("products");
//                Log.e("Array", String.valueOf(jArray));
//                for (int i = 0; i < jArray.length(); i++) {
//                    Product pro = new Product("product", "my_product");
//                    jobj = jArray.getJSONObject(i);
//                    Log.e("JArray", String.valueOf(jobj));
//                    pro.setProduct_name(jobj.getString("product"));
//                    pro.setBase_price(jobj.getString("price"));
//                    //double price= Double.parseDouble(jobj.getString("base_price"));
//                    /*NumberFormat nf=NumberFormat.getInstance();
//                    nf.setMinimumFractionDigits(2);
//                    nf.setMaximumFractionDigits(2);
//                    String price=nf.format(Double.parseDouble(jobj.getString("base_price")));
//
//                    pro.setBase_price(String.valueOf(price));*/
//                    try {
//                        if (jobj.getJSONObject("main_pair") != null) {
//                            pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                        }
//                    } catch (Exception e) {
//
//                    }
//                    pro.setProduct_id(jobj.getString("product_id"));
//                    pro.setList_price(jobj.getString("list_price"));
//                    pro.setShipping_params(jobj.getString("shipping_params"));
//                    pro.setAmount(jobj.getString("amount"));
//
//
//                    String status = jobj.getString("status");
//                    if ("A".equals(status)) {
//                        // pro.setProduct_companyname(jobj.getString(jobj.getJSONObject("product_features").getJSONObject("18").getString("variant")));
//                        productslists.add(pro);
//                    }
//
//                }
//
//            } catch (JSONException e) {
//                Log.e("log_tag", "Error parsing data " + e.toString());
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            ProductslistAdapter adapter = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//            adapter.notifyDataSetChanged();
//            list.setAdapter(adapter);
//
//            list.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                @Override
//                public void onScrollStateChanged(AbsListView view,
//                                                 int scrollState) { // TODO Auto-generated method stub
//                    int threshold = 1;
//                    int count = list.getCount();
//                    // Log.e("count", String.valueOf(count));
//
//                    if (scrollState == SCROLL_STATE_IDLE) {
//                        if (list.getLastVisiblePosition() >= count
//                                - threshold) {
//                            // Execute LoadMoreDataTask AsyncTask
//                            new LoadMoreDataTask().execute();
//
//                        }
//                    }
//                }
//
//                @Override
//                public void onScroll(AbsListView view, int firstVisibleItem,
//                                     int visibleItemCount, int totalItemCount) {
//                    // TODO Auto-generated method stub
//                }
//
//            });
//
//        }
//
//        private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {
//
//            private StringBuffer stringBuffer = new StringBuffer();
//            private JSONArray jArray = new JSONArray();
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                progressDialog = new ProgressDialog(ProductslistActivity.this);
//                progressDialog.setMessage("Loading...");
//                progressDialog.setIndeterminate(false);
//                progressDialog.setCancelable(false);
//                progressDialog.show();
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                BufferedReader bufferedReader = null;
//                try {
//
//
//                    HttpParams httpParams = new BasicHttpParams();
//                    HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
//
//
//                    HttpClient httpClient = new DefaultHttpClient(httpParams);
//                    HttpGet httpGet = new HttpGet();
//
//                    URI uri = new URI(getString(R.string.url) + "products?page=" + pagenumber);//refer to all products. Only GET and POST are supported.
//
//                    httpGet.setURI(uri);
//
//                    httpGet.addHeader(BasicScheme.authenticate(
//                            new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
//                            HTTP.UTF_8, false));
//                    HttpResponse httpResponse;
//                    Log.e("ProductsList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    httpResponse = httpClient.execute(httpGet);
//                    Log.e("ProductsList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                    InputStream inputStream = httpResponse.getEntity().getContent();
//                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String readLine = bufferedReader.readLine();
//                    while (readLine != null) {
//                        stringBuffer.append(readLine);
//                        stringBuffer.append("\n");
//                        readLine = bufferedReader.readLine();
//                        //Log.e("Test", String.valueOf(stringBuffer));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (bufferedReader != null) {
//                        try {
//                            bufferedReader.close();
//                        } catch (IOException e) {
//
//                        }
//                    }
//                }
//
//                try {
//                    JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                    jArray = jobj.optJSONArray("products");
//                    //  Log.e("Array", String.valueOf(jArray));
//                    if (jArray.length() > 0) {
//                        pagenumber++;
//                    }
//
//                    for (int i = 0; i < jArray.length(); i++) {
//                        Product pro = new Product("product", "my_product");
//                        jobj = jArray.getJSONObject(i);
//                        pro.setProduct_name(jobj.getString("product"));
//                        //double price= Double.parseDouble(jobj.getString("base_price"));
//
//                        pro.setBase_price(String.valueOf(jobj.getString("price")));
//                        try {
//                            if (jobj.getJSONObject("main_pair") != null) {
//
//                                pro.setProduct_image(jobj.getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
//                            }
//                        } catch (Exception e) {
//
//                        }
//                        pro.setProduct_id(jobj.getString("product_id"));
//                        pro.setList_price(jobj.getString("list_price"));
//                        pro.setShipping_params(jobj.getString("shipping_params"));
//                        pro.setAmount(jobj.getString("amount"));
//                        String status = jobj.getString("status");
//                        if ("A".equals(status)) {
//                            productslists.add(pro);
//                        }
//                    }
//                    // Log.e("List", String.valueOf(productsList));
//                } catch (JSONException e) {
//                    Log.e("log_tag", "Error parsing data " + e.toString());
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Void result) {
//                // Locate listview last item
//                int position = list.getLastVisiblePosition();
//                progressDialog.dismiss();
//                // Pass the results into ListViewAdapter.java
//                ProductslistAdapter adapter = new ProductslistAdapter(getApplicationContext(), R.layout.productslist_item, productslists);
//                // Binds the Adapter to the ListView
//                adapter.notifyDataSetChanged();
//                list.setAdapter(adapter);
//                // Show the latest retrived results on the top
//                list.setSelectionFromTop(position, 0);
//                // Close the progressdialog
//                adapter.notifyDataSetChanged();
//            }
//        }
//    }
}