package com.dealmaar.productslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by omm on 12/21/2015.
 */
public class ProductslistFilterActivity extends AppCompatActivity {

    CheckBox brand2, brand3, brand4, brand5, price2, price3, price4, price5, brand, price;
    CheckBox[] brand1, price1;
    String filters;
    JSONObject jsonObject = new JSONObject();
    JSONObject jsonObject1 = new JSONObject();
    JSONObject jsonObject2 = new JSONObject();
    LinearLayout brandlist, pricelist;
    ArrayList brands = new ArrayList();
    ArrayList prices = new ArrayList();
    ArrayList selected = new ArrayList();
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productslist_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo = toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        brand1 = new CheckBox[20];
        price1 = new CheckBox[20];
        brandlist = (LinearLayout) findViewById(R.id.ll_brands_list);
        pricelist = (LinearLayout) findViewById(R.id.ll_price_list);
        submit = (Button) findViewById(R.id.b_apply);


        filters = getIntent().getStringExtra("filters");
        Log.e("filters", "" + filters);
        try {
            JSONObject jsonObject = new JSONObject(filters);
            Log.e("JsonObj", String.valueOf(jsonObject));
            jsonObject1 = jsonObject.getJSONObject("price");
            jsonObject2 = jsonObject.getJSONObject("variant");
            // Log.e("1", String.valueOf(jsonObject1));
            // Log.e("2", String.valueOf(jsonObject2));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        brand = (CheckBox) findViewById(R.id.cb_brand);
        price = (CheckBox) findViewById(R.id.cb_price);

        final Iterator keys = jsonObject2.keys();
        final Iterator keys1 = jsonObject1.keys();

        brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (brand.isChecked()) {
                    brandlist.setVisibility(View.VISIBLE);


                    while (keys.hasNext()) {

                        String key = String.valueOf(keys.next());
                        Log.e("Key", "keys" + key);
                        if (key == "0" || key.equals("0")) {
                            Log.e("Key", "keys" + key);
                        } else {

                            brands.add(key);
                        }
                    }


                    Log.e("brand", String.valueOf(brands));

                    for (int i = 0; i < brands.size(); ++i) {
                        brand1[i].setVisibility(View.VISIBLE);
                        brand1[i].setText(brands.get(i).toString());

                    }

                } else {
                    brandlist.setVisibility(View.GONE);
                    for (int i = 0; i < brands.size(); ++i) {
                        brand1[i].setVisibility(View.VISIBLE);
                        brand1[i].setChecked(false);
                    }
                }


            }
        });
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (price.isChecked()) {
                    pricelist.setVisibility(View.VISIBLE);
                    while (keys1.hasNext()) {
                        String key1 = String.valueOf(keys1.next());
                        Log.e("Key", "keys" + key1);

                        prices.add(key1);
                    }
                    for (int i = 0; i < prices.size(); ++i) {
                        price1[i].setVisibility(View.VISIBLE);
                        price1[i].setText(prices.get(i).toString());

                    }
                } else {
                    pricelist.setVisibility(View.GONE);
                    for (int i = 0; i < prices.size(); ++i) {
                        price1[i].setVisibility(View.VISIBLE);
                        price1[i].setChecked(false);
                    }

                }
            }
        });

        // Log.e("Array",selected.toString());
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String variantStr = null;
                String priceStr = null;
                for (int i = 0; i < 10; ++i) {
                    if (brand1[i].isChecked()) {
                        if (variantStr == null) {
                            variantStr = "variant:" + brand1[i].getText().toString();
                        } else {
                            variantStr += ",variant:" + brand1[i].getText().toString();
                        }
                    }
                    if (price1[i].isChecked()) {
                        if (priceStr == null) {
                            priceStr = "price:" + price1[i].getText().toString();
                        } else {
                            priceStr += ",price:" + price1[i].getText().toString();
                        }
                    }
                }

//                Intent intent = new Intent(ProductslistFilterActivity.this, ProductSearchActivity.class);
//                intent.putExtra("VariantString", variantStr);
//                intent.putExtra("PriceString", priceStr);
//                intent.putExtra("FromFilter","Filter activity");
//                startActivity(intent);
//                setResult(1, intent);
                finish();

            }
        });

       /* final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.drawable.menu);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
