package com.dealmaar.productslist;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dealmaar.customer.R;

import java.util.ArrayList;

/**
 * Created by omm on 11/12/2015.
 */
public class ShopbyCategoryAdapter extends ArrayAdapter<Category> {
    ArrayList<Category> ArrayListCategories;
    int Resource;
    Context context;
    LayoutInflater vi;

    public ShopbyCategoryAdapter(Context context, int resource, ArrayList<Category> objects) {
        super(context, resource, objects);

        ArrayListCategories = objects;
        Resource = resource;
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.productslist_shopbygategory_list_items, parent, false);
            holder = new ViewHolder();
            holder.category = (TextView) convertView.findViewById(R.id.productslistshopbycategoryname);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.category.setText(ArrayListCategories.get(position).getCategory());
        TextView electronics = (TextView) convertView.findViewById(R.id.productslistshopbycategoryname);
        Typeface tf = Typeface.createFromAsset(convertView.getContext().getAssets(), "font/Roboto-Regular.ttf");
        electronics.setTypeface(tf);


        return convertView;
    }

    static class ViewHolder {
        public TextView category;

    }
}

