package com.dealmaar.productslist;

/**
 * Created by omm on 4/18/2016.
 */
public class Category {
    String category_id;
    String parent_id;
    String id_path;
    String category;
    String status,flag;

    public Category(String parentid, String category, String categoryid, String status) {
        this.parent_id=parentid;
        this.category=category;
        this.category_id=categoryid;
        this.status=status;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getId_path() {
        return id_path;
    }
    public void setId_path(String id_path)
    {
        this.id_path=id_path;
    }
    public String getCategory()
    {
        return category;
    }
    public void setCategory(String category)
    {
        this.category=category;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status=status;
    }
    public String getFlag()
    {
        return flag;
    }
    public void setFlag(String flag)
    {
        this.flag=flag;
    }
}
