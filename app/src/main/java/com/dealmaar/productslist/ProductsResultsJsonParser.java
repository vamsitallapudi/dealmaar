package com.dealmaar.productslist;

import com.dealmaar.models.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omm on 4/20/2016.
 */
public class ProductsResultsJsonParser {
    private ProductsJsonParser productsResultsJsonParser=new ProductsJsonParser();

    public List<HighlightedResult<Product>> parseResults(JSONObject jsonObject)
    {
        if (jsonObject == null)
            return null;

        List<HighlightedResult<Product>> results = new ArrayList<>();
        JSONArray hits = jsonObject.optJSONArray("hits");
        if (hits == null)
            return null;

        for (int i = 0; i < hits.length(); ++i) {
            JSONObject hit = hits.optJSONObject(i);
            if (hit == null)
                continue;

            Product productslist = productsResultsJsonParser.Parse(hit);
            if (productslist == null)
                continue;

            JSONObject highlightResult = hit.optJSONObject("_highlightResult");
            if (highlightResult == null)
                continue;
            JSONObject highlightTitle = highlightResult.optJSONObject("product");
            if (highlightTitle == null)
                continue;

            String value = highlightTitle.optString("value");
            if (value == null)
                continue;
            HighlightedResult<Product> result = new HighlightedResult<>(productslist);
            result.addHighlight("product", new Highlight("product", value));
            results.add(result);
        }
        return results;
    }
}

