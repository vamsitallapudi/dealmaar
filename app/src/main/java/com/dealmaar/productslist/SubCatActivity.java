package com.dealmaar.productslist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by OMM on 11/10/2015.
 */
public class SubCatActivity extends AppCompatActivity {

    private static final String MY_REQUEST_ID = "id";
    ListView list;
    Categories adapter;
    ArrayList<Category> categoriesList;
    ArrayList<Category> categories;
    public String id1, cat_id, finalProducts;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    public ProgressDialog progressDialog;
    public List<HighlightedResult<Category>> jsonList = new ArrayList<>();
    List<com.dealmaar.productslist.HighlightedResult<Category>> results;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    String uid;
    TextView noItems;
    private CategoryResultsJsonParser resultsParser = new CategoryResultsJsonParser();
    private static final int HITS_PER_PAGE = 2000;
    private int lastSearchedSeqNo;
    private int lastDisplayedSeqNo;
    private int lastRequestedPage;
    private int lastDisplayedPage;
    private boolean endReached;
    String catId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productslist_shopbycategory_subcategories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo=toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        noItems=(TextView)findViewById(R.id.shopbysub_noitems);

        uid= Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        db=((MyApplication)getApplicationContext()).getDatabaseHandler();
        final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbar.setNavigationIcon(R.drawable.menu);
        getSupportActionBar().setHomeButtonEnabled(true);
       // new GetCategoriesTask().executeOnExecutor(MyApplication.threadPoolExecutor);
        list = (ListView) findViewById(R.id.listView);
        //  Log.e("Jsonlist", String.valueOf(jsonList));




        //startActivityForResult(MY_REQUEST_ID);



        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                // Log.e("Json", String.valueOf(jsonList));

                catId = categoriesList.get(position).getCategory_id();



            }

        });


        id1 = getIntent().getExtras().getString("id2");

        //finalProducts = getIntent().getExtras().getString("finalProducts");


        // Log.i("id1", id1);
        categoriesList = new ArrayList<Category>();
        //ArrayList<Categories> id1 = (ArrayList<Categories>) getIntent().getSerializableExtra("id2");

        // Log.e("id1", String.valueOf(id1));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review, menu);
        try{
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View syncItemView = findViewById(R.id.action_cart);
                Log.e("View", String.valueOf(syncItemView));
                //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                String count=db.getCartCount(uid);
                if (count==null ||count.equals("null")) {
                    count = "0";
                }
                else if (Integer.parseInt(count)==0) {
                    count = "0";
                }
                else {
                    Log.e("C", "Count" + count);
                    //Log.e("View1", String.valueOf(syncItemView[0]));
                    BadgeView badge = new BadgeView(SubCatActivity.this, syncItemView);
                    badge.setText(count);
                    badge.show();
                }

            }
        });
    }catch (Exception e){

    }
        return true;
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        SubCatActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }


    //List<Map<String,String>> categoriesList = new ArrayList<Map<String,String>>();
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(SubCatActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(SubCatActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;
            case R.id.action_search:
//                Intent searchintent = new Intent(SubCatActivity.this, ProductSearchActivity.class);
//                startActivity(searchintent);

                return true;


            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class GetCategoriesTask extends AsyncTask<Void, Void, List<JSONObject>> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONArray jArray = new JSONArray();
        public String id;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubCatActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            //progressDialog.show(ShopbyCategoryActivity.this, "", "Loading....", true);
            progressDialog.show();
        }

        @Override
        protected List<JSONObject> doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {
                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);


                HttpClient httpClient = new DefaultHttpClient(httpParams);


                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "categories");//refer to all categories. Only GET and POST are supported.
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("SubByCategories-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("SubByCategories-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //  Log.e("Test", String.valueOf(stringBuffer));
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            return  null;
        }

          /*  try {
                // jArray = new JSONArray(String.valueOf(stringBuffer));
                JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                Log.e("Objects", String.valueOf(jsonObject));
                jArray = jsonObject.optJSONArray("categories");
                Log.e("Array", String.valueOf(jArray));

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

            //List<JSONObject> jsonList = new ArrayList<JSONObject>();
            for (int i = 0; i < jArray.length(); i++)

            {
                try {
                    jsonList.add(jArray.getJSONObject(i));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Collections.sort(jsonList, new Comparator<JSONObject>() {

                    public int compare(JSONObject a, JSONObject b) {
                        String valA = new String();
                        String valB = new String();

                        try {
                            valA = (String) a.get("id_path");
                            valB = (String) b.get("id_path");
                        } catch (JSONException e) {
                            //do something
                        }

                        return valA.compareTo(valB);
                    }
                });
            }
            return jsonList;
        }*/


        protected void onPostExecute(List<JSONObject> result) {
            super.onPostExecute(result);
            // getChildren(result, id1);



        }
    }


}
