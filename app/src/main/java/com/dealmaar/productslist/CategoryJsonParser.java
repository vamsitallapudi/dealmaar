package com.dealmaar.productslist;

import org.json.JSONObject;

/**
 * Created by omm on 4/18/2016.
 */
public class CategoryJsonParser {
    public Category parse(JSONObject jsonObject) {
        if (jsonObject == null)
            return null;

        String parentid = jsonObject.optString("parent_id");
        String category=jsonObject.optString("category");
        String categoryid=jsonObject.optString("category_id");
        String status=jsonObject.optString("status");
        if (parentid != null && category != null && categoryid != null && status!=null) {
            return new Category(parentid,category,categoryid,status);
        }
        return null;
    }
}
