package com.dealmaar.productslist;

/**
 * Created by omm on 4/20/2016.
 */
public class Productlist {
    private String name;
    private String id;
    private String price;
    private String brand;
    private String image;
    private String shipping_params;
    private String category_id;

    public Productlist(String id,String name,String price,String brand,String image,String shipping_params,String category_id)
    {
        this.id=id;
        this.name=name;
        this.brand=brand;
        this.price=price;
        this.image=image;
        this.shipping_params=shipping_params;
        this.category_id=category_id;

    }
    public String getImage()
    {
        return image;
    }
    public void setImage(String image)
    {
        this.image=image;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public String getBrand()
    {
        return brand;
    }
    public void setBrand(String brand)
    {
        this.brand=brand;
    }

    public String getId()
    {
        return  id;
    }
    public void setId(String id)
    {
        this.id=id;
    }
    public String getShipping_params()
    {
        return shipping_params;
    }
    public void setShipping_params(String shipping_params)
    {
        this.shipping_params=shipping_params;
    }
    public String getBasePrice()
    {
        return price;
    }
    public void setBasePrice(String price)
    {
        this.price=price;
    }
    public String getCategory_id()
    {
        return category_id;
    }
    public void setCategory_id(String category_id)
    {
        this.category_id=category_id;
    }

}