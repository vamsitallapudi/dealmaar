package com.dealmaar.productslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.customer.R;
import com.dealmaar.models.Product;

import java.util.ArrayList;

/**
 * Created by omm on 11/6/2015.
 */
public class ProductslistAdapter extends ArrayAdapter<Product> {

    ArrayList<Product> productsList;

    int Resource;
    Context context;
    LayoutInflater vi;
    DatabaseHandler db;
    MainCardModel mainCardModel;



    /*public ProductslistAdapter(Context context, int resource, ArrayList<Product> productsList) {
        super(context, resource, productsList);
        this.productsList = productsList;
        Resource = resource;
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }*/

    public ProductslistAdapter(Context context, int productslist_item, ArrayList<Product> productsList) {
        super(context,productslist_item,productslist_item);
    }


    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.productslist_item, parent, false);

            holder = new ViewHolder();
            holder.product_name = (TextView) convertView.findViewById(R.id.product_name);
            holder.product_price = (TextView) convertView.findViewById(R.id.tv_price);
            holder.product_image = (ImageView) convertView.findViewById(R.id.productslist_image);
            holder.product_image.setImageResource(R.drawable.ic_launcher);
            holder.wishlist = (ImageButton) convertView.findViewById(R.id.iv_wish_list);
            holder.cart = (ImageButton) convertView.findViewById(R.id.iv_cart);

            // new DownloadImageTask(holder.product_image).execute(productsList.get(position).getProduct_image());

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.product_name.setText(productsList.get(position).getProduct_name());
        //holder.product_companyname.setText(productsList.get(position).getProduct_companyname());
        //holder.product_price.setText(productsList.get(position).getBase_price());
//        holder.productid = productsList.get(position).getProduct_id();
//        holder.product_newprice1 = productsList.get(position).getBase_price();
//        holder.uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
//
//        NumberFormat nf = NumberFormat.getInstance();
//        nf.setMinimumFractionDigits(2);
//        nf.setMaximumFractionDigits(2);
////        holder.product_newprice = nf.format(Double.parseDouble(String.valueOf(productsList.get(position).getBase_price())));
//        holder.product_price.setText(holder.product_newprice);
//        holder.product_shippingparams = productsList.get(position).getShipping_params();
////        holder.amount = productsList.get(position).getAmount();
//        Log.e("id", holder.productid);
//        Log.e("newprice", holder.product_newprice);
//        Log.e("shipping", productsList.get(position).getShipping_params());
////
////        try {
//
//
//            String image = productsList.get(position).getProduct_image();
//            // Log.e("Image",productsList.get(position).getProduct_image());
//
//            if (
//                    (image.equalsIgnoreCase("MainScreen/images/category1/600x600_NP300E5A-A01_main.jpg"))
//                            || (image.equalsIgnoreCase("MainScreen/images/category1/700Z-side_back_open-01.jpg"))
//                            || (image.equalsIgnoreCase("MainScreen/images/category1/9-Series-Laptop_04_4.jpg"))) {
//
//                image="file:///android_asset/"+image;
//                Context mcontext = holder.product_image.getContext();
//                Picasso.with(mcontext)
//                        .load(image).error(R.drawable.ic_launcher).into(holder.product_image);
//            } else {
//
//                Uri uri = Uri.parse(image);
//
//                Context mcontext = holder.product_image.getContext();
//                Picasso.with(mcontext)
//                        .load(uri).error(R.drawable.ic_launcher).into(holder.product_image);
//            }
//        } catch (Exception e) {
//
//        }
//
//        holder.wishlist.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e("Clicked", "Wishlist");
//
//                new PostWishlistProduct(holder.productid, holder.product_newprice1, holder.product_shippingparams).executeOnExecutor(MyApplication.threadPoolExecutor);
//
//
//            }
//        });
//
//        holder.cart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e("Amount", holder.amount);
//                final String uid=Preferences.getString(Preferences.PrefType.User_id,getContext());
//                if (Integer.parseInt(holder.amount) > 0) {
//                    new PostCart(holder.productid, holder.product_newprice1).executeOnExecutor(MyApplication.threadPoolExecutor);
//                    db=((MyApplication)getContext().getApplicationContext()).getDatabaseHandler();
//                    try{
//                    //String c = Preferences.getString(Preferences.PrefType.CART_count, getContext());
//                        String c=db.getCartCount(uid);
//                    if(c==null)
//                    {
//                        c="0";
//                    }
//                    else if (Integer.parseInt(c)==0) {
//                        c = "0";
//                    }
//                    String count= String.valueOf(Integer.parseInt(c)+1);
//                        db.deleteCartCount(uid);
//                        mainCardModel=new MainCardModel(uid,count);
//                        db.addCartCount(mainCardModel);
//                    //Preferences.add(Preferences.PrefType.CART_count,c,getContext());
//                    new Handler().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            View syncItemView = parent.getRootView().findViewById(R.id.action_cart);
//                            Log.e("View1", String.valueOf(syncItemView));
//                           //String count=Preferences.getString(Preferences.PrefType.CART_count,getContext());
//                            String count=db.getCartCount(uid);
//                            if (count==null || count.equals("null")) {
//                                count = "0";
//                            }
//                            else if (Integer.parseInt(count)==0) {
//                                count = "0";
//                            }
//                            else {
//                                Log.e("C", "Count" + count);
//
//                                //Log.e("View1", String.valueOf(syncItemView[0]));
//                                BadgeView badge = new BadgeView(getContext(), syncItemView);
//                                badge.setText(count);
//                                badge.show();
//                                ((ProductSearchActivity) getContext()).invalidateOptionsMenu();
//                            }
//
//
//                        }
//                    });
//                    }catch (Exception e){
//
//                    }
//
//                } else {
//                    Toast.makeText(getContext(), "Product is out of stock !", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });

//        convertView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent(v.getContext(), ProductPageActivity.class);
//
////                intent.putExtra("Prodid", (productsList.get(position).getProduct_id()));
////                intent.putExtra("Prodname", (productsList.get(position).getProduct_name()));
////                holder.product_newprice = productsList.get(position).getBase_price();
////                intent.putExtra("Prodprice1", (productsList.get(position).getBase_price()));
////                intent.putExtra("Amount", (productsList.get(position).getAmount()));
//                //holder.product_newprice1 = String.valueOf(holder.product_newprice);
//                NumberFormat nf = NumberFormat.getInstance();
//                nf.setMinimumFractionDigits(2);
//                nf.setMaximumFractionDigits(2);
////                String price = nf.format(Double.parseDouble(String.valueOf(productsList.get(position).getBase_price())));
////                //intent.putExtra("Prodnewprice", price);
////                intent.putExtra("Prodimg", (productsList.get(position).getProduct_image()));
////                intent.putExtra("Prodoldprice", productsList.get(position).getList_price());
//                intent.putExtra("Prodshippingparams", (productsList.get(position).getShipping_params()));
////
////                //intent.putExtra("Calledfromproducts","Product");
////                Log.e("prodid", String.valueOf(productsList.get(position).getProduct_id()));
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//
//                v.getContext().startActivity(intent);
//
//            }
//        });

       /* Intent myintent=new Intent(getContext(), CartActivity.class);
        myintent.putExtra("Prodshippingparams", (productsList.get(position).getShipping_params()));*/

        return convertView;
    }

//    private class PostCart extends AsyncTask<String, String, Void> {
//        String Prod_id;
//        String Prod_newprice;
//        String uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
//        private StringBuffer stringBuffer = new StringBuffer();
//        BufferedReader bufferedReader = null;
//        String message = null;
//
//
//        public PostCart(String productid, String product_newprice1) {
//            this.Prod_id = productid;
//            this.Prod_newprice = product_newprice1;
//            Log.e("price", String.valueOf(Prod_newprice));
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//            JSONObject jsonObject;
//            jsonObject = new JSONObject();
//
//            try {
//                jsonObject.put("user_id", uid);
//                jsonObject.put("product_id", Prod_id);
//                jsonObject.put("amount", "1");
//                jsonObject.put("price", Prod_newprice);
//                jsonObject.put("extra", "");
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            try {
//                HttpClient httpClient = new DefaultHttpClient();
//                HttpPost httpPost = new HttpPost();
//                StringEntity se = new StringEntity(jsonObject.toString());
//                Log.e("test", String.valueOf(se));
//                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart");//refer to all orders. Only GET and POST are supported.
//                httpPost.setURI(uri);
//                httpPost.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                se.setContentType("application/json;charset=UTF-8");
//                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
//                httpPost.setEntity(se);
//                Log.e("Entity", jsonObject.toString());
//                Log.e("CartPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                HttpResponse httpResponse = httpClient.execute(httpPost);
//                Log.e("CartPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                HttpEntity resultEntity = httpResponse.getEntity();
//
//                InputStream inputStream = httpResponse.getEntity().getContent();
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                String readLine = bufferedReader.readLine();
//                while (readLine != null) {
//                    stringBuffer.append(readLine);
//                    stringBuffer.append("\n");
//                    readLine = bufferedReader.readLine();
//                    Log.e("Test", String.valueOf(stringBuffer));
//                }
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//            try {
//                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
//                Log.e("message", String.valueOf(jobj));
//                //jArray = jobj.optJSONObject("wishlist");
//                if (jobj.has("message")) {
//                    message = jobj.getString("message");
//                    Log.e("messa", message);
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//            return null;
//        }
//
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            // progressDialog.dismiss();
//            if (message == null) {
//                Toast.makeText(getContext(), "Added to Cart !",
//                        Toast.LENGTH_LONG).show();
//            } else {
//                Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
//            }
//
//
//        }
//    }

//    private class PostWishlistProduct extends AsyncTask<String, String, String> {
//        String Prod_id;
//        String Prod_newprice;
//        String Prod_shippingparams;
//
//        String uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
//
//        public PostWishlistProduct(String productid, String product_newprice1, String product_shippingparams) {
//            this.Prod_id = productid;
//            this.Prod_newprice = product_newprice1;
//            this.Prod_shippingparams = product_shippingparams;
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            // Log.e("Entered","Hi");
//            JSONObject jsonObject;
//            jsonObject = new JSONObject();
//
//            try {
//                jsonObject.put("user_id", uid);
//                jsonObject.put("product_id", Prod_id);
//                jsonObject.put("amount", "1");
//                jsonObject.put("price", Prod_newprice);
//                jsonObject.put("extra", Prod_shippingparams);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//            try {
//                HttpClient httpClient = new DefaultHttpClient();
//                HttpPost httpPost = new HttpPost();
//                StringEntity se = new StringEntity(jsonObject.toString());
//                Log.e("test", String.valueOf(se));
//                URI uri = new URI(getContext().getString(R.string.url) + "Wishlist");//refer to all orders. Only GET and POST are supported.
//                httpPost.setURI(uri);
//                httpPost.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
//                        HTTP.UTF_8, false));
//                se.setContentType("application/json;charset=UTF-8");
//                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
//                httpPost.setEntity(se);
//                Log.e("Entity", jsonObject.toString());
//                Log.e("WishlistPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                HttpResponse httpResponse = httpClient.execute(httpPost);
//                Log.e("WishlistPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
//                HttpEntity resultEntity = httpResponse.getEntity();
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//
//
//            return null;
//        }
//
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//
//            Toast.makeText(getContext(), "Added to wishlist !",
//                    Toast.LENGTH_LONG).show();
//
//
//        }
//    }


    static class ViewHolder {
        public TextView product_name;
        public TextView product_companyname;
        public TextView product_price;
        public ImageView product_image;
        public TextView product_id;
        public ImageButton wishlist, cart;
        public String productid, uid;
        public String product_newprice, amount;
        public String product_shippingparams, product_newprice1;
    }
}
