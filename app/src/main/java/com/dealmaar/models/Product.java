package com.dealmaar.models;

/**
 * Created by omm on 11/6/2015.
 */
public class Product implements Comparable<Product>{

    private String product_companyname;
    private String product_id;
    private String product_name;
    private String product_image;
    private String base_price;
    private String list_price;
    private String shipping_params;
    private String amount;
    private String status;
    private String category_id;
    private String brand;


    public Product(String product, String my_product) {
    }

    public Product(){

    }

    public Product(String id, String name, String price, String brand, String image, String shipping_params, String category_id, String list_price, String amount, String status)
    {
        this.product_id=id;
        this.product_name=name;
        this.brand=brand;
        this.base_price=price;
        this.product_image=image;
        this.shipping_params=shipping_params;
        this.category_id=category_id;
        this.list_price=list_price;
        this.amount=amount;
        this.status=status;
    }

    public String getCategory_id()
    {
        return category_id;
    }
    public void setCategory_id(String category_id)
    {
        this.category_id=category_id;
    }
    public String getShipping_params() {
        return shipping_params;
    }

    public void setShipping_params(String shipping_params) {
        this.shipping_params = shipping_params;
    }

    public String getList_price() {
        return list_price;
    }

    public void setList_price(String list_price) {
        this.list_price = list_price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBase_price() {
        return base_price;
    }

    public String setBase_price(String base_price) {
        this.base_price = base_price;
        return base_price;
    }


    public String getProduct_companyname() {
        return product_companyname;
    }

    public void setProduct_companyname(String product_companyname) {
        this.product_companyname = product_companyname;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status=status;
    }


    @Override
    public int compareTo(Product another) {
        return 0;
    }
}
