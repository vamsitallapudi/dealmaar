package com.dealmaar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealmaar.customer.R;

/**
 * Created by sciens1 on 8/4/2016.
 */
public class SalesAdapter extends BaseAdapter {
    LayoutInflater inflater;
    Context mContext;
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (inflater == null){
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.sales_adapter_listitem, null);
            holder.mLogoImage = (ImageView)convertView.findViewById(R.id.sales_adapter_logo_iv);
            holder.mTitle = (TextView)convertView.findViewById(R.id.sales_adapter_title_tv);
            holder.mPrice = (TextView)convertView.findViewById(R.id.sales_adapter_price_tv);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        return null;
    }

    public class ViewHolder {
        private ImageView mLogoImage;
        private TextView mTitle,mPrice;
    }
}
