package com.dealmaar.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.login.MyApplication;
import com.dealmaar.models.Product;
import com.dealmaar.productpage.ProductPageActivity;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vamsi on 25-Jun-16.
 */



public class ProductsAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    int productslist_item;
    List<Product> productslists;
    LinearLayout llProductListItem;
    EditText etProductSearch;
    LinearLayout llFilter;
    ImageView ivCart,ivWishList;
    String uid;
    ProgressDialog progressDialog;
    private DatabaseHandler db;
    private MainCardModel mainCardModel;


    public ProductsAdapter(Context context, int productslist_item, ArrayList<Product> productslists){
        this.context = context;
        this.productslist_item =productslist_item;
        this.productslists= productslists;

        uid = Preferences.getString(Preferences.PrefType.User_id, context);

    }

    public ProductsAdapter(Context context, List<Product> productslists){
        this.context = context;
        this.productslists= productslists;
        uid = Preferences.getString(Preferences.PrefType.User_id, context);
    }

    @Override
    public int getCount() {
        return productslists.size();
    }

    @Override
    public Object getItem(int position) {
        return productslists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.productslist_item,null);
        }

        db=((MyApplication)context.getApplicationContext()).getDatabaseHandler();

        TextView tvName = (TextView) convertView.findViewById(R.id.product_name);
        ImageView ivProductImage = (ImageView) convertView.findViewById(R.id.productslist_image);
        ivCart = (ImageView) convertView.findViewById(R.id.iv_cart);
        ivWishList = (ImageView) convertView.findViewById(R.id.iv_wish_list);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);
        etProductSearch = (EditText) convertView.findViewById(R.id.et_product_search);
        tvName.setText(productslists.get(position).getProduct_name());
        llProductListItem = (LinearLayout) convertView.findViewById(R.id.ll_product_list_item);
        llFilter = (LinearLayout) convertView.findViewById(R.id.ll_product_list_item);
        llProductListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(productslists != null && !productslists.equals("")){
                    Intent intent = new Intent(context, ProductPageActivity.class);
                    intent.putExtra("Prodid",productslists.get(position).getProduct_id());
                    intent.putExtra("Prodname",productslists.get(position).getProduct_name());
                    intent.putExtra("Prodoldprice",productslists.get(position).getList_price());
                    Log.d("tvk prodid", productslists.get(position).getProduct_id());
                    intent.putExtra("Prodprice1",productslists.get(position).getBase_price());
                    intent.putExtra("Prodshippingparams",productslists.get(position).getShipping_params());
                    intent.putExtra("Amount",productslists.get(position).getAmount());
                    intent.putExtra("Prodimg", productslists.get(position).getProduct_image());
                    intent.putExtra("FromHome", "H");
                    context.startActivity(intent);
                }

            }
        });



        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(productslists.get(position).getAmount()) > 0) {
                    new PostCart(uid, productslists.get(position).getProduct_id(),
                            productslists.get(position).getProduct_name(),
                            productslists.get(position).getBase_price(),
                            productslists.get(position).getShipping_params()).executeOnExecutor(MyApplication.threadPoolExecutor);

                    try {


                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                View syncItemView =  ((ProductPageActivity)context).findViewById(R.id.action_cart);
                                Log.e("View1", String.valueOf(syncItemView));
                                String count = db.getCartCount(uid);
                                if (count == null || count.equals("null")) {
                                    count = "0";
                                } else if (Integer.parseInt(count) == 0) {
                                    count = "0";
                                } else {
                                    Log.e("C", "Count" + count);

                                    //Log.e("View1", String.valueOf(syncItemView[0]));
                                    BadgeView badge = new BadgeView(context, syncItemView);
                                    badge.setText(count);
                                    badge.show();
                                    notifyDataSetChanged();
                                }


                            }
                        });
                    } catch (Exception e) {

                    }


                } else {
                    Toast.makeText(context, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                }
            }
        });







        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(productslists.get(position).getAmount()) > 0) {
                    new PostCart(uid, productslists.get(position).getProduct_id(),
                            productslists.get(position).getProduct_name(),
                            productslists.get(position).getBase_price(),
                            productslists.get(position).getShipping_params()).executeOnExecutor(MyApplication.threadPoolExecutor);



                } else {
                    Toast.makeText(context, "Product is out of stock !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ivWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("tvk ID", productslists.get(position).getProduct_id());
                new PostWishlistProduct(uid, productslists.get(position).getProduct_id(),
                        productslists.get(position).getProduct_name(), productslists.get(position).getList_price(),
                        productslists.get(position).getShipping_params()).executeOnExecutor(MyApplication.threadPoolExecutor);

            }
        });

        Picasso.with(context).load(productslists.get(position).getProduct_image()).into(ivProductImage);

        double amount = Double.valueOf(productslists.get(position).getBase_price());

        DecimalFormat df = new DecimalFormat("####0.00");

        tvPrice.setText(String.valueOf(df.format(amount)));
        return convertView;
    }

    private class PostWishlistProduct extends AsyncTask<Void, Void, Void> {
        String product_id, product_name, productnew_price, product_shipping;
        String User_id;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Adding to wishlist...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        public PostWishlistProduct(String user_id, String Product_id, String Product_name, String Productnew_price, String Product_shipping) {
            this.User_id = user_id;
            this.product_id = Product_id;
            this.product_name = Product_name;
            this.productnew_price = Productnew_price;
            this.product_shipping = Product_shipping;
        }

        @Override
        protected Void doInBackground(Void... params) {
            JSONObject jsonObject;
            jsonObject = new JSONObject();

            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", product_id);
                jsonObject.put("amount", "1");
                jsonObject.put("price", productnew_price);
                jsonObject.put("extra", product_shipping);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(context.getString(R.string.url) + "Wishlist");//refer to all orders. Only GET and POST are supported.
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(context.getString(R.string.username), context.getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("WishlistPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("WishlistPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                progressDialog.dismiss();
                Toast.makeText(context, "Added to wishlist !",
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                //Toast.makeText(ProductPageActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }

        }
    }

    private class PostCart extends AsyncTask<String, Void, Void> {
        String product_id, product_name, productnew_price, product_shipping;
        private StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        String message = null;

        String User_id;

        public PostCart(String user_id, String Product_id, String Product_name, String Productnew_price, String Product_shipping) {
            this.User_id = user_id;
            this.product_id = Product_id;
            this.product_name = Product_name;
            this.productnew_price = Productnew_price;
            this.product_shipping = Product_shipping;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Adding to cart...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            JSONObject jsonObject;
            jsonObject = new JSONObject();

            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", product_id);
                jsonObject.put("amount", "1");

                jsonObject.put("price", productnew_price);
                jsonObject.put("extra", "");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(context.getString(R.string.url) + "Addtocart");//refer to all orders. Only GET and POST are supported.
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(context.getString(R.string.username), context.getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("CartPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("CartPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();

                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("message", String.valueOf(jobj));
                //jArray = jobj.optJSONObject("wishlist");
                if (jobj.has("message")) {
                    message = jobj.getString("message");
                    Log.e("messa", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (message == null) {
                try{
                    String c=db.getCartCount(uid);
                    // String c = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    if (c == null || c.equals("null")) {
                        c = "0";

                    } else if (Integer.parseInt(c)==0) {
                        c = "0";
                    }

                    String count = String.valueOf(Integer.parseInt(c) + 1);
                    db.deleteCartCount(uid);
                    mainCardModel=new MainCardModel(uid,count);
                    Log.e("Count",count);
                    db.addCartCount(mainCardModel);
                    notifyDataSetInvalidated();
                    //Preferences.add(Preferences.PrefType.CART_count, c, getApplicationContext());

                } catch (Exception e) {

                }

                Toast.makeText(context, "Added to Cart !",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Server Error", Toast.LENGTH_LONG).show();
            }

            try {
                progressDialog.dismiss();
            }catch (Exception e)
            {

            }

        }
    }


}
