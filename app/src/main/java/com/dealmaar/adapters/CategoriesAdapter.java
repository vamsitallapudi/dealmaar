package com.dealmaar.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.dealmaar.activities.SubcategoryActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.productslist.Categories;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vamsi on 30-Jun-16.
 */
public class CategoriesAdapter extends BaseAdapter {

    private List<Categories> primeCatList= null;
    public static ArrayList<Categories> categoriesList;

    private LayoutInflater inflater;
    private Context context;
    private int position;

    public CategoriesAdapter(Context context, List<Categories> primeCatList, ArrayList<Categories> categoriesList) {
        this.primeCatList = primeCatList;
        this.context = context;
        this.categoriesList = categoriesList;
    }

    public CategoriesAdapter(Context context, List<Categories> primeCatList) {
        this.primeCatList = primeCatList;
        this.context = context;
    }


    @Override
    public int getCount() {
        return primeCatList.size();
    }

    @Override
    public Object getItem(int position) {
        return primeCatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.categories_item, null);
        }

        this.position = position;
        Button bCategoryItem = (Button) convertView.findViewById(R.id.b_category_item);


        if (primeCatList.size() != 0) {
            bCategoryItem.setText(primeCatList.get(position).getCategory());
            bCategoryItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context, "Item clicked", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(context, SubcategoryActivity.class);
                    String id = primeCatList.get(position).getCategory_id();
                    i.putExtra("category_id", id);
                    Log.d("this cat id ", id);
                    context.startActivity(i);
                }
            });
        } else {
            Intent intent = new Intent(context, HomeActivity.class);
            context.startActivity(intent);
        }
        return convertView;
    }
}
