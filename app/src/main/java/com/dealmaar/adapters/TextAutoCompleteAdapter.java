package com.dealmaar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dealmaar.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vamsi on 23-Jun-16.
 */
public class TextAutoCompleteAdapter extends BaseAdapter implements Filterable{
    private List<Item> resultList = new ArrayList<>();
    private Context mContext;

    public TextAutoCompleteAdapter (Context mContext){
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Item
    getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_1,parent,false);
        }
        ((TextView)convertView.findViewById(android.R.id.text1)).setText(getItem(position).getName());
        return convertView;

    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null){
                    List<Item> items = findItems(mContext,constraint.toString());
                    filterResults.values = items;
                    filterResults.count = items.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if(results != null && results.count >0){
                    resultList = (List<Item>) results.values;
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }

            }
        };
        return filter;
    }

    private List<Item> findItems(Context context, String itemTitle) {

        return null;

    }


}
