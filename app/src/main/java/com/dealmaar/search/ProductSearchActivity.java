package com.dealmaar.search;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;


import com.batch.android.Batch;
import com.dealmaar.activities.CategoriesActivity;
import com.dealmaar.adapters.ProductsAdapter;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.fragments.FilterFragment;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.models.Product;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ProductSearchActivity extends AppCompatActivity implements View.OnClickListener, FragmentDrawer.FragmentDrawerListener {


    private static final String USERNAME = "phani141@gmail.com";
    private static final String PASSWORD = "1pNql0g9saL9UoD19d6411T6G5X7GL6w";
    public static final String ADDITIONAL_URL = "products&status=A&items_per_page=10&page=";
    EditText etProductSearch;
    ListView lvProducts;
    LinearLayout llFilter;

    //this parameter is used to set price range
    String urlAdd = "";
    List<Product> productsList = new ArrayList<>();
    private ProductsAdapter adapter;
    Button bSort, bFilter, bShopByCat;
    Dialog dialog;
    String url;
    String newChar;
    private Intent mIntent = null;
    String uid;
    int i = 0;
    private String catId;
    DrawerLayout drawerLayout;
    private FragmentDrawer drawerFragment;

    private ProgressDialog progressDialog;
    private DatabaseHandler db;
    private TextView tvNoItems;
    ProgressBar progressBar;
    private String count;
    private int pageCount = 1;
    private boolean value = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        setSupportActionBar(toolbar);


        toolbar.setLogo(R.drawable.logo2);
        Batch.onStart(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        tvNoItems = (TextView) findViewById(R.id.tv_no_items_found);


        drawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.home_fragment_navigation_drawer);
        drawerFragment.setUp(R.id.home_fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.home_drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        drawerFragment.onDestroy();

        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        Batch.User.getEditor().setIdentifier(uid).setAttribute("user_id", uid).save();
        View logo = toolbar.getChildAt(1);


        etProductSearch = (EditText) findViewById(R.id.et_product_search);
        lvProducts = (ListView) findViewById(R.id.lv_products);
        bSort = (Button) findViewById(R.id.b_sort_product_list);
        bFilter = (Button) findViewById(R.id.b_filter);
        bShopByCat = (Button) findViewById(R.id.b_shop_by_cat);

        llFilter = (LinearLayout) findViewById(R.id.ll_filter);

        bSort.setOnClickListener(this);
        bFilter.setOnClickListener(this);
        bShopByCat.setOnClickListener(this);
        if (productsList.size() == 0) {
            if (tvNoItems.getVisibility() == View.GONE) {

                tvNoItems.setVisibility(View.VISIBLE);
            }
        } else {
            tvNoItems.setVisibility(View.GONE);
        }


        if (getIntent().getStringExtra("fromSubCategory").equalsIgnoreCase("1")) {

            etProductSearch.setVisibility(View.GONE);
//            llFilter.setVisibility(View.GONE);

            catId = getIntent().getStringExtra("catid");
            pageCount = 1;
            getUrlAndPerformNetworkRequest(3);

        } else {
            etProductSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    newChar = s.toString();
                    if (newChar.contains(" "))
                        Log.d("textchangedchars: ", s.toString());

                    if (count % 3 == 0) {
                        getUrlAndPerformNetworkRequest(i);
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });


        lvProducts.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView arg0, int arg1) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount > 0) {
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    int myProdCount = lvProducts.getAdapter().getCount();
                    if (myProdCount % 10 == 0 && (lastInScreen==totalItemCount)) {
                        value = false;
                        pageCount++;
                        getUrlAndPerformNetworkRequest(i);
                    }
                }
            }
        });


    }

    private void getUrlAndPerformNetworkRequest(int j) {

        i = j;
        switch (j) {
            case 0:
                url = getString(R.string.url) + ADDITIONAL_URL + pageCount + "?q=" + newChar + urlAdd;
                break;
            case 1:
                url = getString(R.string.url) + ADDITIONAL_URL + pageCount + "?q=" + newChar + "&sort_by=price&sort_order=asc" + urlAdd;
                break;
            case 2:
                url = getString(R.string.url) + ADDITIONAL_URL + pageCount + "?q=" + newChar + "&sort_by=price&sort_order=desc" + urlAdd;
                break;
            case 3:
                url = getString(R.string.url) + "categories/" + catId + "/"+ ADDITIONAL_URL + pageCount + urlAdd;
                break;
            case 4:
                url = getString(R.string.url) + "categories/" + catId + "/"+ ADDITIONAL_URL + pageCount + urlAdd + "&sort_by=price&sort_order=asc";
                break;

            case 5:
                url = getString(R.string.url) + "categories/" + catId + "/"+ADDITIONAL_URL + pageCount + urlAdd + "&sort_by=price&sort_order=desc";
                break;

            default:
                break;
        }
        Log.d("urlbeforeRequest", url);
        new PerformNetworkRequest(url).executeOnExecutor(MyApplication.threadPoolExecutor);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:

                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    progressDialog = new ProgressDialog(ProductSearchActivity.this);
                    progressDialog.setMessage("Processing logout...");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(false);
                    progressDialog.show();

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();
                    progressDialog.dismiss();
                    break;

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }


        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    private class PerformNetworkRequest extends AsyncTask<Void, Void, Void> {

        StringBuffer stringBuffer = new StringBuffer("");
        JSONObject jArray = new JSONObject();
        //StringBuffer stringBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;
        String searchUrl;

        public PerformNetworkRequest(String searchUrl) {
            this.searchUrl = searchUrl;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(ProductSearchActivity.this);
//            progressDialog.setMessage("Fetching Items...");
//            progressDialog.setCancelable(false);
//            progressDialog.setIndeterminate(false);
//            progressDialog.show();
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                // Log.e("url", String.valueOf(uri));

                URI uri = new URI(searchUrl.replace(" ", "%20"));
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                HttpResponse httpResponse;
                httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    //Log.e("Test1", String.valueOf(stringBuffer));
                }


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }


            try {
                JSONObject respObj = new JSONObject(stringBuffer.toString());

                JSONArray productsArray = respObj.getJSONArray("products");
                for (int i = 0; i < productsArray.length(); i++) {

                    JSONObject prodObj = productsArray.getJSONObject(i);
                    final Product products = new Product();
                    try {
                        products.setProduct_name(prodObj.getString("product"));
                        products.setStatus(prodObj.getString("status"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        products.setProduct_id(prodObj.getString("product_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    try {
                        products.setBase_price(prodObj.getString("base_price"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        products.setAmount(prodObj.getString("amount"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        products.setList_price(prodObj.getString("list_price"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        products.setShipping_params(prodObj.getString("shipping_params"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject mainPairObj = prodObj.getJSONObject("main_pair");
                        JSONObject detailed = mainPairObj.getJSONObject("detailed");
                        products.setProduct_image(detailed.getString("image_path"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!prodObj.getString("amount").equalsIgnoreCase("0")) {

                        runOnUiThread(new Runnable() {
                            public void run() {
                                productsList.add(products);
                            }
                        });


                    }
                }

            } catch (JSONException exception) {
                exception.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(value){
                adapter = new ProductsAdapter(ProductSearchActivity.this, productsList);
                lvProducts.setAdapter(adapter);
            }
            adapter.notifyDataSetChanged();
            if (productsList.size() == 0) {
                if (tvNoItems.getVisibility() == View.GONE) {
                    tvNoItems.setVisibility(View.VISIBLE);
                }
            } else {
                tvNoItems.setVisibility(View.GONE);
            }

            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_sort_product_list:
                dialog = new Dialog(this);
                dialog.setContentView(R.layout.productslist_sort);
                dialog.setTitle("Sort List");

                final RadioButton rbProdListAsc = (RadioButton) dialog.findViewById(R.id.rb_prod_list_asc);
                RadioButton rbProdListDes = (RadioButton) dialog.findViewById(R.id.rb_prod_list_des);

                rbProdListAsc.setOnClickListener(this);
                rbProdListDes.setOnClickListener(this);
                dialog.show();
                break;
            case R.id.rb_prod_list_asc:
                dialog.dismiss();
                pageCount = 1;
                value = true;
                productsList.clear();
                if (getIntent().getStringExtra("fromSubCategory").equalsIgnoreCase("1")) {
                    getUrlAndPerformNetworkRequest(4);
                } else {
                    getUrlAndPerformNetworkRequest(1);
                }
                adapter.notifyDataSetChanged();

                break;
            case R.id.rb_prod_list_des:
                dialog.dismiss();
                pageCount = 1;
                value = true;
                productsList.clear();
                if (getIntent().getStringExtra("fromSubCategory").equalsIgnoreCase("1")) {
                    etProductSearch.setVisibility(View.GONE);
                    catId = getIntent().getStringExtra("catid");

                    getUrlAndPerformNetworkRequest(5);


                } else {
                    getUrlAndPerformNetworkRequest(2);
                }
                break;
            case R.id.b_filter:
                if (tvNoItems.getVisibility() == View.VISIBLE) {
                    tvNoItems.setVisibility(View.GONE);
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, new FilterFragment())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.b_shop_by_cat:
                Intent i = new Intent(ProductSearchActivity.this, CategoriesActivity.class);
                startActivity(i);
            default:
                break;
        }
    }

    public void setPriceFilter(String urlAdd) {
        this.urlAdd = urlAdd;
        getUrlAndPerformNetworkRequest(i);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View cart = findViewById(R.id.action_cart);
                    // View notifications=findViewById(R.id.action_posts);
                    // Log.e("View", String.valueOf(cart));
                    // String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                    //Integer c=Integer.parseInt(count);
                    // Log.e("Co", "Count" + (count.equals("null")));
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("ProductSearchActivity", e.toString());
                    }
                    if (count == null || (count.equals("null"))) {
                        //Log.e("If", "if");
                        count = "0";

                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {
                        // Log.e("C", "Count" + count);
                        //Log.e("View1", String.valueOf(syncItemView[0]));
                        BadgeView badge = new BadgeView(ProductSearchActivity.this, cart);
                        badge.setText(count);
                        badge.show();
                    }
                    //HomeActivity.this.invalidateOptionsMenu();
                }
            });
        } catch (Exception e) {

        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_cart:
                Intent cartintent = new Intent(ProductSearchActivity.this, CartActivity.class);
                startActivity(cartintent);
                // finish();
                return true;


            case R.id.action_posts:
                Intent postsintent = new Intent(ProductSearchActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
