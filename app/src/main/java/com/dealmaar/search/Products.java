package com.dealmaar.search;

/**
 * Created by omm on 3/17/2016.
 */
public class Products {
    private String name;
    private int id;
    private String price;
    private String brand;
    private String image;
    private String shipping_params;

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setShipping_params(String shipping_params) {
        this.shipping_params = shipping_params;
    }

    public Products(int id, String name, String price, String brand, String image, String shipping_params)
    {
        this.id=id;
        this.name=name;
        this.brand=brand;
        this.price=price;
        this.image=image;
        this.shipping_params=shipping_params;
    }

    public Products(){

    }
    public String getImage()
    {
        return image;
    }
    public String getName()
    {
        return name;
    }
    public String getBrand()
    {
        return brand;
    }
    public int getId()
    {
        return  id;
    }
    public String getShipping_params()
    {
        return shipping_params;
    }
    public String getPrice()
    {
        return price;
    }

}
