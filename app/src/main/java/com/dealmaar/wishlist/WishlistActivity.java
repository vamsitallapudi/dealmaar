package com.dealmaar.wishlist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.batch.android.Batch;
import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WishlistActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    public ProgressDialog progressDialog;
    ArrayList<WishList> arraywishlist;
    ListView list;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    String uid;
    TextView noitems;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    private String count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wishlist_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        View logo=toolbar.getChildAt(1);
        // toolbar.setNavigationIcon(R.drawable.menu);
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());

        if (uid == null) {
            mIntent = new Intent(this, SigninActivity.class);
            startActivity(mIntent);
            finish();
        }

        db=((MyApplication)getApplicationContext()).getDatabaseHandler();
        new GetWishListTask(uid).executeOnExecutor(MyApplication.threadPoolExecutor);

        list = (ListView) findViewById(R.id.wishlist);
        arraywishlist = new ArrayList<WishList>();
        noitems=(TextView)findViewById(R.id.wishlist_noitems);

        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        //displayView(0);
    }
    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.LEFT))
        {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
        else {
            super.onBackPressed();
        }
    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

           /* case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null). setAttribute("user_id","null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s=db.getName();
                    db.deleteName(s);
                    Log.e("N", "Name" + db.getName());
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private class GetWishListTask extends AsyncTask<String, Void, List<JSONObject>> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject wishlistObject = new JSONObject();
        String User_id;

        public GetWishListTask(String user_id) {
            this.User_id = user_id;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(WishlistActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected List<JSONObject> doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Wishlist/" + User_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("WishList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("WishList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                wishlistObject = jobj.optJSONObject("wishlist");

                if (wishlistObject != null) {
                    for (int i = 0; i < wishlistObject.length(); i++) {
                        WishList wish = new WishList();
                        JSONArray productArray = wishlistObject.getJSONArray("product");
                        JSONObject wishlistItemObject = wishlistObject.getJSONObject(String.valueOf(i));
                        JSONObject productObject = productArray.getJSONObject(i);
                        wish.setProduct_id(wishlistItemObject.getString("product_id"));
                        wish.setPrice(wishlistItemObject.getString("price"));
                        try {
                            wish.setList_price(productObject.getString("list_price"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            wish.setShipping_params(productObject.getString("shipping_params"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            wish.setProduct_name(((JSONObject) productArray.get(i)).getString("product"));
                            Log.e("Name", ""+((JSONObject) productArray.get(i)).getString("product"));

                            wish.setProduct_image(((JSONObject) productArray.get(i)).getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                            Log.e("Amount", "A" + ((JSONObject) productArray.get(i)).getString("amount"));
                            wish.setAmount(((JSONObject) productArray.get(i)).getString("amount"));


                        } catch (JSONException e) {
                            Log.e("log_tag", "Error parsing data " + e.toString());

                        }
                        arraywishlist.add(wish);
                    }
                } else {
                    arraywishlist.isEmpty();
                }

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(List<JSONObject> result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (arraywishlist.size() <= 0) {
                /*Intent intent = new Intent(WishlistActivity.this, WishNoitemsFoundActivity.class);
                startActivity(intent);
                return;*/
                String count="null";
                Preferences.add(Preferences.PrefType.CART_count,count,getApplicationContext());

                noitems.setVisibility(View.VISIBLE);
            }

            WishListAdapter adapter = new WishListAdapter(WishlistActivity.this, R.layout.wishlist_item, arraywishlist);
                list.setAdapter(adapter);

            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        try{
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View syncItemView = findViewById(R.id.action_cart);
                Log.e("View", String.valueOf(syncItemView));

                try{
                    count = db.getCartCount(uid);
                }catch (Exception e){
                    if (count == null || (count.equals("null"))) {
                        //Log.e("If", "if");
                        count = "0";

                    }
                    Log.e("WishlistActivity", e.toString());
                }
                //String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                if (count ==null || count.equals("null")) {
                    count = "0";
                }
                else if (Integer.parseInt(count)==0) {
                    count = "0";
                }
                else {
                    Log.e("C", "Count" + count);
                    //Log.e("View1", String.valueOf(syncItemView[0]));
                    BadgeView badge = new BadgeView(WishlistActivity.this, syncItemView);
                    badge.setText(count);
                    badge.show();
                }

            }
        });
        }catch (Exception e){

        }
        return true;
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        WishlistActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(WishlistActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(WishlistActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
