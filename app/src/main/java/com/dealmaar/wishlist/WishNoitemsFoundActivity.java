package com.dealmaar.wishlist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.dealmaar.cart.CartActivity;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SettingsActivity;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;

/**
 * Created by omm on 2/29/2016.
 */
public class WishNoitemsFoundActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    public ProgressDialog progressDialog;
    ArrayList<WishList> arraywishlist;
    ListView list;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_items_found);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        // toolbar.setNavigationIcon(R.drawable.menu);
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.arrowleft);
        upArrow.setColorFilter(getResources().getColor(R.color.appcolor), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View logo=toolbar.getChildAt(1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        //displayView(0);
    }

    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(i);

    }

    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, SettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;

            /*case 4:
                mIntent = new Intent(this, FeedbackActivity.class);
                startActivity(mIntent);
                //finish();
                break;*/
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    // Log.e("id",Preferences.getString(Preferences.PrefType.User_id,getApplicationContext()));
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View syncItemView = findViewById(R.id.action_cart);
                Log.e("View", String.valueOf(syncItemView));
                String count = Preferences.getString(Preferences.PrefType.CART_count, getApplicationContext());
                if (count==null || Integer.parseInt(count) ==0) {
                    count = "0";
                }
                else {
                    Log.e("C", "Count" + count);
                    //Log.e("View1", String.valueOf(syncItemView[0]));
                    BadgeView badge = new BadgeView(WishNoitemsFoundActivity.this, syncItemView);
                    badge.setText(count);
                    badge.show();
                }

            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_cart:
                Intent cartintent = new Intent(WishNoitemsFoundActivity.this, CartActivity.class);
                startActivity(cartintent);

                return true;

            case R.id.action_posts:
                Intent postsintent = new Intent(WishNoitemsFoundActivity.this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

