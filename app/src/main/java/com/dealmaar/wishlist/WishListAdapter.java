package com.dealmaar.wishlist;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.login.MyApplication;
import com.dealmaar.productpage.ProductPageActivity;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by OMM on 12/2/2015.
 */
public class WishListAdapter extends ArrayAdapter<WishList> {
    ArrayList<WishList> ArrayListWishList;
    int Resource;
    Context context;
    LayoutInflater vi;
    ListView list;
    TextView noitems;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    String uid;

    public WishListAdapter(Context context, int resource, ArrayList<WishList> objects) {
        super(context, resource, objects);
        ArrayListWishList = objects;
        Resource = resource;
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.wishlist_item, parent, false);
            holder = new ViewHolder();
            //holder.product_id = (TextView) convertView.findViewById(R.id.customer_wishlist_name);
            holder.price = (TextView) convertView.findViewById(R.id.customer_wishlist_price);
            holder.cart = (ImageButton) convertView.findViewById(R.id.wishlist_addtocart);
            holder.delete = (ImageButton) convertView.findViewById(R.id.wishlist_delete);
            holder.product_name = (TextView) convertView.findViewById(R.id.wishlist_product_name);
            holder.product_image = (ImageView) convertView.findViewById(R.id.wishlist_image);
            list = (ListView) parent.findViewById(R.id.wishlist);
            noitems = (TextView) parent.getRootView().findViewById(R.id.wishlist_noitems);
            db = ((MyApplication) getContext().getApplicationContext()).getDatabaseHandler();
            try {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        View syncItemView = parent.getRootView().findViewById(R.id.action_cart);
                        Log.e("View1", String.valueOf(syncItemView));
                        String count = db.getCartCount(uid);
                        //String count = Preferences.getString(Preferences.PrefType.CART_count, getContext());
                        if (count == null || count.equals("null")) {
                            count = "0";
                        } else if (Integer.parseInt(count) == 0) {
                            count = "0";
                        } else {
                            Log.e("C", "Count" + count);

                            //Log.e("View1", String.valueOf(syncItemView[0]));
                            BadgeView badge = new BadgeView(getContext(), syncItemView);
                            badge.setText(count);
                            badge.show();
                            ((WishlistActivity) context).invalidateOptionsMenu();
                        }


                    }
                });

            } catch (Exception e) {

            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // holder.product_id.setText(ArrayListWishList.get(position).getProduct_id());
        //holder.price.setText(ArrayListWishList.get(position).getPrice());
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        uid=Preferences.getString(Preferences.PrefType.User_id,getContext());
        holder.product_name.setText(ArrayListWishList.get(position).getProduct_name());
        holder.price.setText(nf.format(Double.parseDouble(String.valueOf(ArrayListWishList.get(position).getPrice()))));
        holder.Product_id = ArrayListWishList.get(position).getProduct_id();
        holder.Price = ArrayListWishList.get(position).getPrice();
        holder.amount = ArrayListWishList.get(position).getAmount();
        Log.e("A", "" + holder.amount);



        Uri uri;
        if (ArrayListWishList.get(position).getProduct_image() != null) {
            uri = Uri.parse(ArrayListWishList.get(position).getProduct_image());
        } else {
            uri = Uri.parse(String.valueOf(R.drawable.ic_launcher));
        }

        Context mcontext = holder.product_image.getContext();
        Picasso.with(mcontext)
                .load(uri).error(R.drawable.ic_launcher).into(holder.product_image);


        holder.uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
        Log.e("uid", holder.uid);
        holder.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.amount) > 0) {

                    new PostCart(holder.Product_id, holder.Price, holder.uid).executeOnExecutor(MyApplication.threadPoolExecutor);

                } else {
                    Toast.makeText(getContext(), "Product is out of stock !", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new WishlistDel(holder.Product_id, holder.uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                // ArrayListWishList.remove(position);
                notifyDataSetChanged();
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ProductPageActivity.class);
                intent.putExtra("Prodid",ArrayListWishList.get(position).getProduct_id());
                intent.putExtra("Prodname",ArrayListWishList.get(position).getProduct_name());
                intent.putExtra("Prodoldprice",ArrayListWishList.get(position).getList_price());
                intent.putExtra("Prodprice1",ArrayListWishList.get(position).getPrice());
                intent.putExtra("Prodshippingparams",ArrayListWishList.get(position).getShipping_params());
                intent.putExtra("Amount",ArrayListWishList.get(position).getAmount());
                intent.putExtra("Prodimg", ArrayListWishList.get(position).getProduct_image());
                intent.putExtra("FromHome", "H");
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    private class GetWishListTask extends AsyncTask<String, Void, List<JSONObject>> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        String User_id;

        ProgressDialog progressDialog=new ProgressDialog(context);
        public GetWishListTask(String user_id) {
            this.User_id = user_id;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected List<JSONObject> doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getContext().getString(R.string.url) + "Wishlist/" + User_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("WishList-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("WishList-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                jArray = jobj.optJSONObject("wishlist");

                if (jArray != null) {
                    for (int i = 0; i < jArray.length(); i++) {
                        WishList wish = new WishList();
                            JSONArray jsonArray = jArray.optJSONArray("product");

                        JSONObject jRealObj = jArray.getJSONObject(String.valueOf(i));
                        wish.setProduct_id(jRealObj.getString("product_id"));
                        wish.setPrice(jRealObj.getString("price"));
                        try {
                            wish.setProduct_name(((JSONObject) jsonArray.get(i)).getString("product"));
                            Log.e("Name", ((JSONObject) jsonArray.get(i)).getString("product"));
                            wish.setAmount(((JSONObject) jsonArray.get(i)).getString("amount"));
                            wish.setProduct_image(((JSONObject) jsonArray.get(i)).getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                        } catch (JSONException e) {
                            Log.e("log_tag", "Error parsing data " + e.toString());

                        }
                        ArrayListWishList.add(wish);
                    }
                } else {
                    ArrayListWishList.isEmpty();
                }

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(List<JSONObject> result) {
            super.onPostExecute(result);
            try {
                if (ArrayListWishList.size() <= 0) {
                /*Intent intent = new Intent(context, WishNoitemsFoundActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
                return;*/


                    noitems.setVisibility(View.VISIBLE);
                    ((WishlistActivity) context).invalidateOptionsMenu();
                    /*WishListAdapter adapter = new WishListAdapter(getContext(), R.layout.wishlist_item, ArrayListWishList);
                    list.setAdapter(adapter);*/
                }
                WishListAdapter adapter = new WishListAdapter(getContext(), R.layout.wishlist_item, ArrayListWishList);
                list.setAdapter(adapter);

                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                //progressDialog.dismiss();
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                Toast.makeText(getContext(), "Please Check your internet connection!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class WishlistDel extends AsyncTask<String, String, Void> {
        String Prod_id, User_id;
        ProgressDialog progressDialog = new ProgressDialog(context);

        public WishlistDel(String Product_id, String user_id) {
            this.Prod_id = Product_id;
            this.User_id = user_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpDelete delete = new HttpDelete();

                URI uri = new URI(getContext().getString(R.string.url) + "wishlist/" + User_id + "?product_id=" + Prod_id);
                delete.setURI(uri);
                delete.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                delete.setHeader("content-type", "application/json");
                Log.e("WishlistDelete-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(delete);
                Log.e("WishlistDelete-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity httpEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ArrayListWishList.removeAll(ArrayListWishList);
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();
            try {
                ArrayListWishList.removeAll(ArrayListWishList);
                new GetWishListTask(User_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                Toast.makeText(getContext(), "Removed from wishlist !",
                        Toast.LENGTH_LONG).show();
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }

            } catch (Exception e) {
                Toast.makeText(getContext(), "Please Check your internet connection!", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private class PostCart extends AsyncTask<String, String, Void> {
        String Prod_id, Prod_newprice, User_id;
        ProgressDialog progressDialog = new ProgressDialog(context);
        private StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        String message = null;


        public PostCart(String Product_id, String product_newprice1, String user_id) {
            this.Prod_id = Product_id;
            this.Prod_newprice = product_newprice1;
            this.User_id = user_id;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            JSONObject jsonObject;
            jsonObject = new JSONObject();


            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", Prod_id);
                jsonObject.put("amount", "1");
                jsonObject.put("price", Prod_newprice);
                jsonObject.put("extra", "");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart");
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                httpPost.setEntity(se);
                Log.e("Entity", jsonObject.toString());
                Log.e("CartPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("CartPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                    Log.e("Test", String.valueOf(stringBuffer));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.e("message", String.valueOf(jobj));
                //jArray = jobj.optJSONObject("wishlist");
                if (jobj.has("message")) {
                    message = jobj.getString("message");
                    Log.e("messa", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();
            try {
                if (message == null) {
                    String c = db.getCartCount(uid);
                    // String c = Preferences.getString(Preferences.PrefType.CART_count, getContext());
                    if (c == null) {
                        c = "0";
                    } else if (Integer.parseInt(c) == 0) {
                        c = "0";
                    }
                    String count = String.valueOf(Integer.parseInt(c) + 1);

                    //Preferences.add(Preferences.PrefType.CART_count,c,getContext());
                    db.deleteCartCount(c);
                    mainCardModel = new MainCardModel(uid,count);
                    db.addCartCount(mainCardModel);
                    Toast.makeText(getContext(), "Added to cart", Toast.LENGTH_LONG).show();
                    ArrayListWishList.removeAll(ArrayListWishList);
                    new WishlistDel(Prod_id,User_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                    if(progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }

                } else {
                    Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e)
            {

            }


        }
    }


    static class ViewHolder {
        public TextView product_id, product_name;
        public TextView price;
        public ImageButton cart, delete;
        public String Price, Product_id, uid, amount;
        public ImageView product_image;

    }
}
