package com.dealmaar.wishlist;

/**
 * Created by OMM on 12/2/2015.
 */
public class WishList {

    private String product_id;
    private String price;
    private String product_image;
    private String product_name, amount,list_price,shipping_params;

    public WishList() {
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShipping_params() {
        return shipping_params;
    }

    public void setShipping_params(String shipping_params) {
        this.shipping_params = shipping_params;
    }

    public String getList_price() {
        return list_price;
    }

    public void setList_price(String list_price) {
        this.list_price = list_price;
    }
}
