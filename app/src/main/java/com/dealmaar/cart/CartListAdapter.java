package com.dealmaar.cart;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.customer.R;
import com.dealmaar.login.MyApplication;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by OMM on 12/3/2015.
 */
public class CartListAdapter extends ArrayAdapter<Cart> {
    ArrayList<Cart> ArrayCartList;
    Context context;
    int Resource,cart_amt;
    LayoutInflater vi;
    TextView cart_amount, noitems,shipping_amt;
    String Price, amt, price,shippingId,cart,shipping,shippingAmount,id=null;
    double Total, total;
    ListView list;
    DatabaseHandler db;
    MainCardModel mainCardModel;
    Button checkout,checkout1;


    public CartListAdapter(Context context, int resource, ArrayList<Cart> objects, double total, TextView cart, String shippingamt, TextView shipping_amt,Button checkout,Button checkout1) {
        super(context, resource, objects);
        ArrayCartList = objects;
        Resource = resource;
        this.context = context;
        this.Total = total;
        this.cart_amount = cart;
        this.shipping=shippingamt;
        this.shipping_amt=shipping_amt;
        this.checkout=checkout;
        this.checkout1=checkout1;

        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = vi.inflate(R.layout.cart_item, parent, false);
            holder = new ViewHolder();
            NumberFormat nf = NumberFormat.getInstance();
            holder.product_name = (TextView) convertView.findViewById(R.id.cart_productname);
            //holder.product_id = (TextView) convertView.findViewById(R.id.customer_cart_id);
            holder.price = (TextView) convertView.findViewById(R.id.customer_cart_price);
            holder.wishlist = (Button) convertView.findViewById(R.id.cart_movetowishlist);
            holder.remove = (Button) convertView.findViewById(R.id.cart_remove);
            holder.plus = (ImageButton) convertView.findViewById(R.id.cart_add);
            holder.minus = (ImageButton) convertView.findViewById(R.id.cart_minus);
            holder.cart_amt = (TextView) convertView.findViewById(R.id.cart_number);
            holder.product_image = (ImageView) convertView.findViewById(R.id.cart_image);
           // shipping_amt=(TextView)parent.findViewById(R.id.shipping_total);
            holder.delivery=(TextView)parent.findViewById(R.id.cart_delivery);
            holder.checkout=(Button)parent.findViewById(R.id.cart_proceedtocheckout);
            holder.checkout1=(Button)parent.findViewById(R.id.cart_proceedtocheckout1);
            list = (ListView) parent.findViewById(R.id.customer_cart_list);
            noitems = (TextView) parent.getRootView().findViewById(R.id.cart_noitems);
            // Toolbar toolbar=(Toolbar)parent.findViewById(R.id.app_bar);
            //Menu menu=toolbar.getMenu();

            try {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        View syncItemView = parent.getRootView().findViewById(R.id.action_cart);
                        String count = Preferences.getString(Preferences.PrefType.CART_count, getContext());
                        if (count == null || count.equals("null")) {
                            Log.e("Entered", "if");
                            count = "0";

                        } else if (Integer.parseInt(count) == 0) {
                            count = "0";
                        } else {
                            Log.e("Entered", "Else");
                            Log.e("C", "Count" + count);
                            //Log.e("View1", String.valueOf(syncItemView[0]));
                            BadgeView badge = new BadgeView(getContext(), syncItemView);
                            badge.setText(count);
                            badge.show();
                            ((CartActivity) context).invalidateOptionsMenu();
                        }
                    }
                });
            } catch (Exception e) {

            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);

        holder.price.setText(nf.format(Double.parseDouble(String.valueOf(ArrayCartList.get(position).getPrice()))));
        holder.product_name.setText(ArrayCartList.get(position).getProduct_name());
        holder.Price1 = ArrayCartList.get(position).getPrice();
        holder.Product_id = ArrayCartList.get(position).getProduct_id();
        holder.cart_amt.setText(ArrayCartList.get(position).getAmount());
        Uri uri;
        if (ArrayCartList.get(position).getProduct_image() != null) {
            uri = Uri.parse(ArrayCartList.get(position).getProduct_image());
        } else {
            uri = Uri.parse(String.valueOf(R.drawable.ic_launcher));
        }
        Context mcontext = holder.product_image.getContext();
        Picasso.with(mcontext)
                .load(uri).error(R.drawable.ic_launcher).into(holder.product_image);
        holder.uid = Preferences.getString(Preferences.PrefType.User_id, context.getApplicationContext());
        holder.wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id=null;
                new PostWishlistProduct(holder.Product_id, holder.Price1, holder.uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                ArrayCartList.remove(position);
                notifyDataSetChanged();

            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id=null;
                new RemoveCart(holder.Product_id, holder.uid).executeOnExecutor(MyApplication.threadPoolExecutor);
                notifyDataSetChanged();

            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    amt = ArrayCartList.get(position).getAmount();
                   id= String.valueOf(position);
                    Log.e("Position",""+id);
                    price = ArrayCartList.get(position).getPrice();
                    Log.e("plus position", String.valueOf(position));
                    new AddCart(holder.Product_id, holder.uid, price, amt, Total).executeOnExecutor(MyApplication.threadPoolExecutor);
                    cart_amt = Integer.parseInt(amt) + 1;
                }catch (Exception e)
                {

                }
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    amt = ArrayCartList.get(position).getAmount();
                    price = ArrayCartList.get(position).getPrice();
                    id= String.valueOf(position);
                    if (Integer.parseInt(amt) > 1) {
                        new DelCart(holder.Product_id, holder.uid, price, amt, Total).executeOnExecutor(MyApplication.threadPoolExecutor);
                        cart_amt = Integer.parseInt(amt) - 1;

                    }

                }catch (Exception e)
                {

                }
            }
        });

        return convertView;

    }

    private class GetShippingrate extends AsyncTask<String, Void, Void> {
        String user_id;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        GetShippingrate(String User_id)
        {
            this.user_id=User_id;
        }
        ProgressDialog progressDialog=new ProgressDialog(context);

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getContext().getString(R.string.url) + "Shippingrate/" + user_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Shipping-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Shipping-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jsonObject=new JSONObject(String.valueOf(stringBuffer));
                if(jsonObject.has("price"))
                {
                    if(shippingAmount=="false" || shippingAmount==null)
                    {
                        shippingAmount="0.0";
                    }

                    shippingAmount=jsonObject.getString("price");
                    JSONObject jsonObject1=jsonObject.optJSONObject("keys");
                    shippingId=jsonObject1.getString("shipping_id");
                }
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try
            {
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            }catch (Exception e)
            {

            }
        }
    }

    private class GetCartListTask extends AsyncTask<Void, Void, Double> {

        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();
        String uid = Preferences.getString(Preferences.PrefType.User_id, getContext());

        ProgressDialog progressDialog=new ProgressDialog(context);
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Double doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart/" + uid);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Cart-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Cart-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                jArray = jobj.optJSONObject("addtocart");
                ArrayCartList.removeAll(ArrayCartList);
                if (jArray != null) {
                    for (int i = 0; i < jArray.length(); i++) {
                        Cart cart = new Cart();
                        JSONArray jsonArray = jArray.optJSONArray("product");
                        JSONObject jRealObj = jArray.getJSONObject(String.valueOf(i));
                        cart.setProduct_id(jRealObj.getString("product_id"));
                        cart.setItem_id(jRealObj.getString("item_id"));
                        cart.setPrice(jRealObj.getString("price"));
                        cart.setAmount(jRealObj.getString("amount"));
                        //total=Double.parseDouble(jRealObj.getString("price"));
                        try {
                            cart.setProduct_name(((JSONObject) jsonArray.get(i)).getString("product"));
                            cart.setProduct_image(((JSONObject) jsonArray.get(i)).getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                        } catch (JSONException e) {
                            Log.e("log_tag", "Error parsing data " + e.toString());

                        }

                        int amt = Integer.parseInt(jRealObj.getString("amount"));
                        double Total, price;
                        price = Double.parseDouble(jRealObj.getString("price"));
                        Total = price;
                        if (amt > 1) {
                            for (int j = 1; j < amt; ++j) {

                                Total += price;
                            }
                        }

                        ArrayCartList.add(cart);
                        //total=Preferences.getDouble(Preferences.PrefType.CART_Total,getApplicationContext());

                        total += Total;
                    }
                } else {
                    ArrayCartList.isEmpty();

                }

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return total;
        }

        protected void onPostExecute(Double result) {
            super.onPostExecute(result);
            ((CartActivity) context).invalidateOptionsMenu();
            if (ArrayCartList.size() <= 0) {

                NumberFormat nf = NumberFormat.getInstance();
                nf.setMinimumFractionDigits(2);
                nf.setMaximumFractionDigits(2);

                String count = null;
                db = ((MyApplication) getContext().getApplicationContext()).getDatabaseHandler();
                String c = db.getCartCount(uid);
                db.deleteCartCount(c);
                mainCardModel = new MainCardModel(uid,count);
                db.addCartCount(mainCardModel);
                noitems.setVisibility(View.VISIBLE);
                shippingAmount = "0.0";
                shipping_amt.setText(nf.format(Double.parseDouble(String.valueOf(shippingAmount))));
                checkout.setVisibility(View.GONE);
                checkout1.setVisibility(View.VISIBLE);
            }


            try {
                if (shippingAmount == null || shippingAmount.equals("false") || shippingAmount.equals(null)) {
                    shippingAmount = "0.0";
                }
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMinimumFractionDigits(2);
                nf.setMaximumFractionDigits(2);

                cart_amount.setText(nf.format(Double.parseDouble(String.valueOf(total))));
                shipping_amt.setText(nf.format(Double.parseDouble(String.valueOf(shippingAmount))));

                CartListAdapter adapter = new CartListAdapter(getContext(), R.layout.cart_item, ArrayCartList, total, cart_amount, shippingAmount, shipping_amt, checkout, checkout1);

                    Log.e("Entered", "id" + id);

                   // list.smoothScrollToPosition(id);
                                // progressDialog.dismiss();
                list.setAdapter(adapter);
                if(id==null || id.equals(""))
                {
                    adapter.notifyDataSetChanged();
                }
               else {
                    list.setSelectionFromTop(Integer.parseInt(id), 0);
                    adapter.notifyDataSetChanged();
                }
               // adapter.notifyDataSetChanged();
                String count = String.valueOf(list.getAdapter().getCount());
                db = ((MyApplication) getContext().getApplicationContext()).getDatabaseHandler();
                String c = db.getCartCount(uid);
                db.deleteCartCount(c);
                mainCardModel = new MainCardModel(uid,count);
                db.addCartCount(mainCardModel);
               /* String count = String.valueOf(list.getAdapter().getCount());
                Preferences.add(Preferences.PrefType.CART_count, count, getContext());*/


                try {
                    progressDialog.dismiss();
                } catch (Exception e) {

                }
            }catch (Exception e)
            {

            }
        }
    }


    private class DelCart extends AsyncTask<String, String, String> {
        String Prod_id, User_id, Pri, amount;
        double tot;

        //int cart_amt,Price;

        ProgressDialog progressDialog = new ProgressDialog(getContext());

        public DelCart(String Product_id, String user_id, String price, String amt, double total) {
            this.Prod_id = Product_id;
            this.User_id = user_id;
            this.Pri = price;
            this.amount = amt;
            this.tot = total;

        }

        JSONObject jsonObject = new JSONObject();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            cart_amt = Integer.parseInt(amount) - 1;
            try {
                jsonObject.put("amount", cart_amt);
                jsonObject.put("product_id", Prod_id);
                jsonObject.put("price", Price);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart/" + User_id);
                Log.e("Uri", String.valueOf(uri));

                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);

                Log.e("Entity", jsonObject.toString());
                Log.e("CartMinus-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("CartMinus-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return String.valueOf(cart_amt);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ArrayCartList.removeAll(ArrayCartList);
            try {
                new GetShippingrate(User_id).execute();
                new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                notifyDataSetChanged();
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
               // Toast.makeText(getContext(), "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }

    }

    private class AddCart extends AsyncTask<String, String, String> {
        String Prod_id, User_id, Pri, amount;
        double tot;

        //int cart_amt,Price;

        ProgressDialog progressDialog = new ProgressDialog(context);

        public AddCart(String Product_id, String user_id, String price, String amt, double total) {
            this.Prod_id = Product_id;
            this.User_id = user_id;
            this.Pri = price;
            this.amount = amt;
            this.tot = total;

        }

        JSONObject jsonObject = new JSONObject();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            cart_amt = Integer.parseInt(amount) + 1;
            ArrayCartList.removeAll(ArrayCartList);

            try {
                jsonObject.put("amount", cart_amt);
                jsonObject.put("product_id", Prod_id);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPut put = new HttpPut();
                StringEntity se = new StringEntity(jsonObject.toString());
                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart/" + User_id);
                put.setURI(uri);
                put.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
                put.setEntity(se);
                Log.e("CartAdd-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(put);
                Log.e("CartAdd-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return String.valueOf(cart_amt);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ArrayCartList.removeAll(ArrayCartList);
            try {
                new GetShippingrate(User_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                notifyDataSetChanged();
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                //Toast.makeText(getContext(), "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private class RemoveCart extends AsyncTask<String, String, Void> {
        String Prod_id, User_id;
        ProgressDialog progressDialog = new ProgressDialog(getContext());

        public RemoveCart(String Product_id, String user_id) {
            this.Prod_id = Product_id;
            this.User_id = user_id;

        }

        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpDelete delete = new HttpDelete();

                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart/" + User_id + "?product_id=" + Prod_id);
                delete.setURI(uri);
                delete.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                delete.setHeader("content-type", "application/json");
                Log.e("CartDelete-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(delete);
                Log.e("CartDelete-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity httpEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ArrayCartList.removeAll(ArrayCartList);
            try {
                new GetShippingrate(User_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                Toast.makeText(getContext(), "Removed from cart !",
                        Toast.LENGTH_LONG).show();
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                Log.e("Exception", String.valueOf(e));
            }


        }
    }

    private class PostWishlistProduct extends AsyncTask<String, String, String> {
        String Prod_id, Price, User_id;

        ProgressDialog progressDialog = new ProgressDialog(context);

        public PostWishlistProduct(String Product_id, String Price1, String user_id) {
            this.Prod_id = Product_id;
            this.Price = Price1;
            this.User_id = user_id;

        }

        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
           progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject;
            jsonObject = new JSONObject();

            try {
                jsonObject.put("user_id", User_id);
                jsonObject.put("product_id", Prod_id);
                jsonObject.put("amount", "1");
                jsonObject.put("price", Price);
                jsonObject.put("extra", "");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost();
                StringEntity se = new StringEntity(jsonObject.toString());
                Log.e("test", String.valueOf(se));
                URI uri = new URI(getContext().getString(R.string.url) + "Wishlist");//refer to all orders. Only GET and POST are supported.
                httpPost.setURI(uri);
                httpPost.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                se.setContentType("application/json;charset=UTF-8");

                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

                httpPost.setEntity(se);
                Log.e("CartPost-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("CartPost-After", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpEntity resultEntity = httpResponse.getEntity();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpDelete delete = new HttpDelete();

                URI uri = new URI(getContext().getString(R.string.url) + "Addtocart/" + User_id + "?product_id=" + Prod_id);
                delete.setURI(uri);
                delete.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getContext().getString(R.string.username), getContext().getString(R.string.password)),
                        HTTP.UTF_8, false));
                delete.setHeader("content-type", "application/json");
                HttpResponse httpResponse = httpClient.execute(delete);
                HttpEntity httpEntity = httpResponse.getEntity();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // progressDialog.dismiss();
            ArrayCartList.removeAll(ArrayCartList);
            try {

                new GetShippingrate(User_id).executeOnExecutor(MyApplication.threadPoolExecutor);
                new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);
                Toast.makeText(getContext(), "Added to wishlist !",
                        Toast.LENGTH_LONG).show();
                if(progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                Toast.makeText(getContext(), "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();

            }
        }
    }


    static class ViewHolder {
        public TextView product_id,price, product_name, cart_amt,shipping_amt,delivery;
        public Button wishlist, remove,checkout,checkout1;
        public String Product_id, Price1, uid;
        public ImageView product_image;
        public ImageButton plus, minus;
        public LinearLayout shipping;

    }
}
