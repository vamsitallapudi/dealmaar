package com.dealmaar.cart;

import java.io.Serializable;

/**
 * Created by OMM on 12/3/2015.
 */
public class Cart implements Serializable {
    private String product_id;
    private String price;
    public String item_id;
    public String amount;
    public String shipping;
    public String product_name;
    public String product_image;
    public String shipping_cost;


    public Cart() {
    }
    public String getShipping_cost()
    {
        return shipping_cost;
    }
    public void setShipping_cost(String shipping_cost)
    {
        this.shipping_cost=shipping_cost;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }


    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
