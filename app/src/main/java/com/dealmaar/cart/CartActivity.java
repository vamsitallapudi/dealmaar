package com.dealmaar.cart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.dealmaar.checkout.CheckoutActivity;
import com.dealmaar.common.BaseActivity;
import com.dealmaar.common.DatabaseHandler;
import com.dealmaar.common.LaunchActivity;
import com.dealmaar.common.MainCardModel;
import com.dealmaar.common.Preferences;
import com.dealmaar.common.SupportActivity;
import com.dealmaar.customer.R;
import com.dealmaar.home.HomeActivity;
import com.dealmaar.login.LoginActivity;
import com.dealmaar.login.MyApplication;
import com.dealmaar.login.SigninActivity;
import com.dealmaar.navigationdrawer.FragmentDrawer;
import com.dealmaar.orderslist.OrdersListActivity;
import com.dealmaar.push.NotificationSettingsActivity;
import com.dealmaar.usernotifications.UserNotificationActivity;
import com.dealmaar.wishlist.WishlistActivity;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CartActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {
    private ProgressDialog progressDialog;
    private ArrayList<Cart> arrayCartList = null;
    private ListView list;
    private FragmentDrawer drawerFragment;
    private Intent mIntent = null;
    private Button checkout, checkout1;
    private String order_id, uid, Total, shippingId;
    double total;
    private TextView cart, shipping;
    public static boolean isChangeMenu = false;

    public static Menu menu;
    TextView noitems;
    DrawerLayout drawerLayout;
    DatabaseHandler db;
    MainCardModel mainCardModel;

    private String shippingAmount;
    private String count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_activity);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.logo2);
        cart = (TextView) findViewById(R.id.cart_total);
        shipping = (TextView) findViewById(R.id.shipping_total);
        list = (ListView) findViewById(R.id.customer_cart_list);
        arrayCartList = new ArrayList<Cart>();
        noitems = (TextView) findViewById(R.id.cart_noitems);
        View logo = toolbar.getChildAt(1);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();
            }
        });


        uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
        if (uid == null) {
            Toast.makeText(CartActivity.this, "User not found. Please login to continue!", Toast.LENGTH_SHORT).show();
            mIntent = new Intent(this, SigninActivity.class);
            startActivity(mIntent);
            finish();
        } else {
            new GetShippingrate(uid).executeOnExecutor(MyApplication.threadPoolExecutor);


            if (getIntent().getExtras() != null) {
                String CalledFromAdapter = getIntent().getExtras().getString("FromCartAdapter");
                String CalledFromAdapterMinus = getIntent().getExtras().getString("CalledfromAdapterdel");
                total = getIntent().getExtras().getDouble("Total");
                if (CalledFromAdapter != null || CalledFromAdapterMinus != null) {
                    new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);

                }

            } else {
                new GetCartListTask().executeOnExecutor(MyApplication.threadPoolExecutor);

            }

            checkout = (Button) findViewById(R.id.cart_proceedtocheckout);
            checkout1 = (Button) findViewById(R.id.cart_proceedtocheckout1);


            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (arrayCartList.isEmpty()) {
                        //checkout.setAlpha(0.5f);
                        shippingAmount = "0.0";
                        Toast.makeText(CartActivity.this, "Your cart is empty !", Toast.LENGTH_LONG).show();
                    } else {

                        Intent myintent = new Intent(CartActivity.this, CheckoutActivity.class);
                        myintent.putExtra("CalledfromCart", "Cart");
                        myintent.putExtra("Total", total);
                        myintent.putExtra("Shipping", shippingAmount);
                        myintent.putExtra("shippingId", shippingId);
                        myintent.putExtra("Cartlist", arrayCartList);

                        startActivity(myintent);

                    }
                }
            });
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            drawerFragment = (FragmentDrawer)
                    getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
            drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
            drawerFragment.setDrawerListener(this);
            //displayView(0);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
            CartActivity.this.invalidateOptionsMenu();
        }
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        CartActivity.this.invalidateOptionsMenu();
        //Refresh your stuff here
    }


    private void displayView(int position) {
        Fragment fragment = null;
        //String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                mIntent = new Intent(this, HomeActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 1:
                mIntent = new Intent(this, OrdersListActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 2:
                mIntent = new Intent(this, WishlistActivity.class);
                startActivity(mIntent);
                //finish();

                break;
            case 3:
                mIntent = new Intent(this, NotificationSettingsActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 4:
                String url = "market://details?id=com.dealmaar.customer";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // finish();
                break;
            case 5:
                mIntent = new Intent(this, SupportActivity.class);
                startActivity(mIntent);
                //finish();
                break;
            case 6:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Install dealmaar mobile app   https://play.google.com/store/apps/details?id=com.dealmaar.customer";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Dealmaar");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 7:
                String uid = Preferences.getString(Preferences.PrefType.User_id, getApplicationContext());
                if (uid == null) {
                    mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {

                    String user = null;
                    Preferences.add(Preferences.PrefType.User_id, user, getApplicationContext());
                    mIntent = new Intent(this, LaunchActivity.class);
                    Batch.User.getEditor().setIdentifier(null).setAttribute("user_id", "null").save();
                    Batch.onStop(this);
                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    String s = db.getName();
                    db.deleteName(s);
                    startActivity(mIntent);
                    finish();

                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        // set the toolbar title
        //getSupportActionBar().setTitle(title);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);

    }


    private class GetCartListTask extends AsyncTask<Void, Void, Double> {
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CartActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Double doInBackground(Void... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Addtocart/" + uid);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Cart-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Cart-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data " + e.toString());
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {

                    }
                }
            }
            try {
                JSONObject jobj = new JSONObject(String.valueOf(stringBuffer));
                Log.d("Stringbuffer", stringBuffer.toString());
                jArray = jobj.optJSONObject("addtocart");
                if (jArray != null) {
                    for (int i = 0; i < jArray.length(); i++) {
                        Cart cart = new Cart();
                        JSONArray jsonArray = jArray.optJSONArray("product");
                        JSONObject jRealObj = jArray.getJSONObject(String.valueOf(i));
                        // cart.setProduct_name(jRealObj.getString("product"));
                        cart.setProduct_id(jRealObj.getString("product_id"));
                        cart.setItem_id(jRealObj.getString("item_id"));
                        cart.setPrice(jRealObj.getString("price"));
                        cart.setAmount(jRealObj.getString("amount"));
                        try {

                            cart.setProduct_name(((JSONObject) jsonArray.get(i)).getString("product"));
                            cart.setProduct_image(((JSONObject) jsonArray.get(i)).getJSONObject("main_pair").getJSONObject("detailed").getString("image_path"));
                        } catch (JSONException e) {
                            Log.e("log_tag", "Error parsing data " + e.toString());

                            progressDialog.dismiss();
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                        }

                        int amt = Integer.parseInt(jRealObj.getString("amount"));
                        double Total, price;
                        price = Double.parseDouble(jRealObj.getString("price"));
                        Total = price;
                        if (amt > 1) {
                            for (int j = 1; j < amt; ++j) {
                                Total += price;
                            }
                        }
                        arrayCartList.add(cart);
                        total += Total;
                    }
                } else {
                    arrayCartList.isEmpty();

                }
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
            return total;
        }

        protected void onPostExecute(Double result) {
            super.onPostExecute(result);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (arrayCartList.size() <= 0) {
                checkout.setVisibility(View.GONE);
                checkout1.setVisibility(View.VISIBLE);
                shippingAmount = "0.0";
                noitems.setVisibility(View.VISIBLE);
            }
            try {
                if (shippingAmount == null || shippingAmount.equals("false") || shippingAmount.equals(null)) {
                    shippingAmount = "0.0";
                }
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMinimumFractionDigits(2);
                nf.setMaximumFractionDigits(2);
                cart.setText(nf.format(Double.parseDouble(String.valueOf(total))));
                shipping.setText(nf.format(Double.parseDouble(String.valueOf(shippingAmount))));

                CartListAdapter adapter = new CartListAdapter(CartActivity.this, R.layout.cart_item, arrayCartList, total, cart, shippingAmount, shipping, checkout, checkout1);

                list.setAdapter(adapter);
                String count = String.valueOf(list.getAdapter().getCount());
                db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                String c = db.getCartCount(uid);
                db.deleteCartCount(c);
                mainCardModel = new MainCardModel(uid, count);
                db.addCartCount(mainCardModel);
                adapter.notifyDataSetChanged();
                CartActivity.this.invalidateOptionsMenu();
                progressDialog.dismiss();


            } catch (Exception e) {
                //Toast.makeText(CartActivity.this, "Failed to connect to internet Try again !", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    View cart = findViewById(R.id.action_cart);

                    db = ((MyApplication) getApplicationContext()).getDatabaseHandler();
                    try {
                        count = db.getCartCount(uid);
                    } catch (Exception e) {
                        if (count == null || (count.equals("null"))) {
                            //Log.e("If", "if");
                            count = "0";

                        }
                        Log.e("HomeActivity", e.toString());
                    }

                    if (count == null || count.equals("null")) {
                        count = "0";

                    } else if (Integer.parseInt(count) == 0) {
                        count = "0";
                    } else {

                        BadgeView badge = new BadgeView(CartActivity.this, cart);
                        badge.setText(count);
                        badge.show();
                    }
                    // HomeActivity.this.invalidateOptionsMenu();
                }
            });
        } catch (Exception e) {

        }
        return true;
    }

    private class GetShippingrate extends AsyncTask<String, Void, Void> {
        String user_id;
        private StringBuffer stringBuffer = new StringBuffer();
        private JSONObject jArray = new JSONObject();

        GetShippingrate(String User_id) {
            this.user_id = User_id;
        }

        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(CartActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet();
                URI uri = new URI(getString(R.string.url) + "Shippingrate/" + user_id);
                httpGet.setURI(uri);
                httpGet.addHeader(BasicScheme.authenticate(
                        new UsernamePasswordCredentials(getString(R.string.username), getString(R.string.password)),
                        HTTP.UTF_8, false));
                Log.e("Cart-Before", "Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Log.e("Cart-After", "New Date" + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String readLine = bufferedReader.readLine();
                while (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                    readLine = bufferedReader.readLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data " + e.toString());
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e("log_tag", "Error parsing data " + e.toString());
                    }
                }
            }
            try {
                JSONObject jsonObject = new JSONObject(String.valueOf(stringBuffer));
                if (jsonObject.has("price")) {
                    if (shippingAmount == "false" || shippingAmount == null) {
                        shippingAmount = "0.0";
                    }

                    shippingAmount = jsonObject.getString("price");
                    JSONObject jsonObject1 = jsonObject.optJSONObject("keys");
                    shippingId = jsonObject1.getString("shipping_id");
                }

            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            /*case R.id.action_cart:
                Intent cartintent = new Intent(this, CartActivity.class);
                startActivity(cartintent);

                return true;
*/
            case R.id.action_posts:
                Intent postsintent = new Intent(this, UserNotificationActivity.class);
                startActivity(postsintent);

                return true;

            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
